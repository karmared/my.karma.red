require("./config/env")

const path = require("path")
const webpack = require("webpack")

const APP_VERSION = require("child_process")
  .execSync("git describe --abbrev=0 --tags")
  .toString()
  .trim()

const {
  fbPixel,
  PWAMeta,
  SWScript,
  // GANKarma,
  AmoPixel,
  PreviewMeta,
  // GTAGManager,
  LoaderStyle,
  RoistatScript,
  TextbackScript,
  YMCounterKarma,
  // GTAGManagerNoScript,
} = require("./config/templateInjections")

const config = {
  entry: path.resolve(__dirname, "src", "index.tsx"),
  devtool: "source-map",
  output: {
    path: path.resolve(__dirname, "public"),
    publicPath: "/",
  },
  resolve: {
    extensions: [
      "*",
      ".mjs",
      ".js",
      ".jsx",
      ".json",
      ".gql",
      ".graphql",
      ".ts",
      ".tsx",
    ],
    alias: {
      src: path.resolve(__dirname, "src"),
    },
  },
  optimization: {
    splitChunks: {
      chunks: "all",
    },
  },
  plugins: [
    new webpack.DefinePlugin({
      "process.env.APP_VERSION": JSON.stringify(APP_VERSION),
    }),
    new webpack.EnvironmentPlugin([
      "NODE_ENV",
      "GRAPHQL_ENDPOINT",
      "GUEST_REDIRECT_PAGE",
    ]),
  ],
  module: {
    rules: [
      {
        test: /\.(png|jpg|jpeg|svg|gif)$/,
        use: ["file-loader"],
      },
      {
        test: /\.(ttf|woff|woff2|eot)$/,
        use: ["file-loader"],
      },
      {
        test: /\.xml$/,
        use: ["xml-loader"],
      },
      {
        test: /\.csv$/,
        use: ["csv-loader"],
      },
    ],
  },
}

const HtmlWebpackPluginConfig = {
  template: path.resolve(__dirname, "src", "index.html"),
  fbPixel,
  PWAMeta,
  SWScript,
  // GANKarma,
  AmoPixel,
  PreviewMeta,
  // GTAGManager,
  LoaderStyle,
  RoistatScript,
  TextbackScript,
  YMCounterKarma,
  // GTAGManagerNoScript,
}

module.exports = {
  config,
  HtmlWebpackPluginConfig,
}

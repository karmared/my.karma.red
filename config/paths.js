const path = require("path")

module.exports = {
  config: path.resolve(process.cwd(), "config"),
  schema: path.resolve(process.cwd(), "config", "schema.graphql"),
  locales: path.resolve(process.cwd(), "src", "locales"),
}

const GTAGManager = `
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-W6HWFQQ');</script>
<!-- End Google Tag Manager -->
`

const GTAGManagerNoScript = `
  <!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-W6HWFQQ"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
`

const fbPixel = `
<!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};
    if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
    n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];
    s.parentNode.insertBefore(t,s)}(window, document,'script',
    'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '1922001517882293');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
               src="https://www.facebook.com/tr?id=308484563073725&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->
`

const GANKarma = `
<script>(
 function(w,d,s,l,i){
  w[l]= w[l]||[];

  w[l].push({'gtm.start': new Date().getTime(),event:'gtm.js'});

  var f = d.getElementsByTagName(s)[0],
      j = d.createElement(s),
      dl = l !='dataLayer'?'&l='+l:'';

      j.async=true;
      j.src='https://www.googletagmanager.com/gtm.js?id='+i+dl;
      f.parentNode.insertBefore(j,f);
      
  })(window,document,'script','dataLayer','GTM-WDKH3K7');</script>
`

const YMCounterKarma = `
<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
(function (d, w, c) {
(w[c] = w[c] || []).push(function() {
try {
w.yaCounter51340867 = new Ya.Metrika2({
id:51340867,
clickmap:true,
trackLinks:true,
accurateTrackBounce:true,
webvisor:true
});
} catch(e) { }
});

    var n = d.getElementsByTagName("script")[0],
        s = d.createElement("script"),
        f = function () { n.parentNode.insertBefore(s, n); };
    s.type = "text/javascript";
    s.async = true;
    s.src = "https://mc.yandex.ru/metrika/tag.js"

    if (w.opera == "[object Opera]") {
        d.addEventListener("DOMContentLoaded", f, false);
    } else { f(); }
})(document, window, "yandex_metrika_callbacks2");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/51340867" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
`

const TextbackScript = `
  <!-- Textback script -->
<script type="text/javascript" encoding="utf-8">
var _tbEmbedArgs = _tbEmbedArgs || [];
(function () {
  var u =  "https://tb-kube-loadbalancer-prod.textback.io/tb-widget-srv/widget";
  _tbEmbedArgs.push(["widgetId", "5cf5fceb-3cd6-4bee-b72e-fda204f85b30"]);
  _tbEmbedArgs.push(["baseUrl", u]);

  var d = document, g = d.createElement("script"), s = d.getElementsByTagName("script")[0];
  g.type = "text/javascript";
  g.charset = "utf-8";
  g.defer = true;
  g.async = true;
  g.src = u + "/widget.js";
  s.parentNode.insertBefore(g, s);
})();
</script>
<!-- /Textback script -->
`

const RoistatScript = `
  <!-- Roistat -->
  <script>
    (function(w, d, s, h, id) {
        w.roistatProjectId = id; w.roistatHost = h;
        var p = d.location.protocol == "https:" ? "https://" : "http://";
        var u = /^.*roistat_visit=[^;]+(.*)?$/.test(d.cookie) ? "/dist/module.js" : "/api/site/1.0/"+id+"/init";
        var js = d.createElement(s); js.charset="UTF-8"; js.async = 1; js.src = p+h+u; var js2 = d.getElementsByTagName(s)[0]; js2.parentNode.insertBefore(js, js2);
    })(window, document, 'script', 'cloud.roistat.com', 'f9015fe177da033f2c8d26da8f91e4f5');
  </script>
  <!-- /Roistat -->
`

const AmoPixel = `
  <!-- Amo Pixel -->
  <script type="text/javascript">(function (w, d) {w.amo_pixel_token = 'p1Ip4iB8yqaEyXU9HPmcTNFk190/n6qSlHVan6YCmIVcipMPIyQEQxW2+R3DID6e';var s = document.createElement('script'), f = d.getElementsByTagName('script')[0];s.id = 'amo_pixel_js';s.type = 'text/javascript';s.async = true;s.src = 'https://piper.amocrm.ru/pixel/js/tracker/pixel.js?token=' + w.amo_pixel_token;f.parentNode.insertBefore(s, f);})(window, document);</script>
  <!-- /Amo Pixel -->
`

// определяем текущий урл приложения по урлу graphql. Работает пока апи и фронт хостятся на одном доменном имени.
const [protocol, rest] = process.env.GRAPHQL_ENDPOINT.split("//")
const baseUrl = `${protocol}//${rest.split("/")[0]}`

const PreviewMeta = `
  <meta property="og:title" content="Karma. Global p2p-lending platform">
  <meta property="og:description" content="Get highest yield by investing in popular and reliable companies">
  <meta property="og:url" content="${baseUrl}">
  <meta property="og:site_name" content="Karma. Global p2p-lending platform">
  <meta property="og:type" content="website">
  <meta property="og:image" content="${baseUrl}/images/rocketman.png">
`

const PWAMeta = `
  <link rel="manifest" href="/manifest.json">
  <meta name="mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="theme-color" content="#b83b3b">
`

const LoaderStyle = `
  <style>
    #app-root:empty {
      position: absolute;
      top: calc(50% - 0.5em);
      left: calc(50% - 0.5em);
      width: 1em;
      height: 1em;
      border-radius: 50%;
      text-indent: -9999em;
      animation: _load 1.1s infinite ease;
    }

    @keyframes _load {
      0%,
      100% {
        box-shadow: 0em -2.6em 0em 0em #ff4a38, 1.8em -1.8em 0 0em rgba(255,74,56, 0.2), 2.5em 0em 0 0em rgba(255,74,56, 0.2), 1.75em 1.75em 0 0em rgba(255,74,56, 0.2), 0em 2.5em 0 0em rgba(255,74,56, 0.2), -1.8em 1.8em 0 0em rgba(255,74,56, 0.2), -2.6em 0em 0 0em rgba(255,74,56, 0.5), -1.8em -1.8em 0 0em rgba(255,74,56, 0.7);
      }
      12.5% {
        box-shadow: 0em -2.6em 0em 0em rgba(255,74,56, 0.7), 1.8em -1.8em 0 0em #ff4a38, 2.5em 0em 0 0em rgba(255,74,56, 0.2), 1.75em 1.75em 0 0em rgba(255,74,56, 0.2), 0em 2.5em 0 0em rgba(255,74,56, 0.2), -1.8em 1.8em 0 0em rgba(255,74,56, 0.2), -2.6em 0em 0 0em rgba(255,74,56, 0.2), -1.8em -1.8em 0 0em rgba(255,74,56, 0.5);
      }
      25% {
        box-shadow: 0em -2.6em 0em 0em rgba(255,74,56, 0.5), 1.8em -1.8em 0 0em rgba(255,74,56, 0.7), 2.5em 0em 0 0em #ff4a38, 1.75em 1.75em 0 0em rgba(255,74,56, 0.2), 0em 2.5em 0 0em rgba(255,74,56, 0.2), -1.8em 1.8em 0 0em rgba(255,74,56, 0.2), -2.6em 0em 0 0em rgba(255,74,56, 0.2), -1.8em -1.8em 0 0em rgba(255,74,56, 0.2);
      }
      37.5% {
        box-shadow: 0em -2.6em 0em 0em rgba(255,74,56, 0.2), 1.8em -1.8em 0 0em rgba(255,74,56, 0.5), 2.5em 0em 0 0em rgba(255,74,56, 0.7), 1.75em 1.75em 0 0em #ff4a38, 0em 2.5em 0 0em rgba(255,74,56, 0.2), -1.8em 1.8em 0 0em rgba(255,74,56, 0.2), -2.6em 0em 0 0em rgba(255,74,56, 0.2), -1.8em -1.8em 0 0em rgba(255,74,56, 0.2);
      }
      50% {
        box-shadow: 0em -2.6em 0em 0em rgba(255,74,56, 0.2), 1.8em -1.8em 0 0em rgba(255,74,56, 0.2), 2.5em 0em 0 0em rgba(255,74,56, 0.5), 1.75em 1.75em 0 0em rgba(255,74,56, 0.7), 0em 2.5em 0 0em #ff4a38, -1.8em 1.8em 0 0em rgba(255,74,56, 0.2), -2.6em 0em 0 0em rgba(255,74,56, 0.2), -1.8em -1.8em 0 0em rgba(255,74,56, 0.2);
      }
      62.5% {
        box-shadow: 0em -2.6em 0em 0em rgba(255,74,56, 0.2), 1.8em -1.8em 0 0em rgba(255,74,56, 0.2), 2.5em 0em 0 0em rgba(255,74,56, 0.2), 1.75em 1.75em 0 0em rgba(255,74,56, 0.5), 0em 2.5em 0 0em rgba(255,74,56, 0.7), -1.8em 1.8em 0 0em #ff4a38, -2.6em 0em 0 0em rgba(255,74,56, 0.2), -1.8em -1.8em 0 0em rgba(255,74,56, 0.2);
      }
      75% {
        box-shadow: 0em -2.6em 0em 0em rgba(255,74,56, 0.2), 1.8em -1.8em 0 0em rgba(255,74,56, 0.2), 2.5em 0em 0 0em rgba(255,74,56, 0.2), 1.75em 1.75em 0 0em rgba(255,74,56, 0.2), 0em 2.5em 0 0em rgba(255,74,56, 0.5), -1.8em 1.8em 0 0em rgba(255,74,56, 0.7), -2.6em 0em 0 0em #ff4a38, -1.8em -1.8em 0 0em rgba(255,74,56, 0.2);
      }
      87.5% {
        box-shadow: 0em -2.6em 0em 0em rgba(255,74,56, 0.2), 1.8em -1.8em 0 0em rgba(255,74,56, 0.2), 2.5em 0em 0 0em rgba(255,74,56, 0.2), 1.75em 1.75em 0 0em rgba(255,74,56, 0.2), 0em 2.5em 0 0em rgba(255,74,56, 0.2), -1.8em 1.8em 0 0em rgba(255,74,56, 0.5), -2.6em 0em 0 0em rgba(255,74,56, 0.7), -1.8em -1.8em 0 0em #ff4a38;
      }
    }
  </style>
`

const SWScript = `
  <script>
    if ('serviceWorker' in navigator) {
      window.addEventListener('load', function() {
        navigator.serviceWorker.register('/service-worker.js').then(function(registration) {
          // Registration was successful
          console.log('ServiceWorker registration successful with scope: ', registration.scope);
        }, function(err) {
          // registration failed :(
          console.log('ServiceWorker registration failed: ', err);
        }).catch(function(err) {
          console.log(err)
        });
      });
    } else {
      console.log('service worker is not supported');
    }
  </script>
`

module.exports = {
  fbPixel,
  PWAMeta,
  //GANKarma,
  AmoPixel,
  SWScript,
  PreviewMeta,
  //GTAGManager,
  LoaderStyle,
  RoistatScript,
  TextbackScript,
  YMCounterKarma,
  //GTAGManagerNoScript,
}

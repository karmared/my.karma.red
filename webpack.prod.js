require("./config/env")

const path = require("path")

const { CleanWebpackPlugin } = require("clean-webpack-plugin")
const HtmlWebpackPlugin = require("html-webpack-plugin")
const CopyWebpackPlugin = require("copy-webpack-plugin")
const TerserWebpackPlugin = require("terser-webpack-plugin")
const SentryWebpackPlugin = require("@sentry/webpack-plugin")

const common = require("./webpack.common")

module.exports = {
  ...common.config,
  mode: "production",
  output: {
    ...common.config.output,
    filename: "[name].[contenthash].js",
  },
  optimization: {
    ...common.config.optimization,
    runtimeChunk: true,
    concatenateModules: true,
    minimizer: [
      new TerserWebpackPlugin({
        cache: true,
      }),
    ],
  },
  plugins: [
    ...common.config.plugins,
    new CleanWebpackPlugin(),
    new CopyWebpackPlugin([
      {
        from: path.resolve(__dirname, "static"),
        to: path.resolve(__dirname, "public"),
      },
      {
        from: path.resolve(__dirname, "static", "service-worker.js"),
        to: path.resolve(__dirname, "public", "service-worker.js"),
        force: true,
        transform(content) {
          return content
            .toString()
            .replace(/__RANDOM_HASH__/g, Math.random().toString(16).slice(2, 8))
        },
      },
    ]),
    new HtmlWebpackPlugin({
      ...common.HtmlWebpackPluginConfig,
      minify: true,
    }),
    new SentryWebpackPlugin({
      include: "./public",
      ignoreFile: ".sentrycliignore",
      ignore: [
        "node_modules",
        "webpack.common.js",
        "webpack.prod.js",
        "webpack.dev.js",
      ],
      configFile: ".sentryclirc",
      rewrite: true,
    }),
  ],
  module: {
    ...common.config.module,
    rules: [
      ...common.config.module.rules,
      {
        test: /\.(js|jsx|ts|tsx)$/,
        include: path.resolve(__dirname, "src"),
        exclude: /(node_modules|bower_components)/,
        use: ["thread-loader", "babel-loader"],
      },
      {
        test: /\.css$/,
        use: ["css-loader"],
      },
    ],
  },
}

// Set this to true for production
var doCache = location.protocol === "https:"

// Name our cache
var CACHE_NAMES = {
  "karmared-cache-v6.__RANDOM_HASH__": {
    urls: [
      // files with hash code in names
      /^http:\/\/localhost(?!:\d+?\/sockjs-node).+\.js$/,
      /^https:\/\/(devnet-)?my\.karma\.red.+\.js$/,
    ],
  },
  "karmared-cache-v6": {
    urls: [
      /^http:\/\/localhost(?!:\d+?\/sockjs-node)([^#\?\/]*?\/)+(?!.+\.js)([^#\?]+?\.)/,
      /^https:\/\/(devnet-)?my\.karma\.red(?!\/cdn\/temporary)([^#\?\/]*?\/)+(?!.+\.js)([^#\?]+?\.)/,
      /^https:\/\/unpkg\.com/,
    ],
  },
}

// Delete old caches that are not our current one!
self.addEventListener("activate", function (event) {
  var cacheWhitelist = Object.keys(CACHE_NAMES)
  event.waitUntil(
    caches.keys().then(function (keyList) {
      return Promise.all(
        keyList.map(function (key) {
          if (!cacheWhitelist.includes(key)) {
            console.log("Deleting cache: " + key)
            return caches.delete(key)
          }
        })
      )
    })
  )
})

// The first time the user starts up the PWA, 'install' is triggered.
self.addEventListener("install", function (event) {
  // if (doCache) {
  //   event.waitUntil(precache())
  // }
  // force update sw to fix dengerous bug in prod
  // self.skipWaiting();
})

// When the webpage goes to fetch files, we intercept that request and serve up the matching files
// if we have them
self.addEventListener("fetch", function (event) {
  if (!doCache) {
    return
  }

  if (event.request.method !== "GET") {
    return
  }

  if (event.request.destination === "document") {
    return
  }

  var cacheNames = Object.keys(CACHE_NAMES)

  for (var j = 0; j < cacheNames.length; j++) {
    var cacheName = cacheNames[j]
    var cacheURLWhitelist = CACHE_NAMES[cacheName].urls

    for (var i = 0; i < cacheURLWhitelist.length; i++) {
      if (event.request.url.match(cacheURLWhitelist[i])) {
        event.respondWith(fromCache(cacheName, event.request.clone()))
        event.waitUntil(update(cacheName, event.request.clone()))
        return
      }
    }
  }
})

// function precache() {
//   return caches.open(CACHE_NAME).then(function (cache) {
//     const urlsToCache = []
//     cache.addAll(urlsToCache)
//   })
// }

function fromCache(cacheName, request) {
  return caches.open(cacheName).then(function (cache) {
    return cache.match(request).then(function (matching) {
      return matching || fetch(request)
    })
  })
}

function update(cacheName, request) {
  return caches.open(cacheName).then(function (cache) {
    return fetch(request).then(function (response) {
      return cache.put(request, response)
    })
  })
}

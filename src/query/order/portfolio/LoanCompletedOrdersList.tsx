import React from "react"
import { graphql, createPaginationContainer } from "react-relay"

import { QueryRenderer } from "src/components"
import { RenderedTable } from "src/components/organisms/PortfolioTable/Tables/LoanCompletedTable"

import { VISIBLE_ORDER } from "./constants"
import { BorrowerRenderComponent, getBorrowerConnectionConfig } from "./utils"

const queries = {
  order: graphql`
    fragment LoanCompletedOrdersListFragment on OrderEdge {
      node {
        id
        status
        confirmedAt
        paymentScheduleList {
          info {
            state
            pastdue_days
          }
          items {
            state
            total
            loan
            payDate
            date
            interest
            total_gross
            total_details {
              currency
              amount
            }
            total_gross_details {
              currency
              amount
            }
          }
        }
        offer {
          data
          receivedAt
          approvedAt
        }
        application {
          data
          shortTitle
          longTitle
        }
        chain {
          id
          gatheredAmount
        }
        profile {
          ... on UserProfile {
            id
            ... on LegalEntityProfile {
              name
              borrower {
                ticker
              }
            }
          }
        }
      }
    }
  `,
  query: graphql`
    query LoanCompletedOrdersListQuery(
      $count: Int!
      $cursor: Cursor
      $profileId: ID!
      $filter: BorrowerOrdersFilter
    ) {
      ...LoanCompletedOrdersList_root
      @arguments(
        count: $count
        cursor: $cursor
        profileId: $profileId
        filter: $filter
      )
    }
  `,
}

const PaginatedOrderTable = createPaginationContainer(
  BorrowerRenderComponent,
  {
    root: graphql`
      fragment LoanCompletedOrdersList_root on Query
      @argumentDefinitions(
        count: { type: "Int", defaultValue: 10 }
        cursor: { type: "Cursor" }
        profileId: { type: "ID!" }
        filter: { type: "BorrowerOrdersFilter" }
      ) {
        node(id: $profileId) {
          ... on LegalEntityProfile {
            borrower {
              orders(first: $count, after: $cursor, filter: $filter)
              @connection(key: "CompletedLoan_orders") {
                edges {
                  ...LoanCompletedOrdersListFragment @relay(mask: false)
                }
              }
            }
          }
        }
      }
    `,
  },
  getBorrowerConnectionConfig(queries.query, "root"),
)

const Container = (render, profile) => (mainProps) => (
  <QueryRenderer
    {...mainProps}
    query={queries.query}
    render={(props) => (
      <PaginatedOrderTable root={props} render={render} profileType="root" />
    )}
    renderNull={() => <RenderedTable isFetching={true} load={{}} />}
    variables={{
      profileId: profile.id,
      count: VISIBLE_ORDER,
      filter: { status: ["COMPLETE"] },
    }}
  />
)

export default Container

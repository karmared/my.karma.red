import React from "react"
import { graphql, createPaginationContainer } from "react-relay"

import { QueryRenderer } from "src/components"
import { RenderedTable } from "src/components/organisms/PortfolioTable/Tables/LoanSucceededTable"

import { VISIBLE_ORDER } from "./constants"
import { BorrowerRenderComponent, getBorrowerConnectionConfig } from "./utils"

const queries = {
  order: graphql`
    fragment LoanSucceededOrdersListFragment on OrderEdge {
      node {
        id
        status
        confirmedAt
        succeededAt
        paymentSchedule {
          status
        }
        offer {
          data
          receivedAt
          approvedAt
        }
        application {
          data
          shortTitle
          longTitle
        }
        chain {
          id
          gatheredAmount
        }
        profile {
          ... on UserProfile {
            id
            ... on LegalEntityProfile {
              name
              borrower {
                ticker
              }
            }
          }
        }
      }
    }
  `,
  query: graphql`
    query LoanSucceededOrdersListQuery(
      $count: Int!
      $cursor: Cursor
      $profileId: ID!
      $filter: BorrowerOrdersFilter
    ) {
      ...LoanSucceededOrdersList_root
      @arguments(
        count: $count
        cursor: $cursor
        profileId: $profileId
        filter: $filter
      )
    }
  `,
}

const PaginatedOrderTable = createPaginationContainer(
  BorrowerRenderComponent,
  {
    root: graphql`
      fragment LoanSucceededOrdersList_root on Query
      @argumentDefinitions(
        count: { type: "Int", defaultValue: 10 }
        cursor: { type: "Cursor" }
        profileId: { type: "ID!" }
        filter: { type: "BorrowerOrdersFilter" }
      ) {
        node(id: $profileId) {
          ... on LegalEntityProfile {
            borrower {
              orders(first: $count, after: $cursor, filter: $filter)
              @connection(key: "SucceededLoan_orders") {
                edges {
                  ...LoanSucceededOrdersListFragment @relay(mask: false)
                }
              }
            }
          }
        }
      }
    `,
  },
  getBorrowerConnectionConfig(queries.query, "root"),
)

const Container = (render, profile) => (mainProps) => (
  <QueryRenderer
    {...mainProps}
    query={queries.query}
    render={(props) => (
      <PaginatedOrderTable root={props} render={render} profileType="root" />
    )}
    renderNull={() => <RenderedTable isFetching={true} load={{}} />}
    variables={{
      profileId: profile.id,
      count: VISIBLE_ORDER,
      filter: { status: ["SUCCEEDED"] },
    }}
  />
)

export default Container

import React from "react"

import { graphql } from "react-relay"
import { QueryRenderer } from "src/components"

const query = graphql`
  query GuestOrderInfoQuery($id: ID!) {
    node(id: $id) {
      id
      __typename
      ... on Order {
        createdAt
        status
        statusFrom
        fee
        profile {
          ... on UserProfile {
            id
            ... on LegalEntityProfile {
              name
            }
            creditRating {
              rating
            }
            _avatar {
              url
            }
          }
        }
        offer {
          ... on OrderOffer {
            id
            data
            receivedAt
          }
        }
        application {
          ... on OrderApplication {
            id
            data
            receivedAt
          }
        }
        confirmedAt
        creditRating {
          rating
        }
        cession {
          due
          isActive
          interestRate
          borrowerName
          originalChainId
          avatar {
            url
          }
          borrowerRating {
            rating
            ratedAt
          }
        }
        skipOffer
        karma {
          isInvestor
          isTrusted
        }
        provision {
          items {
            kind
          }
        }
      }
    }
  }
`

const container = (render) => (props) => {
  const { orderId, ...rest } = props

  const variables = {
    id: orderId,
  }

  return (
    <QueryRenderer
      {...rest}
      query={query}
      render={render}
      variables={variables}
    />
  )
}

container.query = query

export default container

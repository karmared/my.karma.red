import React from "react"

import { graphql } from "react-relay"
import { QueryRenderer } from "src/components"

const query = graphql`
  query LoanInfoQuery($id: ID!) {
    node(id: $id) {
      ...DebtPaymentData_order
      ...InstructionsModal_order
      id
      __typename
      ... on Order {
        createdAt
        status
        statusFrom
        fee
        profile {
          ... on UserProfile {
            id
            ... on LegalEntityProfile {
              name
              inn
              ogrn
              executive {
                name
              }
            }
            creditRating {
              rating
            }
            _avatar {
              url
            }
          }
        }
        offer {
          ... on OrderOffer {
            id
            data
            declineReason
            receivedAt
          }
        }
        application {
          ... on OrderApplication {
            id
            data
            declineReason
            receivedAt
          }
        }
        confirmedAt
        creditRating {
          rating
        }
        otherDocuments {
          attachment {
            url
            filename
          }
          description
        }
        cession {
          due
          amount
          isActive
          interestRate
          borrowerName
          originalChainId
          avatar {
            url
          }
          borrowerRating {
            rating
            ratedAt
          }
        }
        skipOffer
        karma {
          isInvestor
          isTrusted
        }
        chain {
          id
          gatheredAmount
          investorsCount
        }
        guarantors {
          attachment {
            url
            filename
          }
          name
          description
        }
        viewer {
          hasIntent
          investedAmount
        }
        borrowerPayment: document(type: BORROWER_PAYMENT) {
          url
        }
        paymentScheduleList {
          info {
            state
          }
          items {
            state
          }
        }
        provision {
          items {
            kind
          }
        }
      }
    }
  }
`

const container = (render) => ({ orderId, ...rest }) => {
  const variables = {
    id: orderId,
  }

  return (
    <QueryRenderer
      {...rest}
      query={query}
      render={render}
      variables={variables}
    />
  )
}

container.query = query

export default container

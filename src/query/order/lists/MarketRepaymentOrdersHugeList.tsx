import React from "react"

import { graphql, createPaginationContainer } from "react-relay"

import { QueryRenderer } from "src/components"
import { VISIBLE_ORDER_COMPLETED } from "../portfolio/constants"
import { RenderTableComponent } from "./utils"

// TODO: unused - вернуть позже или удалить окончательно
/* const MockLoader = (props) => (
  <Box py="60px">
    <Loader fontSize="14px" />
  </Box>
) */

const manageOrdersQuery = graphql`
  query MarketRepaymentOrdersHugeListQuery(
    $pageSize: Int!
    $after: Cursor
    $filter: OrdersFilterActive
  ) {
    ...MarketRepaymentOrdersHugeList_root
  }
`
const PaginatedOrderTable = createPaginationContainer(
  RenderTableComponent,
  {
    root: graphql`
      fragment MarketRepaymentOrdersHugeList_root on Query {
        orders(first: $pageSize, after: $after, filter: $filter)
          @connection(key: "ManageOrdersRepaymentRelay_orders") {
          edges {
            node {
              id
              __typename
              ... on Order {
                createdAt
                succeededAt
                status
                statusFrom
                tags {
                  name
                }
                backgroundImage {
                  url
                }
                offer {
                  ... on OrderOffer {
                    id
                    data
                  }
                }
                application {
                  shortTitle
                  longTitle
                  externalMedium {
                    video {
                      url
                    }
                  }
                  ... on OrderApplication {
                    id
                    data
                  }
                }
                profile {
                  ... on UserProfile {
                    id
                    ... on EntrepreneurProfile {
                      creditRating {
                        rating
                      }
                    }
                    ... on LegalEntityProfile {
                      name
                      creditRating {
                        rating
                      }
                      _avatar {
                        url
                      }
                      borrower {
                        ticker
                      }
                    }
                  }
                }
                completedAt
                fee
                chain {
                  id
                  gatheredAmount
                  investorsCount
                }
                creditRating {
                  rating
                }
                paymentSchedule {
                  borrower {
                    items {
                      state
                      date
                    }
                    info {
                      state
                    }
                  }
                }
                cession {
                  due
                  amount
                  isActive
                  interestRate
                  borrowerName
                  originalChainId
                  avatar {
                    url
                  }
                  borrowerRating {
                    rating
                    ratedAt
                  }
                }
                karma {
                  conclusion
                  isInvestor
                }
                viewer {
                  investedAmount
                }
                provision {
                  items {
                    kind
                  }
                }
              }
            }
          }
        }
      }
    `,
  },
  {
    direction: "forward",
    query: manageOrdersQuery,
    getConnectionFromProps: (props) => props.root.orders,
    getFragmentVariables: (previousVars) => ({
      ...previousVars,
      pageSize: VISIBLE_ORDER_COMPLETED,
    }),
    getVariables: (props, paginationInfo, { filter }) => ({
      filter,
      pageSize: VISIBLE_ORDER_COMPLETED,
      after: paginationInfo.cursor,
    }),
  },
)

type Props = {
  render: (props: any) => JSX.Element
}

const HugeContainer = ({ render }: Props) => {
  const [filter, setFilter] = React.useState({})

  const paginatedRender = (props) => (
    <PaginatedOrderTable
      root={props.data || null}
      render={render}
      filter={filter}
      setFilter={setFilter}
      pageSize={VISIBLE_ORDER_COMPLETED}
    />
  )

  return (
    <QueryRenderer
      query={manageOrdersQuery}
      render={paginatedRender}
      renderNull={paginatedRender}
      variables={{
        filter: {
          ...filter,
          status: ["COMPLETE"],
          cession: false,
        },
        pageSize: VISIBLE_ORDER_COMPLETED,
      }}
    />
  )
}

export default HugeContainer

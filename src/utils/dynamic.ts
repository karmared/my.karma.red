import loadable from "@loadable/component"

export const dynamic = (load): any => loadable(load)

export const catchError = () => window.location.reload()

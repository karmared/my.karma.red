export const getDebtURL = (chainID: string) => {
  const baseURL = "https://wow.karma.red/debt"

  return chainID ? `${baseURL}#order_${chainID.split(".").pop()}` : baseURL
}

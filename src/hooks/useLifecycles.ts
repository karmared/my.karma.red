import { useEffect } from "react"

const useLifecycles = (mount, unmount) => {
  useEffect(() => {
    if (mount) mount()
    return () => {
      if (unmount) unmount()
    }
  }, [])
}

/*
* const Demo = () => {
    useLifecycles(() => console.log("MOUNTED"), () => console.log("UNMOUNTED"))
    return null;
  }
* */

export default useLifecycles

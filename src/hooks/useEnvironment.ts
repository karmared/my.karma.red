import { useContext } from "react"
import { RelayEnvironmentContext } from "src/context"

const useEnvironment = () => useContext(RelayEnvironmentContext)

export default useEnvironment

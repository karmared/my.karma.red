export { default as UploadFile } from "./UploadFile"
export { default as UploadAvatarInput } from "./UploadAvatar"
export { default as UploadPassportScan } from "./UploadPassportScan"

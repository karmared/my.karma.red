import { graphql } from "relay-runtime"
import enhance from "../enhance"

const mutation = graphql`
  mutation UpdateIndividualProfileInvestorAccreditationMutation(
    $input: UpdateIndividualProfileInvestorAccreditationInput!
  ) {
    updateIndividualProfileInvestorAccreditation(input: $input) {
      profile {
        id
        ...Header_profile
        ...ProfileBody_profile
      }
    }
  }
`

export default enhance({ mutation })

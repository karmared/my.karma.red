import { graphql } from "relay-runtime"
import enhance from "../enhance"

const mutation = graphql`
  mutation MailInvestorPaymentMutation($input: MailInvestorPaymentInput!) {
    mailInvestorPayment(input: $input) {
      status
    }
  }
`

export default enhance({ mutation })

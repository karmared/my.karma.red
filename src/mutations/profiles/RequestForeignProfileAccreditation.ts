import { graphql } from "relay-runtime"
import enhance from "../enhance"

const mutation = graphql`
  mutation RequestForeignProfileAccreditationMutation(
    $input: RequestProfileAccreditationInput!
  ) {
    requestProfileAccreditation(input: $input) {
      foreignProfile {
        id
        ...AccreditationInfoFragment_foreignProfile
      }
    }
  }
`

export default enhance({ mutation })

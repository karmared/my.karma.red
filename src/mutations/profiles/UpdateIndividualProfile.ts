import { graphql } from "relay-runtime"
import enhance from "../enhance"

const mutation = graphql`
  mutation UpdateIndividualProfileMutation(
    $input: UpdateIndividualProfileInput!
  ) {
    updateIndividualProfile(input: $input) {
      profile {
        id
        name
        ...Header_profile
        ...ProfileBody_profile
      }
    }
  }
`

export default enhance({ mutation })

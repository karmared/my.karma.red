import { graphql } from "relay-runtime"
import enhance from "../enhance"

const mutation = graphql`
  mutation GetOrderPortfolioReportMutation(
    $input: GetOrderPortfolioReportInput!
  ) {
    getOrderPortfolioReport(input: $input) {
      url
    }
  }
`

export default enhance({ mutation })

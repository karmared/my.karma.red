import { graphql } from "relay-runtime"
import enhance from "../enhance"

const mutation = graphql`
  mutation CancelInvestmentMutation($input: CancelInvestmentInput!) {
    cancelInvestment(input: $input) {
      order {
        id
      }
    }
  }
`

export default enhance({ mutation })

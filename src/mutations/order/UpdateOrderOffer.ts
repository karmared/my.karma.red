import { graphql } from "relay-runtime"
import enhance from "../enhance"

const mutation = graphql`
  mutation UpdateOrderOfferMutation($input: UpdateOrderOfferInput!) {
    updateOrderOffer(input: $input) {
      order {
        id
      }
    }
  }
`

export default enhance({ mutation })

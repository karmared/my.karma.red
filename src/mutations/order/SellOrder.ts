import { graphql } from "relay-runtime"
import enhance from "../enhance"

const mutation = graphql`
  mutation SellOrderMutation($input: SellOrderInput!) {
    sellOrder(input: $input) {
      order {
        id
        profile {
          id
        }
      }
    }
  }
`

export default enhance({ mutation })

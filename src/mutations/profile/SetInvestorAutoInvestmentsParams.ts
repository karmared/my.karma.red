import { graphql } from "relay-runtime"
import enhance from "../enhance"

const mutation = graphql`
  mutation SetInvestorAutoInvestmentsParamsMutation(
    $input: setInvestorAutoInvestmentsParamsInput!
  ) {
    setInvestorAutoInvestmentsParams(input: $input) {
      status
    }
  }
`

export default enhance({ mutation })

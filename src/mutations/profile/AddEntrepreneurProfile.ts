import { graphql } from "relay-runtime"
import enhance from "../enhance"

const mutation = graphql`
  mutation AddEntrepreneurProfileMutation(
    $input: AddEntrepreneurProfileInput!
  ) {
    addEntrepreneurProfile(input: $input) {
      profile {
        id
        name
        phone
        ... on EntrepreneurProfile {
          lastName
          firstName
          middleName
          accreditation(role: INVESTOR) {
            status
          }
        }
      }
    }
  }
`

const updater = (store) => {
  const root = store.getRoot()
  const viewer = root.getLinkedRecord("viewer")
  const profiles = viewer.getLinkedRecords("profiles") || []
  const payload = store.getRootField("addEntrepreneurProfile")
  const profile = payload.getLinkedRecord("profile")

  viewer.setLinkedRecords([...profiles, profile], "profiles")
}

export default enhance({ mutation, updater })

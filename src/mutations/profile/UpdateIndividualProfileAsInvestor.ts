import { graphql } from "relay-runtime"
import enhance from "../enhance"

const mutation = graphql`
  mutation UpdateIndividualProfileAsInvestorMutation(
    $input: UpdateIndividualProfileAsInvestorInput!
  ) {
    updateIndividualProfileAsInvestor(input: $input) {
      profile {
        id
      }
    }
  }
`

export default enhance({ mutation })

import { graphql } from "relay-runtime"
import enhance from "../enhance"

const mutation = graphql`
  mutation ResetPasswordMutation($input: ResetPasswordInput!) {
    resetPassword(input: $input) {
      status
    }
  }
`

export default enhance({ mutation })

import styled from "styled-components"

export const MockItem = styled.div`
  height: 260px;
  width: 100%;
  margin: 0 0 20px;
  border: 1px solid ${({ theme }) => theme.colors.grey};
  background-color: ${({ theme }) => theme.colors.alabaster};
  border-radius: 5px;
`

import React from "react"
import memoize from "memoize-one"

import { MockItem } from "../access/styles"

const initCards = memoize(() => {
  const CARD_HEIGHT = 260

  const windowHeight = window.innerHeight
  const cardCount = Math.ceil(windowHeight / CARD_HEIGHT)

  return new Array(cardCount).fill((i) => <MockItem key={i} />)
})

export default () => initCards().map((cart, i) => cart(i))

import access from "./access"
import list from "./list"

const render = () => null

render.access = access
render.list = list

export default render

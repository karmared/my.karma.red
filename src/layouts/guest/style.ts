import styled from "styled-components"

export const Main = styled.div`
  color: ${({ theme }) => theme.colors.white};
  display: flex;
  flex-direction: column;
  height: 100%;

  min-width: 1024px;
`

export const HeaderContainer = styled.header``

export const ContentContainer = styled.div`
  display: flex;
  flex: 1;
  flex-direction: column;
`

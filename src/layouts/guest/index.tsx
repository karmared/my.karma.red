import React from "react"

import { Background, GuestPageHeader } from "src/components"

import { Main, HeaderContainer, ContentContainer } from "./style"

const render = ({ button, children }) => (
  <Main>
    <Background />
    <HeaderContainer>
      <GuestPageHeader button={button} />
    </HeaderContainer>
    <ContentContainer>{children}</ContentContainer>
  </Main>
)

export default render

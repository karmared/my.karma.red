import React from "react"

import {
  Box, Text, Flex, Link, Button, Translate,
} from "src/components"

import { StyledLink, IconContainer, TitleContainer } from "./styles"

const InvestIcon = () => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="40"
    height="44"
    viewBox="0 0 40 44"
  >
    <g fill="none" fillRule="evenodd" stroke="#FFF">
      <path strokeWidth="2" d="M32.357 15.149h6.547V1.504h-6.547z" />
      <path
        strokeLinejoin="round"
        strokeWidth="2"
        d="M32.3 4.633l-13.968-3.47a5.528 5.528 0 0 0-4.371.747L2.043 11.27c-.892.587-1.372 1.584-.784 2.477.588.893
        1.637.86 2.53.272l9.072-6.145 4.937-.512 3.188 3.191-9.776 4.173a1.937 1.937 0 0 0 1.546 3.55l6.729-2.929c1.842
        1.685 5.5 1.123 7.936.061l5.259-2.289"
      />
      <path
        strokeLinecap="round"
        strokeWidth="1.5"
        d="M24.368 31.56l3.5-3.5 3.5 3.5M27.898 28.611v7.5"
      />
      <path
        strokeWidth="2"
        d="M17.868 32.048c0-5.523 4.478-10 10-10s10 4.477 10 10c0 5.522-4.478 10-10 10s-10-4.478-10-10zM2.1
        17.313a4 4 0 1 1 8 0 4 4 0 0 1-8 0z"
      />
    </g>
  </svg>
)

const BorrowIcon = () => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="42"
    height="42"
    viewBox="0 0 42 42"
  >
    <g fill="none" fillRule="evenodd" stroke="#FFF">
      <path strokeWidth="2" d="M1 38.806h6.547V25.161H1z" />
      <path
        strokeWidth="2"
        d="M7.224 27.19h5.735c2.66 0 6.236.945 7.253 3.227h7.338a1.935 1.935 0 1 1 0 3.873h-8.388"
      />
      <path
        strokeWidth="2"
        d="M7.547 35.68l13.594 4.728a5.532 5.532 0 0 0 4.42-.346l13.328-7.46c.943-.504 1.51-1.453
        1.007-2.396-.504-.943-1.552-1.005-2.495-.5l-8.908 4.398"
      />
      <path
        strokeLinecap="round"
        strokeWidth="1.5"
        d="M34.042 14.755l-3.5 3.5-3.5-3.5M30.511 17.704v-7.498"
      />
      <path
        strokeWidth="2"
        d="M40.542 14.268c0 5.523-4.478 10-10 10-5.523 0-10-4.477-10-10 0-5.522 4.477-10 10-10 5.522 0 10 4.478 10 10z"
      />
      <path
        strokeLinejoin="round"
        strokeWidth="2"
        d="M17.41 5a4 4 0 1 1-8 0 4 4 0 0 1 8 0z"
      />
    </g>
  </svg>
)

const icons = {
  borrow: BorrowIcon,
  invest: InvestIcon,
}

const IntentionItem = (props) => {
  const { type } = props
  const Icon = icons[type]

  return (
    <Flex>
      <IconContainer>
        <Icon />
      </IconContainer>
      <Text color="white" fontSize="20px" lineHeight="1.3">
        <Translate i18n={`pages:guest-orders.actions.${type}`} />
      </Text>
    </Flex>
  )
}

IntentionItem.defaultProps = {
  type: "borrow",
}

export const Title = () => (
  <TitleContainer>
    <Box mb="20px">
      <Text
        color="white"
        fontSize="50px"
        fontWeight="bold"
        textTransform="uppercase"
      >
        <Translate i18n="pages:guest-orders.title" />
      </Text>
    </Box>
    <Flex>
      <Box width="225px" mr="35px">
        <IntentionItem type="invest" />
      </Box>
      <Box width="275px">
        <IntentionItem />
      </Box>
    </Flex>
  </TitleContainer>
)

export const RegisterButton = () => (
  <Link to="/join">
    <Button variant="red" height="40px">
      <Translate i18n="pages:guest-orders.buttons.register" />
    </Button>
  </Link>
)

export const LoginButton = () => (
  <Box mt="17px">
    <StyledLink to="/login">
      <Translate i18n="pages:guest-orders.buttons.login" />
    </StyledLink>
  </Box>
)

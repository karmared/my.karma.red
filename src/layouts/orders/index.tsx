import React from "react"

import { PageContainer, GuestPageHeader } from "src/components"

import { HeaderContainer } from "./styles"

import { Title, LoginButton, RegisterButton } from "./elements"

const Orders = (props) => {
  const { children } = props

  return (
    <>
      <HeaderContainer>
        <GuestPageHeader />
        <Title />
        <RegisterButton />
        <LoginButton />
      </HeaderContainer>
      <PageContainer>{children}</PageContainer>
    </>
  )
}

export default Orders

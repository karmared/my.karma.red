import styled from "styled-components"

import { Box, Link } from "src/components"

export const HeaderContainer = styled(Box)`
  position: relative;
  width: 100%;
  height: 500px;
  text-align: center;
  color: white;
  background-image: url(/images/backgrounds/guest-market.jpg);
  background-repeat: round;

  @media only screen and (-webkit-min-device-pixel-ratio: 2) {
    background-image: url(/images/backgrounds/guest-market@2x.jpg);
  }

  @media only screen and (min-device-pixel-ratio: 3) {
    background-image: url(/images/backgrounds/guest-market@3x.jpg);
  }
`

export const IconContainer = styled(Box)`
  position: relative;
  top: 3px;
  width: 40px;
  height: 40px;
  margin-right: 17px;
`

export const TitleContainer = styled.div`
  max-width: max-content;
  margin: 60px auto 85px auto;
`

export const StyledLink = styled(Link)`
  color: white;
  text-decoration: underline;
`

import styled from "styled-components"

export const Main = styled.div`
  align-items: center;
  background-color: ${({ theme }) => theme.colors.greyLighter};
  color: ${({ theme }) => theme.colors.black};
  display: flex;
  flex-direction: column;
  min-height: 100%;
  min-width: 1024px;
`

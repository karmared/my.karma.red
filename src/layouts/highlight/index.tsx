import React from "react"
import styled from "styled-components"

import { Box } from "src/components"

const wrap = (component) => styled(component)`
  align-items: center;
  display: flex;
  flex: 1;
  flex-direction: column;
  min-height: 100%;
`

const render = ({ children }) => {
  const Container = wrap(Box)
  return <Container bg="greyLighter">{children}</Container>
}

export default render

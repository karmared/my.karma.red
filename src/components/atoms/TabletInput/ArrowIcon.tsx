/* eslint-disable max-len */
import React from "react"
import styled from "styled-components"

const Icon = () => (
  <svg width="14" height="10" viewBox="0 0 14 10">
    <path
      fill="#FFF"
      fillRule="nonzero"
      d="M13 4H3.4l2.31-2.31A.948.948 0 0 0 6 1c0-.492-.406-1-1-1a.943.943 0 0 0-.69.29L.331 4.269C.167 4.433 0 4.636 0 5s.14.54.323.724L4.31 9.71c.193.194.424.29.69.29.594 0 1-.508 1-1a.948.948 0 0 0-.29-.69L3.4 6H13a1 1 0 0 0 0-2z"
    />
  </svg>
)

const IconContainer = styled.div<any>`
  width: 14px;
  height: 14px;
  transform: rotate(${({ reversed }) => (reversed ? 180 : 0)}deg);
`

export default React.memo(({ reversed }: any) => (
  <IconContainer reversed={reversed}>
    <Icon />
  </IconContainer>
))

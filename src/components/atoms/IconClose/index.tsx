import React from "react"

export default () => (
  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 14.584 14.584">
    <path transform="rotate(45 6.482 16.359)" d="M0 0h1.875v18.75H0z" />
    <path transform="rotate(-45 1.956 .516)" d="M0 0h1.875v18.75H0z" />
  </svg>
)

import November from "./November"
import Investor from "./Investor"

const render = () => null

render.november = November
render.investor = Investor

export default render

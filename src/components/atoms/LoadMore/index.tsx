import React from "react"
import { LoadMoreButton } from "./styles"

const LoadMore = (props) => <LoadMoreButton {...props} />

export default LoadMore

import React from "react"
import PhoneInput from "./style"

/* eslint-disable  no-useless-escape */
const phoneMask = "+ 7 ( 999 ) 999 99 99"

export default (props) => <PhoneInput mask={phoneMask} {...props} />

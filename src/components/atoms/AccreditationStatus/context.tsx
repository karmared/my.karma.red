import React from "react"

const StatusDataContext = React.createContext({})

export const AccreditationStatusProvider = StatusDataContext.Provider
export const AccreditationStatusConsumer = StatusDataContext.Consumer

import React from "react"
import { Redirect } from "react-router-dom"

import { setRedirectPath } from "src/utils"

const render = (props) => {
  const redirectPath = props.location.pathname + props.location.search
  setRedirectPath(redirectPath || "")
  return <Redirect to="/" />
}

export default render

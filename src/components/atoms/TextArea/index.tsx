import React from "react"
import TextArea from "./styles"

export default (props) => <TextArea {...props} />

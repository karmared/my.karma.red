import styled, { css } from "styled-components"

const state = (color) => css`
  border-color: ${color} !important;
  border-left-width: 4px;
  padding-left: 7px;
`

const TextArea = styled.textarea<any>`
  background-color: ${({ theme }) => theme.colors.white};
  border: 1px solid ${({ theme }) => theme.colors.grey};
  border-radius: 5px;
  height: 120px;
  width: 100%;
  max-width: 740px;
  margin: 0;
  padding: 12px 10px;
  resize: vertical;

  &::placeholder {
    color ${({ theme }) => theme.colors.greyDark};
  }

  &:focus {
    border-color: ${({ theme }) => theme.colors.greyDark};
  }

  &:disabled {
    opacity: .4;
  }

  ${({ error }) => error && state("red")};
  ${({ success }) => success && state("green")};
`

export default TextArea

import React from "react"
import List from "./List"
import Input from "./Input"

const render = (props) => {
  const { onRemove, attachments, ...rest } = props

  return (
    <React.Fragment>
      <Input {...rest} />
      <List attachments={attachments} onRemove={onRemove} />
    </React.Fragment>
  )
}

export default render

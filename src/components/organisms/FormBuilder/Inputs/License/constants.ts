const DefaultLicenseItem = {
  valid_date: "",
  license: false,
  license_name: "",
  license_date: "",
  license_number: "",
}

export default DefaultLicenseItem

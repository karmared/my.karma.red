import React from "react"

import {
  Box,
  Label,
  FormRow,
  FlexBox,
  Translate,
  RadioButton,
} from "src/components"

import { TENURE } from "src/constants"

class Tenure extends React.PureComponent<any, any> {
  onChange = (event) => {
    const { value, checked } = event.target

    if (checked) {
      this.props.onChange({
        name: this.props.name,
        value,
      })
    }
  }

  render() {
    return (
      <FormRow>
        <Box width={280}>
          <Label>
            <Translate
              i18n="application.worksheet.rows.right.label"
              ns="components"
            />
          </Label>
        </Box>
        <Box>
          {TENURE.map(({ label, value }, index) => (
            <FlexBox
              key={index}
              display="flex"
              flexDirection="row"
              justifyContnet="flex-start"
              alignItems="center"
              py="6px"
            >
              <RadioButton
                label={label}
                value={value}
                onChange={this.onChange}
                name={`tenure${this.props.index}`}
                checked={this.props.value === value}
              />
            </FlexBox>
          ))}
        </Box>
      </FormRow>
    )
  }
}

export default Tenure

import React from "react"

import {
  Box, Label, FormRow, Translate, TextField,
} from "src/components"

class Area extends React.Component<any, any> {
  onChange = (event) => {
    const { value } = event.target

    this.props.onChange({
      name: this.props.name,
      value,
    })
  }

  render() {
    return (
      <FormRow>
        <Box width={280}>
          <Label>
            <Translate
              i18n="application.worksheet.rows.area.label"
              ns="components"
            />
          </Label>
        </Box>
        <TextField
          value={this.props.value}
          onChange={this.onChange}
          name="area"
        />
      </FormRow>
    )
  }
}

export default Area

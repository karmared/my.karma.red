import React from "react"

import {
  Text, AddButton, Translate,
} from "src/components"
import { cloneDeep } from "src/utils"

import DefaultRoomItem from "./constants"
import { AddButtonWrapper } from "../../styles"
import { RemoveButton } from "../../elements"

import RoomItem from "./RoomItem"

export class RoomCharacteristics extends React.PureComponent<any, any> {
  onChange = (index) => ({ value }) => {
    const roomItems = cloneDeep(this.props.value)
    roomItems[index] = value
    this.forceChange(roomItems)
  }

  onRemove = (index) => () => {
    const roomItems = [...this.props.value]
    roomItems.splice(index, 1)
    this.forceChange(roomItems)
  }

  onAdd = (event) => {
    if (event) {
      event.preventDefault()
    }

    const roomItems = [...this.props.value, cloneDeep(DefaultRoomItem)]
    this.forceChange(roomItems)
  }

  forceChange = (roomItems) => {
    this.props.onChange({
      name: this.props.name,
      value: roomItems,
    })
  }

  render() {
    return (
      <React.Fragment>
        {this.props.value.map((item, index) => (
          <React.Fragment key={index}>
            {index > 0 && <RemoveButton onClick={this.onRemove(index)} />}
            <RoomItem
              value={item}
              key={index}
              index={index}
              onChange={this.onChange(index)}
            />
          </React.Fragment>
        ))}
        <AddButtonWrapper>
          <AddButton onClick={this.onAdd}>
            <Text>
              <Translate
                i18n="application.worksheet.switch_values.placement_purpose.add_placement"
                ns="components"
              />
            </Text>
          </AddButton>
        </AddButtonWrapper>
      </React.Fragment>
    )
  }
}

export default RoomCharacteristics

import React from "react"

const InputsDataContext = React.createContext({})

export const InputsDataProvider = InputsDataContext.Provider
export const InputsDataConsumer = InputsDataContext.Consumer

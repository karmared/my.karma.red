import React from "react"

import { FloatingLabelInput } from "src/components"
import { preventEventBubbling } from "src/utils"

import Container from "./styles"

/*
*
*
  region: null, area: null,
  locality: null, street: null,

  house: null,  housing: null, structure: null, flat: null,
*
*
* */

const inputsSchema = [
  {
    name: "region",
    width: "330px",
  },
  {
    name: "area",
    width: "330px",
  },
  {
    name: "locality",
    width: "330px",
  },
  {
    name: "street",
    width: "330px",
  },
  {
    name: "house",
    width: "145px",
    margin: "0 20px 20px 0",
  },
  {
    name: "housing",
    width: "145px",
    margin: "0 20px 20px 0",
  },
  {
    name: "structure",
    width: "145px",
    margin: "0 20px 20px 0",
  },
  {
    name: "flat",
    width: "145px",
    margin: "0 auto 20px 0",
  },
]

const AddressInput = (props) => {
  const { name, onChange } = props

  const changeHandler = (event) => {
    preventEventBubbling(event)

    const injectedValue = { ...props.value }
    const { name: inputName, value } = event.target

    injectedValue[inputName] = value

    onChange({
      name,
      value: injectedValue,
    })
  }

  return (
    <Container>
      {inputsSchema.map(({ name: inputName, width, margin }, index) => (
        <FloatingLabelInput
          key={index + inputName}
          name={inputName}
          value={props.value[inputName]}
          onChange={changeHandler}
          label={`components:accreditation.individual.rows.present_registration.row_hints.${inputName}`}
          width={width}
          margin={margin}
        />
      ))}
    </Container>
  )
}

export default AddressInput

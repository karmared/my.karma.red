import styled from "styled-components"

const InfoLinkWrapper = styled.div`
  margin-top: -22px;
  padding-bottom: 28px;
`

export default InfoLinkWrapper

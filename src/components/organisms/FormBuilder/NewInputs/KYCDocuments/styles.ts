import styled from "styled-components"

const InfoLinkWrapper = styled.div`
  margin: -22px 0 20px;
  border-bottom: 1px solid ${({ theme }) => theme.colors.grey};
  padding-bottom: 28px;
`

export default InfoLinkWrapper

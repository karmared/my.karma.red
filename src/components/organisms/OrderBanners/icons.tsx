import React from "react"

export const Arrow = () => (
  <svg
    width="14"
    height="23"
    viewBox="0 0 14 23"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M0.390411 19.7237L8.59625 11.5L0.390411 3.27625L2.91666 0.75L13.6667 11.5L2.91666 22.25L0.390411 19.7237Z"
      fill="white"
    />
  </svg>
)

export const Dot = (props) => (
  <svg
    style={{ margin: 5 }}
    width="9"
    height="9"
    viewBox="0 0 9 9"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <circle
      cx="4.5"
      cy="4.5"
      r="4.5"
      transform="rotate(180 4.5 4.5)"
      fill={props.isDotsActive ? "#818181" : "#F1F1F1"}
    />
  </svg>
)

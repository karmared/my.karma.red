import { pick } from "src/utils"

const getBankAccountInput = (flatAccount) => {
  if (!flatAccount) return null

  const {
    name, IBAN, SWIFT, address, ownerName,
  } = flatAccount

  return {
    IBAN,
    ownerName,
    bank: {
      name,
      SWIFT,
      address,
    },
  }
}

export const getUpdateVariables = (account) => ({
  input: {
    accountId: account.id,
    attributes: getBankAccountInput(account),
  },
})

export const getCreateVariables = (account, profileId) => ({
  input: {
    profileId,
    attributes: getBankAccountInput(account),
  },
})

export const getValidity = (form) => {
  const fieldsToCheck = ["IBAN", "ownerName", "name", "SWIFT", "address"]
  return Object.keys(pick(form, fieldsToCheck)).every((key) => !!form[key])
}

const flattenReduceFunc = (data) => (accum, key) => (typeof data[key] === "object"
  ? { ...accum, ...data[key] }
  : { ...accum, [key]: data[key] })

export const flatten = (data) => Object.keys(data).reduce(flattenReduceFunc(data), {})

export const clearFormError = (clearError) => (path) => (...rest) => {
  clearError(path)

  return rest
}

import React from "react"

import { BankAccountStatuses } from "src/constants"
import { Box, Text, Translate } from "src/components"

import { StyledText } from "../styles"

const statusColor = (status) => (status === BankAccountStatuses.approved ? "green" : "red")

const render = ({ edit, status }) => (
  <Box mb="25px">
    <Box mb="10px">
      <Text fontSize="16px" fontWeight="500" lineHeight="1.5">
        <Translate
          i18n={`components:account.bank_account.info.${
            edit ? "requisites_edit" : "requisites"
          }`}
        />
      </Text>
      {!edit && (
        <Text color={statusColor(status)} fontSize="14px" lineHeight="1.15">
          <Translate
            i18n={`account.bank_account.info.status.${status}`}
            ns="components"
          />
        </Text>
      )}
    </Box>
    <StyledText>
      <Translate ns="components" i18n="account.bank_account.edit.info" />
    </StyledText>
  </Box>
)

export default React.memo(render)

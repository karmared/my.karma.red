export default [
  {
    name: "ownerName",
    errorPath: "ownerName",
  },
  {
    name: "name",
    errorPath: "bank.name",
  },
  {
    name: "address",
    errorPath: "bank.address",
  },
  {
    name: "SWIFT",
    errorPath: "bank.SWIFT",
  },
  {
    name: "IBAN",
    errorPath: "IBAN",
  },
]

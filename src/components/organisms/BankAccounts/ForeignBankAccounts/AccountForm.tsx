import React from "react"

import {
  Box,
  Button,
  Translate,
  BankDataSuggestion,
  FloatingLabelInput as FormInput,
} from "src/components"

import { compose } from "src/utils"

import inputs from "./inputs"
import { clearFormError } from "./utils"

import { InputsContainer } from "../styles"

const AccountForm = (props: any) => {
  const {
    busy,
    valid,
    account,
    onClick,
    onChange,
    getError,
    clearError,
    onSuggestionSelected,
  } = props

  const buttonKey = account.id ? "edit" : "save"
  const errorPrefix = account.id
    ? "updateForeignBankAccount"
    : "addForeignProfileBankAccount"
  const clear = clearFormError(clearError)

  return (
    <>
      <InputsContainer>
        {inputs.map((input: any) => {
          const {
            name, suggest, errorPath, daDataSelector,
          } = input

          const path = `${errorPrefix}.${errorPath}`
          const onChangeDecorated = compose(clear(path), onChange)

          return suggest ? (
            <BankDataSuggestion
              key={name}
              name={name}
              value={account[name]}
              error={getError(path)}
              onChange={onChangeDecorated}
              daDataSelector={daDataSelector}
              label={`components:account.bank_account.edit.placeholders.${name}`}
              renderInputComponent={(inputProps) => (
                <FormInput {...inputProps} />
              )}
              onSuggestionSelected={onSuggestionSelected}
            />
          ) : (
            <FormInput
              key={name}
              name={name}
              value={account[name]}
              error={getError(path)}
              onChange={onChangeDecorated}
              label={`components:account.bank_account.edit.placeholders.${name}`}
            />
          )
        })}
      </InputsContainer>
      <Box>
        <Button variant="blueWide" disabled={!valid || busy} onClick={onClick}>
          <Translate
            ns="components"
            i18n={`account.bank_account.edit.buttons.${buttonKey}`}
          />
        </Button>
      </Box>
    </>
  )
}

export default AccountForm

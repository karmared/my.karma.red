import React from "react"

import { BankAccountStatuses } from "src/constants"

import {
  Box, Flex, Text, Button, Translate,
} from "src/components"

const isApproved = (status) => status === BankAccountStatuses.approved

const BankDataRow = ({ name, value }) => (
  <Flex alignItems="center" mb="15px">
    <Box width="210px">
      <Text fontSize="11px" color="greyShuttle" textTransform="uppercase">
        <Translate i18n={name} />
      </Text>
    </Box>
    <Text fontSize="16px" color="greyShuttle">
      {value}
    </Text>
  </Flex>
)

const render = ({ account, onEdit, onConfirm }) => (
  <Box mt="12px">
    <BankDataRow
      value={account.ownerName}
      name="components:account.bank_account.info.account_info.ownerName"
    />
    <BankDataRow
      value={account.name}
      name="components:account.bank_account.info.account_info.name"
    />
    <BankDataRow
      value={account.address}
      name="components:account.bank_account.info.account_info.address"
    />
    <BankDataRow
      value={account.SWIFT}
      name="components:account.bank_account.info.account_info.SWIFT"
    />
    <BankDataRow
      value={account.IBAN}
      name="components:account.bank_account.info.account_info.IBAN"
    />
    <Flex mt="40px">
      {!isApproved(account.status) && (
        <Box mr="20px">
          <Button variant="default" onClick={onConfirm}>
            <Translate i18n="components:account.bank_account.edit.buttons.confirm" />
          </Button>
        </Box>
      )}
      <Button variant="default" onClick={onEdit}>
        <Translate i18n="components:account.bank_account.edit.buttons.edit" />
      </Button>
    </Flex>
  </Box>
)

export default React.memo(render)

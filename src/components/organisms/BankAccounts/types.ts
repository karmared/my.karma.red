export type ConfirmationType = ConfirmationMainType & {
  clearTimer: () => void;
  commit: (value: any) => void;
  restartTimer: (value?: boolean) => void;
  secondsLeft: number;
  startTimer: () => void;
}

export type ConfirmationMainType = {
  accountId: string;
  clearError: (value: string) => void;
  environment: any;
  getError: (value: string) => void;
  onClose: () => void;
  onCompleted: () => void;
  profileId: string;
}

import React from "react"
import { Translate } from "../../molecules"

/* eslint-disable max-len */
const tables = {
  debtor: {
    header: [
      {
        label: (
          <Translate i18n="components:scoring_info.borrower.table.headers.0" />
        ),
      },
      {
        label: (
          <Translate i18n="components:scoring_info.borrower.table.headers.1" />
        ),
      },
    ],
    body: [
      {
        type: "info",
        columns: [
          {
            label: (
              <Translate i18n="components:scoring_info.borrower.table.rows.0.0" />
            ),
          },
          {
            label: (
              <Translate i18n="components:scoring_info.borrower.table.rows.0.1" />
            ),
          },
        ],
      },
      {
        type: "info",
        columns: [
          {
            label: (
              <Translate i18n="components:scoring_info.borrower.table.rows.1.0" />
            ),
          },
          {
            label: (
              <Translate i18n="components:scoring_info.borrower.table.rows.1.1" />
            ),
          },
        ],
      },
      {
        type: "info",
        columns: [
          {
            label: (
              <Translate i18n="components:scoring_info.borrower.table.rows.2.0" />
            ),
          },
          {
            label: (
              <Translate i18n="components:scoring_info.borrower.table.rows.2.1" />
            ),
          },
        ],
      },
      {
        type: "info",
        columns: [
          {
            label: (
              <Translate i18n="components:scoring_info.borrower.table.rows.3.0" />
            ),
          },
          {
            label: (
              <Translate i18n="components:scoring_info.borrower.table.rows.3.1" />
            ),
          },
        ],
      },
      {
        type: "warning",
        columns: [
          {
            label: (
              <Translate i18n="components:scoring_info.borrower.table.rows.4.0" />
            ),
          },
          {
            label: (
              <Translate i18n="components:scoring_info.borrower.table.rows.4.1" />
            ),
          },
        ],
      },
      {
        type: "warning",
        columns: [
          {
            label: (
              <Translate i18n="components:scoring_info.borrower.table.rows.5.0" />
            ),
          },
          {
            label: (
              <Translate i18n="components:scoring_info.borrower.table.rows.5.1" />
            ),
          },
        ],
      },
      {
        type: "danger",
        columns: [
          {
            label: (
              <Translate i18n="components:scoring_info.borrower.table.rows.6.0" />
            ),
          },
          {
            label: (
              <Translate i18n="components:scoring_info.borrower.table.rows.6.1" />
            ),
          },
        ],
      },
      {
        type: "danger",
        columns: [
          {
            label: (
              <Translate i18n="components:scoring_info.borrower.table.rows.7.0" />
            ),
          },
          {
            label: (
              <Translate i18n="components:scoring_info.borrower.table.rows.7.1" />
            ),
          },
        ],
      },
      {
        type: "danger",
        columns: [
          {
            label: (
              <Translate i18n="components:scoring_info.borrower.table.rows.8.0" />
            ),
          },
          {
            label: (
              <Translate i18n="components:scoring_info.borrower.table.rows.8.1" />
            ),
          },
        ],
      },
      {
        type: "critical",
        columns: [
          {
            label: (
              <Translate i18n="components:scoring_info.borrower.table.rows.9.0" />
            ),
          },
          {
            label: (
              <Translate i18n="components:scoring_info.borrower.table.rows.9.1" />
            ),
          },
        ],
      },
    ],
  },
  provision: {
    header: [
      {
        label: (
          <Translate i18n="components:scoring_info.collateral.table.headers.0" />
        ),
      },
      {
        label: (
          <Translate i18n="components:scoring_info.collateral.table.headers.1" />
        ),
      },
    ],
    body: [
      {
        type: "info",
        columns: [
          {
            label: (
              <Translate i18n="components:scoring_info.collateral.table.rows.0.0" />
            ),
          },
          {
            label: [
              <Translate i18n="components:scoring_info.collateral.table.rows.0.1" />,
              <Translate i18n="components:scoring_info.collateral.table.rows.0.2" />,
            ],
          },
        ],
      },
      {
        type: "info",
        columns: [
          {
            label: (
              <Translate i18n="components:scoring_info.collateral.table.rows.1.0" />
            ),
          },
          {
            label: [
              <Translate i18n="components:scoring_info.collateral.table.rows.1.1" />,
            ],
          },
        ],
      },
      {
        type: "info",
        columns: [
          {
            label: (
              <Translate i18n="components:scoring_info.collateral.table.rows.2.0" />
            ),
          },
          {
            label: [
              <Translate i18n="components:scoring_info.collateral.table.rows.2.1" />,
              <Translate i18n="components:scoring_info.collateral.table.rows.2.2" />,
            ],
          },
        ],
      },
      {
        type: "info",
        columns: [
          {
            label: (
              <Translate i18n="components:scoring_info.collateral.table.rows.3.0" />
            ),
          },
          {
            label: [
              <Translate i18n="components:scoring_info.collateral.table.rows.3.1" />,
              <Translate i18n="components:scoring_info.collateral.table.rows.3.2" />,
              <Translate i18n="components:scoring_info.collateral.table.rows.3.3" />,
            ],
          },
        ],
      },
      {
        type: "warning",
        columns: [
          {
            label: (
              <Translate i18n="components:scoring_info.collateral.table.rows.4.0" />
            ),
          },
          {
            label: [
              <Translate i18n="components:scoring_info.collateral.table.rows.4.1" />,
              <Translate i18n="components:scoring_info.collateral.table.rows.4.2" />,
              <Translate i18n="components:scoring_info.collateral.table.rows.4.3" />,
            ],
          },
        ],
      },
      {
        type: "warning",
        columns: [
          {
            label: (
              <Translate i18n="components:scoring_info.collateral.table.rows.5.0" />
            ),
          },
          {
            label: [
              <Translate i18n="components:scoring_info.collateral.table.rows.5.1" />,
              <Translate i18n="components:scoring_info.collateral.table.rows.5.2" />,
            ],
          },
        ],
      },
      {
        type: "danger",
        columns: [
          {
            label: (
              <Translate i18n="components:scoring_info.collateral.table.rows.6.0" />
            ),
          },
          {
            label: [
              <Translate i18n="components:scoring_info.collateral.table.rows.6.1" />,
              <Translate i18n="components:scoring_info.collateral.table.rows.6.2" />,
            ],
          },
        ],
      },
      {
        type: "danger",
        columns: [
          {
            label: (
              <Translate i18n="components:scoring_info.collateral.table.rows.7.0" />
            ),
          },
          {
            label: [
              <Translate i18n="components:scoring_info.collateral.table.rows.7.1" />,
              <Translate i18n="components:scoring_info.collateral.table.rows.7.2" />,
            ],
          },
        ],
      },
      {
        type: "danger",
        columns: [
          {
            label: (
              <Translate i18n="components:scoring_info.collateral.table.rows.8.0" />
            ),
          },
          {
            label: [
              <Translate i18n="components:scoring_info.collateral.table.rows.8.1" />,
              <Translate i18n="components:scoring_info.collateral.table.rows.8.2" />,
              <Translate i18n="components:scoring_info.collateral.table.rows.8.3" />,
              <Translate i18n="components:scoring_info.collateral.table.rows.8.4" />,
            ],
          },
        ],
      },
      {
        type: "critical",
        columns: [
          {
            label: (
              <Translate i18n="components:scoring_info.collateral.table.rows.9.0" />
            ),
          },
          {
            label: [
              <Translate i18n="components:scoring_info.collateral.table.rows.9.1" />,
              <Translate i18n="components:scoring_info.collateral.table.rows.9.2" />,
              <Translate i18n="components:scoring_info.collateral.table.rows.9.3" />,
              <Translate i18n="components:scoring_info.collateral.table.rows.9.4" />,
            ],
          },
        ],
      },
    ],
  },
}

export default tables

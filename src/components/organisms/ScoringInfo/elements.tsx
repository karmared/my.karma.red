import React from "react"

import styled from "styled-components"

import { Box, Text, Heading as HeadingEl } from "src/components"

export const Bold = styled.span`
  font-weight: 600;
  color: ${({ theme }) => theme.colors.black};
`

export const Aside = styled.span`
  color: ${({ theme }) => theme.colors.greyDarkest};
  font-size: 13px;
  text-align: left;
  line-height: 20px;
  position: relative;
  margin-top: 8px;
  padding-top: 20px;
  display: inline-block;

  &:before {
    position: absolute;
    content: "";
    top: -1px;
    width: calc(100vw - 100px);
    left: 0;
    height: 1px;
    background-color: ${({ theme }) => theme.colors.greyDarkest};
  }
`

export const SubHeader = styled.h6`
  font-size: 16px;
  line-height: 1.5;
  font-weight: 600;
  margin: 0 0 10px;
  padding: 6px 0 16px;
  margin: 0;
  color: ${({ theme }) => theme.colors.greyDarkest};
`

export const UnorderedList = styled.ul`
  margin: 6px 0 26px;
  padding-left: 24px;
  list-style: none;
  line-height: 24px;
  font-size: 16px;

  li {
    color: ${({ theme }) => theme.colors.greyDarkest};
    position: relative;
  }

  li::before {
    content: "•";
    position: absolute;
    left: -24px;
    font-size: 16px;
    padding-right: 0;
    color: ${({ theme }) => theme.colors.ginger};
  }
`

export const TableContainer = styled.div`
  padding: 28px 0 38px;
`

export const InfoText = (props) => (
  <Text
    color="greyDarkest"
    fontSize={16}
    textAlign="left"
    lineHeight={1.5}
    {...props}
  >
    {props.children}
  </Text>
)

export const TextBox = (props) => (
  <Box mb={20} {...props}>
    {props.children}
  </Box>
)

export const Heading = (props) => (
  <Box mb={20}>
    <HeadingEl.h2 fontSize={28} {...props}>
      {props.children}
    </HeadingEl.h2>
  </Box>
)

export const SubHeading = (props) => (
  <HeadingEl.h4 fontSize={16} fontWeight="700" {...props}>
    {props.children}
  </HeadingEl.h4>
)

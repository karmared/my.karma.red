import React from "react"

import { Text } from "src/components"

import { Row, List } from "./styles"

export const HeadCell = (props) => (
  <th>
    <Text
      color="#0b1016"
      fontSize={14}
      fontWeight="600"
      textAlign="left"
      {...props}
    >
      {props.children}
    </Text>
  </th>
)

export const RowTitleCell = (props) => (
  <td>
    <Text
      color="#7b838b"
      fontSize={15}
      fontWeight="normal"
      textAlign="left"
      {...props}
    >
      {props.children}
    </Text>
  </td>
)

export const Cell = (props) => (
  <td>
    <Text
      color="#0b1016"
      fontSize={15}
      fontWeight={props.accent ? "500" : "normal"}
      textAlign="left"
      whiteSpace="pre-line"
      lineHeight={1.33}
      {...props}
    >
      {props.children}
    </Text>
  </td>
)

const colors = {
  info: "rgba(91, 171, 58, 0.1)",
  warning: "rgba(255, 215, 0, 0.1)",
  danger: "rgba(226, 37, 28, 0.1)",
  critical: "rgba(226, 37, 28, 0.25)",
}

export const TableRow = (props) => (
  <Row background={colors[props.type || "info"]} {...props}>
    {props.children}
  </Row>
)

export const ListCell = (props) => (
  <td>
    <List {...props}>
      {props.items.map((item, idx) => (
        <li key={idx}>
          <Text
            color="#0b1016"
            fontSize={14}
            fontWeight="normal"
            textAlign="left"
            lineHeight={1.33}
            {...props}
          >
            {item}
          </Text>
        </li>
      ))}
    </List>
  </td>
)

import styled from "styled-components"
import { background } from "styled-system"

export const TableContainer = styled.table`
  border-top: 1px solid #d9dee2;
  width: 100%;
  border-collapse: collapse;

  td,
  th {
    padding: 12px 28px 12px 0;
    vertical-align: top;
  }

  td {
    border-top: 1px solid #d9dee2;
  }
`

export const TableHeader = styled.thead`
  tr th:last-child {
    width: 100%;
  }
`

export const Row = styled.tr`
  ${background}

  td:first-child {
    width: 100px;
    padding-left: 15px;
  }

  td:nth-child(2) {
    width: 200px;
  }

  td:last-child {
    width: 100%;
  }
`

export const List = styled.ul`
  margin: 0;
  padding-left: 17px;
`

import React from "react"

import { TableHeader, TableContainer } from "./styles"

import {
  Cell, TableRow, HeadCell, ListCell,
} from "./elements"

export default (props) => (
  <TableContainer>
    <TableHeader>
      <tr>
        {props.schema.header.map((item, idx) => (
          <HeadCell key={idx}>{item.label}</HeadCell>
        ))}
      </tr>
    </TableHeader>
    <tbody>
      {props.schema.body.map((row, bodyIndex) => (
        <TableRow key={bodyIndex} type={row.type}>
          {row.columns.map((item, rowIndex) => (typeof item.label === "string"
            || React.isValidElement(item.label) ? (
              <Cell key={rowIndex}>{item.label}</Cell>
            ) : (
              <ListCell key={rowIndex} items={item.label} />
            )))}
        </TableRow>
      ))}
    </tbody>
  </TableContainer>
)

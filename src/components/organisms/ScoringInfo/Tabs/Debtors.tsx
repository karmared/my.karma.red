import React from "react"

import { Translate } from "src/components"

import Builder from "../TableBuilder"
import tableScheema from "../tables"

import {
  Bold, Aside, TextBox, InfoText, TableContainer,
} from "../elements"

/* eslint-disable max-len */
export default () => (
  <>
    <TextBox>
      <InfoText>
        <Bold>
          <Translate i18n="components:scoring_info.borrower.contents.0" />
        </Bold>
        &nbsp;
        <Translate i18n="components:scoring_info.borrower.contents.1" />
      </InfoText>
    </TextBox>
    <TextBox>
      <InfoText>
        <Translate i18n="components:scoring_info.borrower.contents.2" />
      </InfoText>
    </TextBox>
    <TextBox>
      <InfoText>
        <Bold>
          <Translate i18n="components:scoring_info.borrower.contents.3" />
        </Bold>
      </InfoText>
    </TextBox>
    <TextBox>
      <InfoText>
        <Translate i18n="components:scoring_info.borrower.contents.4" />
      </InfoText>
    </TextBox>
    <TextBox>
      <InfoText>
        <Translate i18n="components:scoring_info.borrower.contents.5" />
      </InfoText>
    </TextBox>
    <TextBox>
      <InfoText>
        <Translate i18n="components:scoring_info.borrower.contents.6" />
      </InfoText>
    </TextBox>
    <TextBox>
      <InfoText>
        <Translate i18n="components:scoring_info.borrower.contents.7" />
      </InfoText>
    </TextBox>
    <TextBox>
      <InfoText>
        <Translate i18n="components:scoring_info.borrower.contents.8" />
      </InfoText>
    </TextBox>
    <TableContainer>
      <Builder schema={tableScheema.debtor} />
    </TableContainer>
    <TextBox>
      <Aside>
        <Translate i18n="components:scoring_info.borrower.contents.9" />
      </Aside>
    </TextBox>
  </>
)

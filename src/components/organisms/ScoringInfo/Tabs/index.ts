import Debtors from "./Debtors"
import Provision from "./Provision"

const render = () => null

render.debtors = Debtors
render.provision = Provision

export default render

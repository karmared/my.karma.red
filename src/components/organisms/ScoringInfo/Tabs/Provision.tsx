import React from "react"

import { Box, Heading, Translate } from "src/components"

import Builder from "../TableBuilder"
import tableScheema from "../tables"

import {
  Bold,
  Aside,
  TextBox,
  SubHeader,
  InfoText,
  UnorderedList,
  TableContainer,
} from "../elements"

/* eslint-disable max-len */
export default () => (
  <>
    <TextBox>
      <InfoText>
        <Bold>
          <Translate i18n="components:scoring_info.collateral.common.0" />
        </Bold>
        &nbsp;
        <Translate i18n="components:scoring_info.collateral.common.1" />
      </InfoText>
      <UnorderedList>
        <li>
          <Translate i18n="components:scoring_info.collateral.common.2" />
        </li>
        <li>
          <Translate i18n="components:scoring_info.collateral.common.3" />
        </li>
        <li>
          <Translate i18n="components:scoring_info.collateral.common.4" />
        </li>
      </UnorderedList>
    </TextBox>
    <TextBox>
      <InfoText>
        <Translate i18n="components:scoring_info.collateral.common.5" />
      </InfoText>
    </TextBox>
    <TextBox>
      <InfoText>
        <Translate i18n="components:scoring_info.collateral.common.6" />
      </InfoText>
    </TextBox>
    <TextBox>
      <InfoText>
        <Translate i18n="components:scoring_info.collateral.common.7" />
      </InfoText>
    </TextBox>
    <TextBox>
      <SubHeader>
        <Translate i18n="components:scoring_info.collateral.characteristics.0" />
      </SubHeader>
      <InfoText>
        <Translate i18n="components:scoring_info.collateral.characteristics.1" />
      </InfoText>
    </TextBox>
    <TextBox>
      <InfoText>
        <Bold>
          <Translate i18n="components:scoring_info.collateral.characteristics.2" />
        </Bold>
        &nbsp;
        <Translate i18n="components:scoring_info.collateral.characteristics.3" />
      </InfoText>
    </TextBox>
    <TextBox>
      <InfoText>
        <Translate i18n="components:scoring_info.collateral.characteristics.4" />
      </InfoText>
      <UnorderedList>
        <li>
          <Translate i18n="components:scoring_info.collateral.characteristics.5" />
        </li>
        <li>
          <Translate i18n="components:scoring_info.collateral.characteristics.6" />
        </li>
        <li>
          <Translate i18n="components:scoring_info.collateral.characteristics.7" />
        </li>
        <li>
          <Translate i18n="components:scoring_info.collateral.characteristics.8" />
        </li>
      </UnorderedList>
    </TextBox>
    <TextBox>
      <InfoText>
        <Bold>
          <Translate i18n="components:scoring_info.collateral.characteristics.9" />
        </Bold>
        &nbsp;
        <Translate i18n="components:scoring_info.collateral.characteristics.10" />
      </InfoText>
    </TextBox>
    <TextBox>
      <InfoText>
        <Translate i18n="components:scoring_info.collateral.characteristics.11" />
      </InfoText>
      <UnorderedList>
        <li>
          <Translate i18n="components:scoring_info.collateral.characteristics.12" />
        </li>
        <li>
          <Translate i18n="components:scoring_info.collateral.characteristics.13" />
        </li>
        <li>
          <Translate i18n="components:scoring_info.collateral.characteristics.14" />
        </li>
        <li>
          <Translate i18n="components:scoring_info.collateral.characteristics.15" />
        </li>
      </UnorderedList>
    </TextBox>
    <TextBox>
      <InfoText>
        <Bold>
          <Translate i18n="components:scoring_info.collateral.characteristics.16" />
        </Bold>
        &nbsp;
        <Translate i18n="components:scoring_info.collateral.characteristics.17" />
      </InfoText>
    </TextBox>
    <TextBox>
      <InfoText>
        <Translate i18n="components:scoring_info.collateral.characteristics.18" />
      </InfoText>
      <UnorderedList>
        <li>
          <Translate i18n="components:scoring_info.collateral.characteristics.19" />
        </li>
        <li>
          <Translate i18n="components:scoring_info.collateral.characteristics.20" />
        </li>
        <li>
          <Translate i18n="components:scoring_info.collateral.characteristics.21" />
        </li>
        <li>
          <Translate i18n="components:scoring_info.collateral.characteristics.22" />
        </li>
      </UnorderedList>
    </TextBox>
    <TableContainer>
      <Builder schema={tableScheema.provision} />
    </TableContainer>
    <Box mb={20}>
      <Heading.h6>
        <Translate i18n="components:scoring_info.collateral.ending.0" />
      </Heading.h6>
    </Box>
    <TextBox>
      <InfoText>
        <Translate i18n="components:scoring_info.collateral.ending.1" />
      </InfoText>
      <InfoText>
        <Translate i18n="components:scoring_info.collateral.ending.2" />
      </InfoText>
    </TextBox>
    <SubHeader>
      <Translate i18n="components:scoring_info.collateral.ending.3" />
    </SubHeader>
    <TextBox>
      <InfoText>
        <Translate i18n="components:scoring_info.collateral.ending.4" />
      </InfoText>
    </TextBox>
    <SubHeader>
      <Translate i18n="components:scoring_info.collateral.ending.5" />
    </SubHeader>
    <TextBox>
      <InfoText>
        <Translate i18n="components:scoring_info.collateral.ending.6" />
      </InfoText>
    </TextBox>
    <TextBox>
      <InfoText>
        <Translate i18n="components:scoring_info.collateral.ending.7" />
      </InfoText>
    </TextBox>
    <TextBox>
      <InfoText>
        <Translate i18n="components:scoring_info.collateral.ending.8" />
      </InfoText>
      <UnorderedList>
        <li>
          <Translate i18n="components:scoring_info.collateral.ending.9" />
        </li>
        <li>
          <Translate i18n="components:scoring_info.collateral.ending.10" />
        </li>
        <li>
          <Translate i18n="components:scoring_info.collateral.ending.11" />
        </li>
        <li>
          <Translate i18n="components:scoring_info.collateral.ending.12" />
        </li>
      </UnorderedList>
    </TextBox>
    <SubHeader>
      <Translate i18n="components:scoring_info.collateral.ending.13" />
    </SubHeader>
    <TextBox>
      <InfoText>
        <Translate i18n="components:scoring_info.collateral.ending.14" />
      </InfoText>
    </TextBox>
    <TextBox>
      <Aside>
        <Translate i18n="components:scoring_info.collateral.ending.15" />
      </Aside>
    </TextBox>
  </>
)

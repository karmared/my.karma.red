export function removeDots(val) {
  const str = val.toString()

  if (!val || !str) {
    return ""
  }

  return str.replace(/\./gim, "")
}

import React from "react"

import { Box, Text, Translate } from "src/components"

const Pastdue = () => (
  <Box>
    <Text fontSize={16} color="red">
      <Translate
        ns="components"
        i18n="account.order_item.pastdue_status.header"
      />
    </Text>
  </Box>
)

export default Pastdue

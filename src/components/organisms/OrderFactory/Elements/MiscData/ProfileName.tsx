import React from "react"

import { Title } from "src/components"

export default ({ profile }) => (profile && profile.name ? <Title>{profile.name}</Title> : null)

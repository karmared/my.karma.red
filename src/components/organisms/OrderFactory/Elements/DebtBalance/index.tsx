import React from "react"

import {
  Box, Text, Flex, Tooltip, Translate,
} from "src/components"

import { number } from "src/utils"

import InfoGlyph from "./styles"

const options = {
  style: "currency",
  currency: "RUB",
  minimumFractionDigits: 0,
  maximumFractionDigits: 0,
}

const DebtBalance = (props) => {
  const { amount } = props

  return (
    <Flex>
      <Text fontSize="16px" color="greyDarker">
        <Translate i18n="components:order_list.approval.debt_balance" />
      </Text>
      <Tooltip
        alignLeft
        borderColor="grey"
        borderWidth="2px"
        glyph={InfoGlyph}
        text="components:order_list.approval.debt_balance_tooltip"
      />
      <Box ml="10px">
        <Text fontSize="16px" fontWeight="500">
          {number(amount, options)}
        </Text>
      </Box>
    </Flex>
  )
}

export default DebtBalance

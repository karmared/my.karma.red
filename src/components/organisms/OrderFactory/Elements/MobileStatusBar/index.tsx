import React from "react"
import { StatusbarContainer, StatusBarFilled, StatusBarEmpty } from "./styles"

export default (props) => (
  <StatusbarContainer margin={props.margin} width={props.width}>
    <StatusBarFilled value={props.value} />
    <StatusBarEmpty />
  </StatusbarContainer>
)

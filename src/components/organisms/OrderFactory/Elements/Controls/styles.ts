import styled from "styled-components"

export const ControlsContainer = styled.div`
  margin: auto 0 0 auto;
`

export const Link = styled.a`
  text-decoration: none;
`

/* eslint-disable no-undef */
import { getIsDisabledButton } from "./getIsDisabledButton"

describe("Amount more button test", () => {
  it("investor with initial investment can invest if delta > 10 000", () => {
    const order = {
      application: {
        data: { maxValue: "10000000", minValue: "1000000" },
      },
      chain: {
        gatheredAmount: 9900000,
        investorsCount: 8,
      },
      offer: {
        data: {},
      },
      status: "CONFIRMED",
      viewer: {
        investedAmount: 10000,
      },
    }
    const profile = { investor: {} }
    expect(getIsDisabledButton(profile, order)).toBe(false)
  })

  it("investor with initial investment can invest if delta < 10 000", () => {
    const order = {
      application: {
        data: { maxValue: "10000000", minValue: "1000000" },
      },
      chain: {
        gatheredAmount: 9999900,
        investorsCount: 8,
      },
      offer: {
        data: {},
      },
      status: "CONFIRMED",
      viewer: {
        investedAmount: 10000,
      },
    }
    const profile = { investor: {} }
    expect(getIsDisabledButton(profile, order)).toBe(false)
  })

  it("investor with initial investment can't invest if delta < 0", () => {
    const order = {
      application: {
        data: { maxValue: "10000000", minValue: "1000000" },
      },
      chain: {
        gatheredAmount: 10000000,
        investorsCount: 8,
      },
      offer: {
        data: {},
      },
      status: "CONFIRMED",
      viewer: {
        investedAmount: 10000,
      },
    }
    const profile = { investor: {} }
    expect(getIsDisabledButton(profile, order)).toBe(true)
  })

  it("investor without initial investment can't invest if delta < 10 000 ", () => {
    const order = {
      application: {
        data: { maxValue: "10000000", minValue: "1000000" },
      },
      chain: {
        gatheredAmount: 9999900,
        investorsCount: 8,
      },
      offer: {
        data: {},
      },
      status: "CONFIRMED",
      viewer: {
        investedAmount: 0,
      },
    }
    const profile = { investor: {} }
    expect(getIsDisabledButton(profile, order)).toBe(true)
  })

  it("investor without initial investment can invest if delta > 10 000 ", () => {
    const order = {
      application: {
        data: { maxValue: "10000000", minValue: "1000000" },
      },
      chain: {
        gatheredAmount: 1000000,
        investorsCount: 8,
      },
      offer: {
        data: {},
      },
      status: "CONFIRMED",
      viewer: {
        investedAmount: 0,
      },
    }
    const profile = { investor: {} }
    expect(getIsDisabledButton(profile, order)).toBe(false)
  })
})

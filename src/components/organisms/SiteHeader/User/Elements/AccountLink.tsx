import React from "react"
import { Route } from "react-router-dom"
import { Flex, Icon } from "src/components"

import Link from "./Link"
import { AccountLinkContainer } from "../styles"

const render = (props) => (
  <AccountLinkContainer>
    <Link to={`/accounts/${props.accountLink || ""}`}>
      <Flex
        alignItems="center"
        style={{ padding: 10, boxSizing: "border-box" }}
      >
        <Icon size={22}>account</Icon>
      </Flex>
    </Link>
  </AccountLinkContainer>
)

export default (props) => (
  <Route>{({ match }) => render({ ...props, match })}</Route>
)

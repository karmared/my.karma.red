import React from "react"
import styled from "styled-components"
import { Route } from "react-router"

import { Button } from "src/components"
import { notifyGTM, pipe } from "src/utils"
import { ViewerAccessContainer } from "src/containers"

const StyledButton = styled(Button)`
  width: 142px;
  height: 32px;
  background-color: ${({ theme }) => theme.colors.red};
  color: ${({ theme }) => theme.colors.white};
  line-height: 32px;
  text-align: center;
  text-decoration: none;
  margin: auto;
  border-radius: 4px;
  border: 1px solid ${({ theme }) => theme.colors.red};

  &:hover {
    background-color: transparent;
    border-color: ${({ theme }) => theme.colors.red};
    color: ${({ theme }) => theme.colors.white};
  }
`

const handleRoute = (history, to) => () => {
  history.push(to)
}

const clickHandler = (checkAccess, history, to) => () => {
  checkAccess(handleRoute(history, to))
}

export default (props) => {
  const { to, children } = props

  const sendGTMEvent = notifyGTM({
    eventAction: "request",
    eventLabel: "click",
    extras: {
      "profile-id": props.user.id,
    },
  })

  return (
    <ViewerAccessContainer mode="withModal" role="borrower">
      {({ checkAccess }) => (
        <Route>
          {({ history }) => (
            <StyledButton
              onClick={pipe(
                clickHandler(checkAccess, history, to),
                sendGTMEvent,
              )}
            >
              {children}
            </StyledButton>
          )}
        </Route>
      )}
    </ViewerAccessContainer>
  )
}

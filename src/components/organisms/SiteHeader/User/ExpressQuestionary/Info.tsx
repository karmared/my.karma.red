import React from "react"
import { Route } from "react-router"

import {
  Box, Text, Button, Translate,
} from "src/components"

import { Divider, InfoPivot, InfoContainer } from "./styles"

const colors = {
  APPROVED: "green",
  DECLINED: "red",
}

const buildCta = (button, content, link) => [button, content, link]

const cta = new Map([
  [
    "create_order",
    () => buildCta(
      "components:user-page-header.express-scoring.actions.create-order",
      "components:user-page-header.express-scoring.advice.create-order",
      "/new-order",
    ),
  ],
  [
    "edit_order",
    (id?: any) => buildCta(
      "components:user-page-header.express-scoring.actions.edit-order",
      "components:user-page-header.express-scoring.advice.edit-order",
      `/orders/${id}`,
    ),
  ],
  [
    "create_borrower",
    () => buildCta(
      "components:user-page-header.express-scoring.actions.create-borrower",
      "components:user-page-header.express-scoring.advice.create-borrower",
      "/profiles/create/juristic",
    ),
  ],
  [
    "profiles_list",
    (id?: any) => buildCta(
      "components:user-page-header.express-scoring.actions.profiles-list",
      "components:user-page-header.express-scoring.advice.profiles-list",
      `/accounts/${id}`,
    ),
  ],
])

const isQuestionaryDeclined = (questionary) => questionary.state.status === "DECLINED"

const useCallToActionConfig = (profilesInfo, questionary) => React.useMemo(() => {
  const {
    borrowers,
    approvedBorrowers,
    initialOrders,
    hasReleasedOrder,
  } = profilesInfo

  if (hasReleasedOrder || isQuestionaryDeclined(questionary)) return []

  if (!borrowers.length) return cta.get("create_borrower")()
  if (!approvedBorrowers.length) return cta.get("profiles_list")(borrowers[0].id)

  if (initialOrders.length) return cta.get("edit_order")(initialOrders[0].id)

  return cta.get("create_order")()
}, [profilesInfo])

const Info = ({ questionary, profilesInfo, ...props }) => {
  const textColor = colors[questionary.state.status]
  const ctaConfig = useCallToActionConfig(profilesInfo, questionary)
  const [ctaTitle, advice, ctaLink] = ctaConfig

  const onClick = React.useCallback(() => {
    props.history.push(ctaLink)
    props.onClose()
  }, [ctaLink])

  return (
    <InfoPivot>
      <InfoContainer>
        <Text
          fontSize="14px"
          lineHeight="1.5"
          fontWeight="500"
          color={textColor}
        >
          <Translate
            i18n={`components:user-page-header.express-scoring.status.${questionary.state.status}`}
          />
        </Text>
        <Box mt="7px">
          <Text fontSize="12px" lineHeight="1.35" whiteSpace="pre-line">
            {questionary.state.reason}
          </Text>
        </Box>
        {!!ctaConfig.length && (
          <>
            <Box mt="20px">
              <Divider />
              <Box mt="20px" mb="10px">
                <Text fontSize="12px" lineHeight="1.35">
                  <Translate i18n={advice} />
                </Text>
              </Box>
            </Box>
            <Button height="32px" onClick={onClick} variant="blueWide">
              <Translate i18n={ctaTitle} />
            </Button>
          </>
        )}
      </InfoContainer>
    </InfoPivot>
  )
}

export default (props) => (
  <Route>{({ history }) => <Info {...props} history={history} />}</Route>
)

import React from "react"
import Relay, { graphql } from "react-relay"
import onClickOutside from "react-onclickoutside"

import { isJuristicBorrower, getProperty } from "src/utils"
import { Box, Text, Translate } from "src/components"

import Info from "./Info"
import { Container } from "./styles"
import { StatusIcon } from "./elements"

const useInfoState = (questionary) => {
  const [open, setOpen] = React.useState(false)
  const hideInfo = React.useCallback(() => setOpen(false), [])
  const showInfo = React.useCallback(
    () => !["PENDING", "INITIAL"].includes(questionary.state.status)
      && setOpen(true),
    [questionary],
  )

  return [open, showInfo, hideInfo]
}

const isApproved = (profile) => getProperty(profile, "borrowerAccreditation.status") === "APPROVED"

const useProfilesInfo = (viewer) => React.useMemo(() => {
  const borrowers = viewer.profiles.filter(isJuristicBorrower)
  const approvedBorrowers = borrowers.filter(isApproved)

  const orders = borrowers
    .reduce((memo, profile) => [...memo, ...profile.orders.edges], [])
    .map((edge) => edge.node)

  const initialOrders = orders.filter((order) => order.status === "INITIAL")
  const hasReleasedOrder = orders.length > initialOrders.length

  return {
    borrowers,
    approvedBorrowers,
    initialOrders,
    hasReleasedOrder,
  }
}, [viewer])

const ExpressQuestionary = ({ viewer }) => {
  const questionary = viewer.questionaries[0]
  const [open, showInfo, hideInfo] = useInfoState(questionary)
  const profilesInfo = useProfilesInfo(viewer);

  (ExpressQuestionary as any).handleClickOutside = hideInfo

  if (!questionary || profilesInfo.hasReleasedOrder) return null

  return (
    <Box>
      <Container onClick={showInfo as any}>
        <Box mr="7px">
          <StatusIcon status={questionary.state.status} />
        </Box>
        <Text fontSize="12px" lineHeight="1.2" color="grey">
          <Translate
            i18n={`components:user-page-header.express-scoring.title.${questionary.state.status}`}
          />
        </Text>
      </Container>
      {open && (
        <Info
          onClose={hideInfo}
          questionary={questionary}
          profilesInfo={profilesInfo}
        />
      )}
    </Box>
  )
}

const config = {
  handleClickOutside: () => (ExpressQuestionary as any).handleClickOutside,
}
const render = onClickOutside(ExpressQuestionary, config)

export default Relay.createFragmentContainer(render, {
  viewer: graphql`
    fragment ExpressQuestionary_viewer on User {
      questionaries {
        id
        state {
          status
          reason
        }
      }
      profiles {
        id
        borrowerAccreditation: accreditation(role: BORROWER) {
          status
        }
        __typename
        orders {
          edges {
            node {
              id
              status
            }
          }
        }
      }
    }
  `,
})

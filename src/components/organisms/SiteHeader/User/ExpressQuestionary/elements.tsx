/* eslint-disable import/prefer-default-export, max-len */
import React from "react"

import { IconContainer } from "./styles"

const Pending = () => (
  <svg width="18" height="18" viewBox="0 0 18 18">
    <g fill="none" fillRule="evenodd">
      <circle cx="9" cy="9" r="9" fill="#FF8F00" />
      <path
        stroke="#FFF"
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth="2"
        d="M9 4.58v4.808l2.543 2.168"
      />
    </g>
  </svg>
)

const Rejected = () => (
  <svg width="18" height="18" viewBox="0 0 18 18">
    <g fill="none" fillRule="evenodd">
      <circle cx="9" cy="9" r="9" fill="#E2251C" />
      <path stroke="#FFF" strokeLinecap="round" strokeWidth="2" d="M9 4.5v4" />
      <circle cx="9" cy="12" r="1" fill="#FFF" />
    </g>
  </svg>
)

const Approved = () => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="18"
    height="18"
    viewBox="0 0 18 18"
  >
    <g fill="none" fillRule="evenodd">
      <circle cx="9" cy="9" r="9" fill="#5BAB3A" />
      <path
        stroke="#FFF"
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth="2"
        d="M4.875 9.103L8.088 12l4.598-5.25"
      />
    </g>
  </svg>
)

const icons = new Map([
  ["INITIAL", Pending],
  ["PENDING", Pending],
  ["DECLINED", Rejected],
  ["APPROVED", Approved],
])

export const StatusIcon = ({ status }) => {
  const IconComponent = icons.get(status) || React.Fragment

  return (
    <IconContainer>
      <IconComponent />
    </IconContainer>
  )
}

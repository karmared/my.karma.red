import styled from "styled-components"

export const IconContainer = styled.div`
  min-width: 18px;
  width: 18px;
  min-height: 18px;
  height: 18px;
`

export const Container = styled.div`
  display: flex;
  align-items: center;
  height: 100%;
  margin-left: 15px;
  margin-right: 30px;
  cursor: pointer;
  user-select: none;
`

export const Divider = styled.div`
  width: 100%;
  height: 1px;
  background-color: ${({ theme }) => theme.colors.grey};
`

export const InfoPivot = styled.div`
  width: 0;
  height: 0;
  position: relative;
`

export const InfoContainer = styled.div`
  position: absolute;
  top: calc(100% + 10px);
  box-sizing: border-box;
  left: -30px;
  width: 355px;
  padding: 25px;
  border-radius: 5px;
  box-shadow: 0 0 24px 0 rgba(0, 0, 0, 0.16);
  background-color: white;

  &:before {
    position: absolute;
    top: -16px;
    left: 39px;
    width: 0;
    height: 0;
    content: "";
    border-style: solid;
    border-width: 0 15px 20px 15px;
    border-color: transparent transparent white transparent;
  }
`

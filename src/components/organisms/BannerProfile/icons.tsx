import React from "react"

export const Arrow = () => (
  <svg
    width="14"
    height="23"
    viewBox="0 0 14 23"
    fill="none"
    style={{ marginLeft: 20 }}
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M0.390442 19.7239L8.59628 11.5001L0.390442 3.27637L2.91669 0.750122L13.6667
    11.5001L2.91669 22.2501L0.390442 19.7239Z"
      fill="#4A4A4A"
    />
  </svg>
)

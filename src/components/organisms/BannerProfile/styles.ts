import styled from "styled-components"

export const BannerProfileWrap = styled.a`
  width: 100%;
  box-shadow: 3px 2px 14px rgba(0, 0, 0, 0.25);
  border-radius: 4px;
  padding: 30px 50px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-top: 60px;
  margin-bottom: 40px;
  font-size: 24px;
  line-height: 30px;
  color: #4a4a4a;
  text-decoration: none;
  @media screen and (max-width: 800px) {
    flex-direction: column;
    text-align: center;
  }
`
export const ActionWrap = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  font-weight: bold;
  @media screen and (max-width: 800px) {
    margin-top: 30px;
  }
`

import React from "react"

import {
  Box, Icons, Button, ModalContainer,
} from "src/components"

import {
  Container,
  CloseButton,
  ReturnButton,
  Title,
  ControlsContainer,
  StyledText,
} from "./styles"

const Error = ({ onClose }) => (
  <ModalContainer isOpened>
    <Container>
      <ReturnButton onClick={onClose}>
        <Icons.LeftArrow />
        <span>Назад</span>
      </ReturnButton>

      <CloseButton onClick={onClose}>
        <Icons.Close />
      </CloseButton>

      <Title>Ой</Title>

      <Box m="20px 0">
        <StyledText>Произошла неизвестная ошибка.</StyledText>
      </Box>

      <ControlsContainer>
        <Box mr="20px">
          <Button variant="primary" height="48px" onClick={onClose}>
            Закрыть
          </Button>
        </Box>
      </ControlsContainer>
    </Container>
  </ModalContainer>
)

export default Error

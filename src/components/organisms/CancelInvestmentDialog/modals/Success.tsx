import React from "react"

import { getOrderFullTitle } from "src/utils"
import {
  Box, Icons, Button, ModalContainer,
} from "src/components"

import {
  Container,
  CloseButton,
  ReturnButton,
  Title,
  ControlsContainer,
  StyledText,
} from "./styles"

const Success = ({ onComplete, order }) => (
  <ModalContainer isOpened>
    <Container>
      <ReturnButton onClick={onComplete}>
        <Icons.LeftArrow />
        <span>Назад</span>
      </ReturnButton>

      <CloseButton onClick={onComplete}>
        <Icons.Close />
      </CloseButton>

      <Title>Инвестиция отозвана</Title>

      <Box m="20px 0">
        <StyledText>
          Вы отменили инвестицию в заявку № {order.chain.id.split(".").pop()},{" "}
          {getOrderFullTitle(order.application)}
        </StyledText>
      </Box>

      <ControlsContainer>
        <Box mr="20px">
          <Button variant="primary" height="48px" onClick={onComplete}>
            Закрыть
          </Button>
        </Box>
      </ControlsContainer>
    </Container>
  </ModalContainer>
)

export default Success

import Dead from "./Dead"
import Confirmed from "./Confirmed"

const render = () => null

render.dead = Dead
render.confirmed = Confirmed

export default render

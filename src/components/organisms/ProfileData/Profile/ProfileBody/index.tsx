import Relay, { graphql } from "react-relay"
import { Route } from "react-router-dom"
import React, { useState, useCallback } from "react"

import { Box, SwitchBar } from "src/components"

import {
  Tab, tabs, TabsContainer, useSwitchBarSchema,
} from "./elements"

const ProfileBody = (props) => {
  const profile = props.profile || props.foreignProfile
  const schema = useSwitchBarSchema(profile)
  const [activeTab, setActiveTab] = useState(schema[0].type)
  const onTabChange = useCallback(({ activeTab: tab }) => setActiveTab(tab), [])

  const ActiveTab = React.useMemo(() => tabs[activeTab], [activeTab])

  return (
    <Box p="30px">
      <SwitchBar
        activeTab={activeTab}
        onChange={onTabChange}
        schema={schema}
        renderTab={Tab}
        renderContainer={TabsContainer}
      />
      <ActiveTab
        profile={props.profile}
        foreignProfile={props.foreignProfile}
      />
    </Box>
  )
}

const WithFragment = Relay.createFragmentContainer(ProfileBody, {
  profile: graphql`
    fragment ProfileBody_profile on UserProfile {
      id
      __typename
      bankAccounts {
        id
        status
      }
      ... on LegalEntityProfile {
        accreditation(role: BORROWER) {
          status
        }
      }
      ... on IndividualProfile {
        passport {
          scans {
            id
          }
        }
        accreditation(role: INVESTOR) {
          status
        }
      }
      ... on EntrepreneurProfile {
        passport {
          scans {
            id
          }
        }
        accreditation(role: INVESTOR) {
          status
        }
      }
      ...BankAccounts_profile
      ...AccreditationInfoFragment_profile
    }
  `,
  foreignProfile: graphql`
    fragment ProfileBody_foreignProfile on ForeignIndividualProfile {
      id
      __typename
      allBankAccounts {
        ... on ForeignBankAccount {
          id
          status
        }
        ... on BankAccount {
          id
          status
        }
      }
      accreditation(role: INVESTOR) {
        status
      }
      ...BankAccounts_foreignProfile
      ...AccreditationInfoFragment_foreignProfile
    }
  `,
})

export default (props) => (
  <Route>
    {({ location }) => (
      <WithFragment
        {...props}
        search={location.search}
        pathname={location.pathname}
      />
    )}
  </Route>
)

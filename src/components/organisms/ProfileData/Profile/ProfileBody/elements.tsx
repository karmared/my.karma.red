import React from "react"

import { BankAccountStatuses } from "src/constants"
import { getProperty, isRussianEntrepreneur, isRussianInvestor } from "src/utils"

import {
  Box,
  Flex,
  Text,
  Tooltip,
  Translate,
  BankAccounts,
  AccreditationInfo,
  TransactionsHistory,
} from "src/components"

import { StyledGlyph, StyledLink, StyledTab } from "../styles"

const isPassportFilled = (profile) => {
  if (!isRussianInvestor(profile) && !isRussianEntrepreneur(profile)) return true

  const scans = getProperty(profile, "passport.scans", [])
  return !!scans.length
}

const PassportTooltip = React.memo(({ profileId }: any) => (
  <Text color="white" fontSize="12px" lineHeight="1.3">
    <Translate i18n="components:account.account_item.tabs.tooltips.scans.start" />
    &nbsp;
    <StyledLink to={`/accounts/${profileId}/accreditation`}>
      <Translate i18n="components:account.account_item.tabs.tooltips.scans.link" />
    </StyledLink>
    &nbsp;
    <Translate i18n="components:account.account_item.tabs.tooltips.scans.end" />
  </Text>
))

export const useSwitchBarSchema = (profile) => React.useMemo(() => {
  const bankAccounts = profile.bankAccounts || []
  const hasApprovedBankAccount = bankAccounts.some(
    (acc) => acc.status === BankAccountStatuses.approved,
  )

  return [
    {
      label: "components:account.account_item.tabs.accreditation_info",
      type: "information",
      tooltip: isPassportFilled(profile)
        ? null
        : () => <PassportTooltip profileId={profile.id} />,
    },
    {
      label: "components:account.account_item.tabs.transactions_history",
      type: "history",
    },
    {
      label: "components:account.account_item.tabs.bank_accounts",
      type: "bankAccounts",
      tooltip: hasApprovedBankAccount
        ? null
        : "components:account.account_item.tabs.tooltips.bank_accounts",
    },
  ]
}, [profile])

export const tabs = {
  history: (props) => <TransactionsHistory {...props} />,
  information: (props) => <AccreditationInfo {...props} />,
  bankAccounts: (props) => (
    <Box width="640px">
      <BankAccounts {...props} />
    </Box>
  ),
}

export const TabsContainer = ({ children }) => <Flex mb="30px">{children}</Flex>

export const Tab = React.memo((props: any) => {
  const {
    label, active, onClick, tooltip,
  } = props

  const contentProp = typeof tooltip === "string" ? { text: tooltip } : { content: tooltip }

  return (
    <Flex mr="25px">
      <StyledTab active={active} onClick={onClick}>
        <Translate i18n={label} />
      </StyledTab>
      {tooltip && (
        <Box mt="2px">
          <Tooltip
            borderColor="red"
            borderWidth="2px"
            glyph={() => <StyledGlyph>i</StyledGlyph>}
            {...contentProp}
          />
        </Box>
      )}
    </Flex>
  )
})

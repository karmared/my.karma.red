import { backgroundColor } from "styled-system"
import styled, { css } from "styled-components"

import {
  Box, Flex, Text, Link, Button,
} from "src/components"

export const ProfileContainer = styled(Box)`
  width: 100%;
  max-width: 1280px;
  border-radius: 4px;
  box-shadow: 0 0 24px 0 rgba(0, 0, 0, 0.12);
  background-color: ${({ theme }) => theme.colors.white};
`

export const HeaderContainer = styled(Flex)`
  justify-content: space-between;
  padding: 20px 30px 30px 30px;
  background-color: ${({ theme }) => theme.colors.greyLights[3]};
`

export const ShadedText = styled(Text)`
  font-size: 14px;
  line-height: 1.45;
  color: ${({ theme }) => theme.colors.greyDark};
`

export const HeadingText = styled(Text)`
  font-size: 24px;
  font-weight: 300;
  line-height: 1.33;
`

export const AmountText = styled(Text)`
  font-size: 14px;
  font-weight: 500;
  line-height: 1.45;
`

export const AccreditationBadge = styled(Box)`
  width: fit-content;
  padding: 5px 10px;
  font-size: 10px;
  font-weight: 500;
  line-height: 1.2;
  letter-spacing: 0.8px;
  border-radius: 3px;
  color: ${({ theme }) => theme.colors.white};
  ${backgroundColor};
`

export const StyledButton = styled(Button)`
  height: 32px;
  font-size: 12px;
  font-weight: 500;
  line-height: 1.33;
  letter-spacing: 1px;
  text-transform: uppercase;
  background-color: transparent;
`

const activeTabStyles = ({ theme }) => css`
  border-bottom: 3px solid ${theme.colors.red};
  color: ${theme.colors.black};
  font-weight: 500;
`

export const StyledTab = styled(Box)`
  padding: 5px 2px;
  background-color: #fff;
  cursor: pointer;
  user-select: none;
  border-bottom: none;
  font-size: 14px;
  color: ${({ theme }) => theme.colors.greyDark};
  text-transform: uppercase;

  ${({ active }) => active && activeTabStyles}

  &:hover {
    background-color: ${({ theme }) => theme.colors.greyLight};
  }
`

export const Divider = styled(Box)`
  width: 100%;
  height: 1px;
  background-color: ${({ theme }) => theme.colors.greyLight};
`

export const StyledGlyph = styled(Text)`
  margin-top: 2px;
  line-height: 1;
  font-size: 13px;
  font-weight: bold;
  color: ${({ theme }) => theme.colors.red};
`

export const AccountBalanceContainer = styled.div`
  min-width: 620px;
  width: 620px;
  height: fit-content;
  margin-left: 20px;
`

export const StyledLink = styled(Link)`
  text-decoration: underline;
`

export const GlyphContent = styled(Text)`
  margin-top: 2px;
  line-height: 1;
  font-size: 13px;
  font-weight: bold;
  color: ${({ theme }) => theme.colors.grey};
`

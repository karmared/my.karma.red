import React from "react"
import { graphql } from "react-relay"
import { fetchQuery } from "relay-runtime"

import { useEnvironment } from "src/hooks"
import { isForeignInvestor } from "src/utils"
import { Flex, Loader, QueryRenderer } from "src/components"

import ProfileHeader from "./Header"
import ProfileBody from "./ProfileBody"
import { ProfileContainer } from "./styles"

const russianProfileQuery = graphql`
  query ProfileAccreditationInfoQuery($id: ID!) {
    node(id: $id) {
      ... on UserProfile {
        id
        __typename
        ...Header_profile
        ...ProfileBody_profile
      }
    }
  }
`

const foreignProfileQuery = graphql`
  query ProfileAccreditationInfoForeignQuery($id: ID!) {
    node(id: $id) {
      ... on ForeignIndividualProfile {
        id
        __typename
        ...Header_foreignProfile
        ...ProfileBody_foreignProfile
      }
    }
  }
`

const nodeTypeQuery = graphql`
  query ProfileAccreditationTypenameQuery($id: ID!) {
    node(id: $id) {
      __typename
    }
  }
`

const renderNull = () => (
  <Flex width="100%" alignItems="center" justifyContent="center">
    <Loader fontSize="14px" />
  </Flex>
)

const render = ({ node }) => {
  const [profile, foreignProfile] = isForeignInvestor(node)
    ? [null, node]
    : [node, null]

  return (
    <ProfileContainer>
      <ProfileHeader profile={profile} foreignProfile={foreignProfile} />
      <ProfileBody profile={profile} foreignProfile={foreignProfile} />
    </ProfileContainer>
  )
}

export default React.memo(({ profileId }: any) => {
  const variables = { id: profileId }

  const { environment } = useEnvironment()
  const [profile, setProfile] = React.useState(null)

  React.useEffect(() => {
    fetchQuery(environment, nodeTypeQuery, variables).then((data: any) => setProfile(data.node))
  }, [])

  if (!profile) return renderNull()

  const query = isForeignInvestor(profile)
    ? foreignProfileQuery
    : russianProfileQuery

  return (
    <QueryRenderer
      query={query}
      render={render}
      variables={variables}
      renderNull={renderNull}
    />
  )
})

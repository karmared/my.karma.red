import React from "react"
import Relay, { graphql } from "react-relay"

import { HeaderContainer } from "../styles"
import { BalanceInfo, ProfileInfo } from "./elements"

const Header = (props) => {
  const profile = props.profile || props.foreignProfile

  return (
    <HeaderContainer>
      <ProfileInfo profile={profile} />
      <BalanceInfo
        profile={props.profile}
        foreignProfile={props.foreignProfile}
      />
    </HeaderContainer>
  )
}

export default Relay.createFragmentContainer(Header, {
  profile: graphql`
    fragment Header_profile on UserProfile {
      id
      __typename
      ...Deposit_profile
      ...Withdraw_profile
      ...Convert_profile
      ... on IndividualProfile {
        name
        accountBalance
        accreditation(role: INVESTOR) {
          status
          declineReason
        }
        accountBalanceDetailed {
          currency
          available
          invested
          blocked
        }
      }
      ... on EntrepreneurProfile {
        name
        accountBalance
        accreditation(role: INVESTOR) {
          status
          declineReason
        }
        accountBalanceDetailed {
          currency
          available
          invested
          blocked
        }
      }
      ... on LegalEntityProfile {
        name
        accountBalance
        accreditation(role: BORROWER) {
          status
          declineReason
        }
        accountBalanceDetailed {
          currency
          available
        }
      }
    }
  `,
  foreignProfile: graphql`
    fragment Header_foreignProfile on ForeignIndividualProfile {
      ...Deposit_foreignProfile
      ...Withdraw_foreignProfile
      ...Convert_foreignProfile
      id
      __typename
      firstName
      lastName
      accountBalanceDetailed {
        currency
        available
        invested
        blocked
      }
      accreditation(role: INVESTOR) {
        status
        declineReason
      }
    }
  `,
})

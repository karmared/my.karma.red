import React from "react"

import {
  Box,
  Flex,
  Tooltip,
  Translate,
  VirtualAccountActions,
} from "src/components"
import { CurrencySigns } from "src/constants"
import { getProfileDetailedBalance, getProfileName, number } from "src/utils"

import {
  Divider,
  ShadedText,
  AmountText,
  HeadingText,
  GlyphContent,
  AccreditationBadge,
  AccountBalanceContainer,
} from "../styles"

const formatterOptions = {
  minimumFractionDigits: 2,
  maximumFractionDigits: 2,
}

const EmptyAccountBalance = () => (
  <Box>
    <AmountText>
      {number(0, formatterOptions)} {CurrencySigns.RUB}
    </AmountText>
  </Box>
)

const AccountBalance = ({ profile }) => {
  const detailedBalances = getProfileDetailedBalance(profile)

  if (!detailedBalances.length) return <EmptyAccountBalance />

  const detailedBalance = detailedBalances.find(
    (item) => item.currency === "RUB",
  )
  const { blocked, invested, available } = detailedBalance
  const total = (blocked || 0) + (invested || 0) + (available || 0)

  return (
    <Box>
      <AmountText>
        {number(total, formatterOptions)}{" "}
        {CurrencySigns[detailedBalance.currency]}
      </AmountText>
    </Box>
  )
}

const InfoGlyph = () => <GlyphContent>i</GlyphContent>

const BlockedTooltip = () => (
  <Tooltip
    borderColor="grey"
    borderWidth="2px"
    glyph={InfoGlyph}
    content={() => (
      <Translate i18n="components:account.account_item.header.blocked_info" />
    )}
  />
)

const BalanceDetailingItem = (props) => {
  const {
    type, amount, tooltip, currency,
  } = props

  return (
    <Box mr="30px">
      <Flex>
        <ShadedText>
          <Translate i18n={`components:account.account_item.header.${type}`} />
        </ShadedText>
        {tooltip}
      </Flex>
      <AmountText>
        {number(amount || 0, formatterOptions)} {CurrencySigns[currency]}
      </AmountText>
    </Box>
  )
}

const BalanceDetailing = ({ profile }) => {
  const detailedBalances = getProfileDetailedBalance(profile)

  if (!detailedBalances.length) return null

  const detailedBalance = detailedBalances.find(
    (item) => item.currency === "RUB",
  )
  const { blocked, invested, available } = detailedBalance

  return (
    <Flex>
      <BalanceDetailingItem currency="RUB" amount={invested} type="invested" />
      <BalanceDetailingItem
        currency="RUB"
        amount={blocked}
        type="blocked"
        tooltip={<BlockedTooltip />}
      />
      <BalanceDetailingItem
        currency="RUB"
        amount={available}
        type="available"
      />
    </Flex>
  )
}

const Actions = (props) => (
  <Flex mt="5px">
    <Box mr="10px">
      <VirtualAccountActions.Deposit
        profile={props.profile}
        foreignProfile={props.foreignProfile}
      />
    </Box>
    <VirtualAccountActions.Withdraw
      profile={props.profile}
      foreignProfile={props.foreignProfile}
    />
  </Flex>
)

export const BalanceInfo = React.memo((props: any) => {
  const profile = props.profile || props.foreignProfile

  return (
    <AccountBalanceContainer>
      <Flex width="100%" alignItems="center" mb="15px">
        <Box mr="25px">
          <ShadedText>
            <Translate i18n="components:account.account_item.header.balance" />
          </ShadedText>
        </Box>
        <AccountBalance profile={profile} />
      </Flex>
      <Divider />
      <Flex width="100%" mt="15px" justifyContent="space-between">
        <BalanceDetailing profile={profile} />
        <Actions
          profile={props.profile}
          foreignProfile={props.foreignProfile}
        />
      </Flex>
    </AccountBalanceContainer>
  )
})

const statusColors = {
  INITIAL: "blue",
  PENDING: "orange",
  APPROVED: "green",
  DECLINED: "red",
}

/* eslint-disable no-underscore-dangle */
export const ProfileInfo = ({ profile }) => (
  <Box>
    <Box mb="5px">
      <ShadedText>
        <Translate
          i18n={`components:account.account_item.header.profile_type.${profile.__typename}`}
        />
      </ShadedText>
    </Box>
    <Box mb="5px">
      <HeadingText>{getProfileName(profile)}</HeadingText>
    </Box>
    <AccreditationBadge bg={statusColors[profile.accreditation.status]}>
      <Translate
        i18n={`components:account.role_item.status.state.${profile.accreditation.status}`}
      />
    </AccreditationBadge>
  </Box>
)

/* eslint-disable no-undef */
import React from "react"
import Enzyme, { shallow } from "enzyme"
import Adapter from "enzyme-adapter-react-16"
import { MIN_INVESTMENT_AMOUNT } from "src/constants/system"
import { useOnSubmit } from "./hookUseOnSubmit.tsx"

Enzyme.configure({ adapter: new Adapter() })

describe("Amount test", () => {
  it("invested sum < MIN_INVESTEMENT_AMOUNT with no previous investing is no passed", () => {
    const handleComplete = jest.fn()
    const AmountMockElement = () => {
      const onSubmit = useOnSubmit({
        amount: `${MIN_INVESTMENT_AMOUNT - 1}`,
        availableAmount: 100000,
        setManualError: jest.fn(),
        onComplete: handleComplete,
        order: { viewer: { investedAmount: 0 }, application: { shortTitle: "test" } },
        profile: { accountBalance: [{ currency: "RUB", available: 100000 }] },
      })
      return <button onClick={onSubmit} />
    }
    const container = shallow(<AmountMockElement />)

    expect(container.find("button").at(0).exists()).toBeTruthy()
    container.find("button").at(0).simulate("click")
    expect(handleComplete).toHaveBeenCalledTimes(0)
  })

  it("invested sum < MIN_INVESTMENT_AMOUNT with previous investing is passed", () => {
    const handleComplete = jest.fn()
    const AmountMockElement = () => {
      const onSubmit = useOnSubmit({
        amount: `${MIN_INVESTMENT_AMOUNT - 1}`,
        availableAmount: 100000,
        setManualError: jest.fn(),
        onComplete: handleComplete,
        order: {
          viewer: { investedAmount: MIN_INVESTMENT_AMOUNT },
          application: { shortTitle: "test" },
        },
        profile: { accountBalance: [{ currency: "RUB", available: 100000 }] },
      })
      return <button onClick={onSubmit} />
    }
    const container = shallow(<AmountMockElement />)

    expect(container.find("button").at(0).exists()).toBeTruthy()
    container.find("button").at(0).simulate("click")
    expect(handleComplete).toHaveBeenCalledTimes(1)
  })

  it("invested sum < Available is no passed", () => {
    const handleComplete = jest.fn()
    const AmountMockElement = () => {
      const onSubmit = useOnSubmit({
        amount: `${MIN_INVESTMENT_AMOUNT}`,
        availableAmount: MIN_INVESTMENT_AMOUNT - 1,
        setManualError: jest.fn(),
        onComplete: handleComplete,
        order: {
          viewer: { investedAmount: MIN_INVESTMENT_AMOUNT },
          application: { shortTitle: "test" },
        },
        profile: {
          accountBalance: [
            { currency: "RUB", available: MIN_INVESTMENT_AMOUNT - 1 },
          ],
        },
      })
      return <button onClick={onSubmit} />
    }
    const container = shallow(<AmountMockElement />)

    expect(container.find("button").at(0).exists()).toBeTruthy()
    container.find("button").at(0).simulate("click")
    expect(handleComplete).toHaveBeenCalledTimes(0)
  })
})

import { History, Location } from "history"

/* TODO: inused - вернуть или удалить окончательно
import { ProposalInfoQueryResponse } from "src/query/order/items/__generated__/ProposalInfoQuery.graphql"
*/

export type ErrorType = {
  path: string;
  keyword: string;
  sum?: string;
}

export type OrderType = any;
// ProposalInfoQueryResponse["node"] TODO: решить проблему с типом для data

export type InvestorType = {
  canBeInvestor: boolean;
  investedAmount: number;
  qualified: boolean;
}

export type BankAccountType = {
  id: string;
  status: "INITIAL" | "APPROVED" | "DECLINED";
}

export type AccreditationType = {
  status: "INITIAL" | "PENDING" | "APPROVED" | "DECLINED";
}

export type CurrencyType = "RUB" | "EUR";

export type AccountBalanceType = {
  available: number;
  currency: CurrencyType;
}

export type ProfileType = {
  accountBalance: number;
  accountBalanceDetailed: AccountBalanceType[];
  accreditation: AccreditationType;
  approvedAsInvestor: boolean;
  bankAccounts: BankAccountType[];
  id: string;
  investor: InvestorType;
  name: string;
  phone: string;
  supportingDocuments: [];
  __typename: string;
}

export type ViewerType = {
  country: string;
  createdAt: string;
  currency: CurrencyType;
  email: string;
  foreignProfiles: any;
  id: string;
  locale: "RU" | "EN" | "KO" | "ZH";
  profiles: ProfileType[];
}

export type AmountType = AmountMainType & {
  clearError: (value: string) => void;
  errors: [];
  getError: (value: string) => void;
  setErrors: (transaction) => void;
  setManualError: (error) => void;
  viewer: ViewerType;
}

export type AmountMainType = {
  amount: string;
  availableAmount: number;
  error: null | string;
  goBack: () => void;
  onChange: (evt: React.ChangeEvent<HTMLInputElement>) => void;
  onClose: () => void;
  onComplete: () => void;
  onError: (err) => void;
  order: any;
  orderNumber: string;
  profile: ProfileType;
  profiles: ProfileType[];
  setProfile: (profile: ProfileType) => void;
  showDeposit: () => void;
  viewer: ViewerType;
}

export type InvestmentPageType = InvestmentPageMainType & {
  checkAccess: (value1?: any, value2?: any) => void;
  children: (renderProps: any) => void;
  data: {node: OrderType};
  order: OrderType;
}

export type ConsumerProps = {
  children: (renderProps: any) => void;
}

export type InvestmentPageMainType = {
  history: History;
  location: Location;
  match: any;
  retry: () => void;
  staticContext: any;
  viewer: ViewerType;
}

export type InvestmentRenderType = InvestmentPageMainType & {
  data: {node: OrderType};
  node: OrderType;
}

export type NextStepType = {
  next: string | null;
  component: (props: AmountMainType) => JSX.Element;
}

export type ItemsType = {
  amount: NextStepType;
  confirmation: NextStepType;
  deposit: NextStepType;
  error: NextStepType;
  guest: NextStepType;
  success: NextStepType;
}

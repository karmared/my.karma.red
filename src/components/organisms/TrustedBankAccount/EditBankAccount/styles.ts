import styled from "styled-components"

export const Form = styled.form`
  padding: 0;
  margin: 0;
  width: 100%;
  max-width: 550px;
`

import React from "react"

import {
  Box, Text, Forms, Translate,
} from "src/components"

import {
  Divider,
  ShadedText,
  GearContainer,
  WordWrapContainer,
} from "../styles"

/* eslint-disable max-len */
const GearIcon = () => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="12"
    height="12"
    viewBox="0 0 24 24"
  >
    <path
      fill="#9EA6AE"
      fillRule="nonzero"
      d="M21.169 13.176l2.603 1.98c.234.18.296.504.148.768l-2.467 4.152a.61.61 0 0 1-.753.264l-3.072-1.2a9.545 9.545 0 0 1-2.085 1.176l-.468 3.18a.597.597 0 0 1-.605.504H9.535a.597.597 0 0 1-.604-.504l-.469-3.18a9.077 9.077 0 0 1-2.085-1.176l-3.072 1.2c-.271.096-.604 0-.752-.264L.086 15.924a.594.594 0 0 1 .148-.768l2.603-1.98A9.262 9.262 0 0 1 2.75 12c0-.396.037-.792.087-1.176L.234 8.844a.581.581 0 0 1-.148-.768l2.467-4.152a.61.61 0 0 1 .752-.264l3.072 1.2a9.545 9.545 0 0 1 2.085-1.176l.469-3.18A.597.597 0 0 1 9.535 0h4.935c.308 0 .568.216.605.504l.468 3.18c.753.3 1.444.696 2.085 1.176l3.072-1.2c.271-.096.605 0 .753.264l2.467 4.152a.594.594 0 0 1-.148.768l-2.603 1.98A9.1 9.1 0 0 1 21.255 12a9.1 9.1 0 0 1-.086 1.176zM12.003 16.2c2.38 0 4.318-1.884 4.318-4.2 0-2.316-1.937-4.2-4.318-4.2-2.381 0-4.318 1.884-4.318 4.2 0 2.316 1.937 4.2 4.318 4.2z"
    />
  </svg>
)

const ListHeader = ({ email }) => {
  const [passwordModal, setPasswordModal] = React.useState(false)
  const showModal = React.useCallback(() => setPasswordModal(true), [])
  const hideModal = React.useCallback(() => setPasswordModal(false), [])

  return (
    <>
      <Box mb="10px">
        <ShadedText>
          <Translate i18n="components:account.list.header" />
        </ShadedText>
      </Box>
      <WordWrapContainer mb="20px">
        <Text fontSize="16px" fontWeight="500" lineHeight="1.33">
          {email}
        </Text>
        <GearContainer onClick={showModal}>
          <GearIcon />
        </GearContainer>
      </WordWrapContainer>
      <Divider />
      <Forms.password.change open={passwordModal} onClose={hideModal} />
    </>
  )
}

export default React.memo(ListHeader)

import React from "react"

import { Box, Translate } from "src/components"

import { ShadedText } from "../styles"

const render = ({ profiles }) => {
  if (profiles.length) return null

  return (
    <Box mt="20px">
      <ShadedText>
        <Translate i18n="components:account.list.empty" />
      </ShadedText>
    </Box>
  )
}

export default render

/* eslint-disable react-hooks/rules-of-hooks */
import React from "react"
import { Route } from "react-router"

import { ProfileType } from "src/constants"

import {
  isJuristicBorrower,
  isRussianInvestor,
  isForeignInvestor,
  isRussianEntrepreneur,
} from "src/utils"

import {
  Box, Text, Translate, FormLoadingButton,
} from "src/components"

import { Divider } from "../styles"
import ProfileItem from "./ProfileItem"

const Profiles = ({ profiles }) => (
  <>
    {profiles.map((profile) => (
      <ProfileItem key={profile.id} profile={profile} />
    ))}
  </>
)

const Heading = ({ children }: any) => (
  <Box mt="15px" mb="12px">
    <Text
      fontSize="11px"
      color="greyDark"
      fontWeight="500"
      lineHeight="1.45"
      textTransform="uppercase"
    >
      {children}
    </Text>
  </Box>
)

const SubHeading = ({ children }) => (
  <Box mt="15px" mb="12px">
    <Text fontSize="13px" color="greyDark" lineHeight="1.5">
      {children}
    </Text>
  </Box>
)

const CreateProfile = React.memo((props: any) => {
  const onClick = React.useCallback(() => {
    if (props.onClick) props.onClick()
    if (props.to) props.history.push(props.to)
  }, [])

  return (
    <Box mt="40px">
      <FormLoadingButton
        width="100%"
        height="40px"
        variant="default"
        onClick={onClick}
        isLoading={props.busy}
        disabled={props.disabled}
      >
        <Translate i18n="components:account.list.add_profile" />
      </FormLoadingButton>
    </Box>
  )
})

const CreateButton = (props) => (
  <Route>
    {({ history }) => <CreateProfile {...props} history={history} />}
  </Route>
)

const BorrowerProfiles = (props) => (
  <Box mb="30px">
    <Heading>
      <Translate i18n="components:account.list.borrower_profiles" />
    </Heading>
    <Profiles {...props} />
    <CreateButton to="/profiles/create/juristic" />
  </Box>
)

const ForeignInvestorProfiles = (props) => (
  <Box mb="30px">
    <Heading>
      <Translate i18n="components:account.list.investor_profiles.general" />
    </Heading>
    <Profiles {...props} />
    {!props.profiles.length && <CreateButton to="/accounts/create/foreign" />}
  </Box>
)

const createProfileLinks = {
  [ProfileType.individual]: "/accounts/create/individual",
  [ProfileType.entrepreneur]: "/accounts/create/entrepreneur",
}

const InvestorProfiles = (props) => (
  <Box mb="30px">
    <SubHeading>
      <Translate
        i18n={`components:account.list.investor_profiles.${props.type}`}
      />
    </SubHeading>
    <Profiles {...props} />
    {!props.profiles.length && (
      <CreateButton to={createProfileLinks[props.type]} />
    )}
  </Box>
)

const useProfilesByType = (profiles) => React.useMemo(() => {
  const borrowers = profiles.filter(isJuristicBorrower)
  const investors = profiles.filter(isRussianInvestor)
  const foreignInvestors = profiles.filter(isForeignInvestor)
  const entrepreneurs = profiles.filter(isRussianEntrepreneur)

  return {
    investors,
    borrowers,
    entrepreneurs,
    foreignInvestors,
  }
}, [profiles])

const ProfilesList = ({ profiles }) => {
  if (!profiles.length) return null

  const {
    investors,
    borrowers,
    entrepreneurs,
    foreignInvestors,
  } = useProfilesByType(profiles)

  if (foreignInvestors.length) {
    return <ForeignInvestorProfiles profiles={foreignInvestors} />
  }

  return (
    <>
      <Heading fontSize="13px">
        <Translate i18n="components:account.list.investor_profiles.general" />
      </Heading>
      <InvestorProfiles type={ProfileType.individual} profiles={investors} />
      <InvestorProfiles
        type={ProfileType.entrepreneur}
        profiles={entrepreneurs}
      />
      <Divider />
      <BorrowerProfiles profiles={borrowers} />
    </>
  )
}

export default ProfilesList

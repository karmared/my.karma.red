import React from "react"

import { getProfileName } from "src/utils"
import { Box, Text, Translate } from "src/components"

import { ItemContainer, StyledNavLink, StyledEnterSVG } from "../styles"

const EnterIcon = () => (
  <StyledEnterSVG width="24" height="24" viewBox="0 0 24 24">
    <g fill="none" fillRule="evenodd">
      <g stroke="#D9DEE2" strokeLinecap="round" strokeWidth="1.5">
        <path d="M7 4v9a3 3 0 0 0 3 3h6" />
        <path strokeLinejoin="round" d="M14 19.5l4-3.5-4-3.5" />
      </g>
    </g>
  </StyledEnterSVG>
)

const statusColors = {
  INITIAL: "blue",
  PENDING: "orange",
  APPROVED: "green",
  DECLINED: "red",
}

const render = ({ profile }) => (
  <StyledNavLink to={`/accounts/${profile.id}`}>
    <ItemContainer>
      <EnterIcon />
      <Box ml="12px">
        <Text fontSize="15px" color="greyDarker" lineHeight="1.33">
          {getProfileName(profile)}
        </Text>
        <Text
          fontSize="15px"
          lineHeight="1.6"
          color={statusColors[profile.accreditation.status]}
        >
          <Translate
            i18n={`components:account.role_item.status.state.${profile.accreditation.status}`}
          />
        </Text>
      </Box>
    </ItemContainer>
  </StyledNavLink>
)

export default React.memo(render)

import React from "react"
import i18next from "i18next"
import { toast } from "react-toastify"

import { useMutation } from "src/hooks"
import { MailVirtualDeposit } from "src/mutations"
import { Translate, FormLoadingButton } from "src/components"

const SendButton = ({ profileId }) => {
  const [busy, setBusy] = React.useState(false)
  const mailVirtualDeposit = useMutation(MailVirtualDeposit)

  const onCompleted = () => {
    setBusy(false)

    const message = i18next.t(
      "components:account.operations.deposit.toasts.send.success",
    )
    toast.success(message)
  }

  const onError = () => {
    setBusy(false)

    const message = i18next.t(
      "components:account.operations.deposit.toasts.send.error",
    )
    toast.error(message)
  }

  mailVirtualDeposit.onCompleted(onCompleted)
  mailVirtualDeposit.onError(onError)

  const commit = React.useCallback(() => {
    const variables = {
      input: {
        profileId,
      },
    }

    setBusy(true)
    mailVirtualDeposit.commit(variables)
  }, [profileId])

  return (
    <>
      <FormLoadingButton
        width="100%"
        variant="default"
        onClick={commit}
        isLoading={busy}
      >
        <Translate i18n="components:account.operations.deposit.buttons.send" />
      </FormLoadingButton>
    </>
  )
}

export default SendButton

import React, { useEffect, useMemo, useState } from "react"

import {
  Translate,
  LoaderBox,
  MiniLoader,
  LoadMore,
  CancelInvestmentDialog,
} from "src/components"
import { InvestmentActiveOrdersListQueryContainer } from "src/query"
import { getOrderTicker, toCurrencyFormat } from "src/utils"

import {
  TData,
  TDName,
  TDNameLink,
  TDBold,
  ActionButton,
  ActionLink,
  TDStatus,
  ActionWrapper,
  TDDate,
  TableLoader,
  TableWrapper,
  DesktopWrapper,
  MobileWrapper,
} from "./styles"
import { ClockIcon } from "./icons"
import { Statusbar, Table, List } from "../Elements"
import {
  parseInvestments,
  getStatusbarValue,
  getOrderAmount,
  getOrderDate,
  getOrderName,
  getOrderDuration,
  getOrderRate,
} from "./utils"
import { InvestmentActiveCard } from "../Cards"

export const statuses = new Map([
  [
    "TRANSFER",
    {
      text: "Ожидается поступление средств на счет заемщика",
      color: "#FF8A00",
    },
  ],
  [
    "CONFIRMED",
    {
      text: "Идёт сбор средств",
      color: "#6FA84B",
    },
  ],
])

export function RenderedTable(props) {
  const [isExpanded, setExpanded] = useState(true)
  const [isLoading, setLoading] = useState(false)
  const [cancelOrder, setCancelOrder] = useState()
  const {
    toProposal,
    getProposalLink,
    toInvest,
    isFetching,
    data,
    retry,
    load: { handleLoad, hasNextPage },
    filter,
    setFilter,
  } = props

  function getColumns() {
    return [
      {
        title: "Название",
        render: (i, obj) => (
          <TDName key={i}>
            <TDNameLink to={getProposalLink(obj)}>
              {getOrderName(obj)}
            </TDNameLink>
          </TDName>
        ),
        placeholder: (i) => <TDName key={i}>_</TDName>,
      },
      {
        title: "№ заявки",
        render: (i, obj) => (
          <TData key={i}>{getOrderTicker(obj, !!obj.cession)}</TData>
        ),
        placeholder: (i) => <TData key={i}>_</TData>,
      },
      {
        title: "",
        render: (i, obj) => (
          <TData key={i}>
            <Statusbar width="70px" value={getStatusbarValue(obj)} />
          </TData>
        ),
        placeholder: (i) => (
          <TData key={i}>
            <Statusbar width="70px" value={0} />
          </TData>
        ),
      },
      {
        title: "Цель, ₽",
        render: (i, obj) => <TData key={i}>{getOrderAmount(obj)}</TData>,
        placeholder: (i) => <TData key={i}>0</TData>,
      },
      {
        title: "Ставка",
        render: (i, obj) => <TData key={i}>{getOrderRate(obj)}</TData>,
        placeholder: (i) => <TData key={i}>0%</TData>,
      },
      {
        title: "Срок",
        render: (i, obj) => (
          <TData key={i}>
            {getOrderDuration(obj) && (
              <>
                {getOrderDuration(obj)} <Translate i18n={"models:loan.month"} />
              </>
            )}
            {!getOrderDuration(obj) && "_"}
          </TData>
        ),
        placeholder: (i) => (
          <TData key={i}>
            0 <Translate i18n={"models:loan.month"} />
          </TData>
        ),
      },
      {
        title: "Инвестиции, ₽",
        render: (i, obj) => (
          <TDBold key={i}>{toCurrencyFormat(obj.amount)}</TDBold>
        ),
        placeholder: (i) => <TData key={i}>0</TData>,
      },
      {
        title: "Сбор до",
        render: (i, obj) => (
          <TDDate key={i}>
            {ClockIcon} {getOrderDate(obj)}
          </TDDate>
        ),
        placeholder: (i) => <TDDate key={i}>{ClockIcon} -</TDDate>,
      },
      {
        title: "Статус",
        render: (i, obj, isHover) => {
          const status = statuses.get(obj.status) || {
            text: "Неизвестно",
            color: "grey",
          }

          const showActions = isHover && obj.status !== "TRANSFER"

          return (
            <TDStatus color={status.color} key={i}>
              {showActions ? (
                <ActionWrapper>
                  <ActionLink to={`/market/${obj.id}`}>
                    Инвестировать
                  </ActionLink>
                  <ActionButton
                    variant="secondary"
                    onClick={() => setCancelOrder(obj)}
                  >
                    Отменить
                  </ActionButton>
                </ActionWrapper>
              ) : (
                status.text
              )}
            </TDStatus>
          )
        },
        placeholder: (i) => (
          <TDStatus color={"#6FA84B"} key={i}>
            Идет сбор средств
          </TDStatus>
        ),
      },
    ]
  }

  const { investor } = data || {}
  const orders = useMemo(() => [...parseInvestments(investor)], [investor])
  const columns = getColumns()

  useEffect(() => {
    setLoading(false)
  }, [data?.investor?.investments?.edges])

  function load() {
    setLoading(true)
    handleLoad()
  }

  const handleCloseCancelInvestment = React.useCallback(() => {
    setCancelOrder(null)
  }, [])
  const handleCompleteCancelInvestment = React.useCallback(() => {
    setCancelOrder(null)
    retry()
  }, [retry])

  const hasFilter = Boolean(Object.values(filter).find(Boolean))

  return (
    <div>
      {(orders.length > 0 || isFetching || hasFilter) && (
        <>
          <DesktopWrapper>
            <TableWrapper>
              {isFetching && (
                <TableLoader>
                  <MiniLoader margin="auto" />
                </TableLoader>
              )}

              <Table
                title="СОБИРАЮТ СРЕДСТВА"
                descr="Если заявки не соберут необходимую сумму, ваши средства будут возвращены."
                columns={columns}
                items={orders}
                isFetching={isFetching}
                isExpanded={isExpanded}
                setExpanded={setExpanded}
                filter={filter}
                setFilter={setFilter}
              />
            </TableWrapper>
          </DesktopWrapper>

          <MobileWrapper>
            <List
              title="СОБИРАЮТ СРЕДСТВА"
              descr="Если заявки не соберут необходимую сумму, ваши средства будут возвращены."
              items={orders}
              Render={InvestmentActiveCard}
              isFetching={isFetching}
              toProposal={toProposal}
              toInvest={toInvest}
              cancelInvestment={setCancelOrder}
              isExpanded={isExpanded}
              setExpanded={setExpanded}
              filter={filter}
              setFilter={setFilter}
            />
          </MobileWrapper>

          {isExpanded && (
            <LoaderBox>
              {hasNextPage && !isLoading && (
                <LoadMore onClick={load}>+ загрузить еще</LoadMore>
              )}
              {hasNextPage && isLoading && <MiniLoader />}
            </LoaderBox>
          )}
        </>
      )}
      <CancelInvestmentDialog
        isOpened={Boolean(cancelOrder)}
        order={cancelOrder}
        onClose={handleCloseCancelInvestment}
        onComplete={handleCompleteCancelInvestment}
      />
    </div>
  )
}

export default function (props) {
  const { profile, ...rest } = props

  return (
    <InvestmentActiveOrdersListQueryContainer
      profile={profile}
      render={(data) => <RenderedTable {...rest} {...data} />}
    />
  )
}

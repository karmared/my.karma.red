import React, { useEffect, useState } from "react"

import {
  Translate, LoaderBox, MiniLoader, LoadMore,
} from "src/components"
import { LoanSucceededOrdersListQueryContainer } from "src/query"
import { getOrderTicker, toCurrencyFormat } from "src/utils"

import {
  TData,
  TDName,
  TDNameLink,
  TDBold,
  TDDate,
  TDStatus,
  TableLoader,
  TableWrapper,
  DesktopWrapper,
  MobileWrapper,
} from "./styles"
import { ClockIcon } from "./icons"
import { Table, List } from "../Elements"
import { LoanSucceededCard } from "../Cards"
import {
  parseOrders,
  getOrderName,
  getOrderDuration,
  getOrderRate,
  getOrderSuccedDate,
} from "./utils"

export const statuses = new Map([
  [
    "BEFOREHAND",
    {
      text: "Погашен досрочно",
      color: "#6FA84B",
    },
  ],
  [
    "PAID",
    {
      text: "Погашен",
      color: "#6FA84B",
    },
  ],
  [
    "DELAYED",
    {
      text: "Погашен с задержкой",
      color: "#F70000",
    },
  ],
])

export function RenderedTable(props) {
  const [isExpanded, setExpanded] = useState(true)
  const [isLoading, setLoading] = useState(false)
  const {
    toProposal,
    getProposalLink,
    isFetching,
    data,
    load: { handleLoad, hasNextPage },
  } = props

  function getColumns() {
    return [
      {
        title: "Название",
        render: (i, obj) => (
          <TDName key={i}>
            <TDNameLink to={getProposalLink(obj)}>{getOrderName(obj)}</TDNameLink>
          </TDName>
        ),
        placeholder: (i) => <TDName key={i}>_</TDName>,
      },
      {
        title: "№ заявки",
        render: (i, obj) => (
          <TData key={i}>{getOrderTicker(obj, !!obj.cession)}</TData>
        ),
        placeholder: (i) => <TData key={i}>_</TData>,
      },
      {
        title: "Ставка",
        render: (i, obj) => <TData key={i}>{getOrderRate(obj)}</TData>,
        placeholder: (i) => <TData key={i}>0%</TData>,
      },
      {
        title: "Срок",
        render: (i, obj) => (
          <TData key={i}>
            {getOrderDuration(obj) && (
              <>
                {getOrderDuration(obj)} <Translate i18n={"models:loan.month"} />
              </>
            )}
            {!getOrderDuration(obj) && "_"}
          </TData>
        ),
        placeholder: (i) => (
          <TData key={i}>
            0 <Translate i18n={"models:loan.month"} />
          </TData>
        ),
      },
      {
        title: "Дата погашения займа",
        render: (i, obj) => (
          <TDDate key={i}>
            {ClockIcon} {getOrderSuccedDate(obj)}
          </TDDate>
        ),
        placeholder: (i) => <TDDate key={i}>{ClockIcon} -</TDDate>,
      },
      {
        title: "Сумма займа, ₽",
        render: (i, obj) => (
          <TDBold key={i}>{toCurrencyFormat(obj?.chain?.gatheredAmount)}</TDBold>
        ),
        placeholder: (i) => <TDBold key={i}>0</TDBold>,
      },
      {
        title: "Статус",
        render: (i, obj) => {
          const status = statuses.get(obj.paymentSchedule.status) || {
            text: "Неизвестно",
            color: "grey",
          }

          return (
            <TDStatus color={status.color} key={i}>
              {status.text}
            </TDStatus>
          )
        },
        placeholder: (i) => (
          <TDStatus color={"#6FA84B"} key={i}>
            Погашен
          </TDStatus>
        ),
      },
    ]
  }

  const { borrower } = data || {}
  const orders = [...parseOrders(borrower)]
  const columns = getColumns()

  useEffect(() => {
    setLoading(false)
  }, [data?.borrower?.orders?.edges])

  function load() {
    setLoading(true)
    handleLoad()
  }

  return (
    <div>
      {(orders.length > 0 || isFetching) && (
        <>
          <DesktopWrapper>
            <TableWrapper>
              {isFetching && (
                <TableLoader>
                  <MiniLoader margin="auto" />
                </TableLoader>
              )}

              <Table
                title="ЗАВЕРШЕННЫЕ"
                columns={columns}
                items={orders}
                isFetching={isFetching}
                isExpanded={isExpanded}
                setExpanded={setExpanded}
              />
            </TableWrapper>
          </DesktopWrapper>

          <MobileWrapper>
            <List
              title="ЗАВЕРШЕННЫЕ"
              items={orders}
              Render={LoanSucceededCard}
              isFetching={isFetching}
              toProposal={toProposal}
              isExpanded={isExpanded}
              setExpanded={setExpanded}
            />
          </MobileWrapper>

          {isExpanded && (
            <LoaderBox>
              {hasNextPage && !isLoading && (
                <LoadMore onClick={load}>+ загрузить еще</LoadMore>
              )}
              {hasNextPage && isLoading && <MiniLoader />}
            </LoaderBox>
          )}
        </>
      )}
    </div>
  )
}

export default function (props) {
  const { profile, ...rest } = props

  const InvestmentCompletedTable = LoanSucceededOrdersListQueryContainer(
    (data) => <RenderedTable {...rest} {...data} />,
    profile,
  )

  return <InvestmentCompletedTable />
}

import React from "react"

import { FormBuilder, ScrollToTopOnMount } from "src/components"

import { OrderApplicationSchema } from "src/constants"

import Container from "./styles"

export class Application extends React.Component<any, any> {
  componentDidMount() {
    this.props.sendGTMEvent()
  }

  onChange = (value) => {
    this.props.onChange({
      name: this.props.name,
      value,
    })
  }

  render() {
    return (
      <Container>
        <ScrollToTopOnMount />
        <FormBuilder
          environment={this.props.environment}
          schema={OrderApplicationSchema}
          value={this.props.value}
          onChange={this.onChange}
        />
      </Container>
    )
  }
}

export default Application

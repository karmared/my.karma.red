export const valueValidationError = {
  keyword: "match",
  path: "application.value",
  type: "validation",
}

export const dateRequiredError = {
  keyword: "required",
  path: "application.startDate",
  type: "validation",
}

export const termValidationError = {
  keyword: "match",
  path: "application.term",
  type: "validation",
}

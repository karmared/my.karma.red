import React from "react"

export const ArrowIcon = () => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="44"
    height="14"
    viewBox="0 0 44 14"
  >
    <g fill="none" fillRule="evenodd" stroke="#9EA6AE" opacity=".327">
      <path d="M0 7h44" />
      <path strokeLinecap="square" d="M37 0l7 7-7 7" />
    </g>
  </svg>
)

export const Placeholder = ""

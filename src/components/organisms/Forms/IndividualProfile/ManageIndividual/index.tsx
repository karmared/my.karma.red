import React from "react"
import Relay, { graphql } from "react-relay"
import { toast, ToastContainer } from "react-toastify"

import { ErrorsContainer } from "src/containers"
import { initialStateFromProps } from "src/utils"
import { RelayEnvironmentConsumer } from "src/context"

import { CreateIndividualProfile, UpdateIndividualProfile } from "src/mutations"

import {
  Box,
  Label,
  FormRow,
  Template,
  TextField,
  Translate,
  ImgUploader,
  RowsContainer,
  ErrorForField,
  PhoneConfirmInput,
  AccreditationSuggestModal,
} from "src/components"

import { FormHeader, FormControls } from "./Elements"

class Form extends React.Component<any, any> {
  state = {
    busy: false,
    isLoading: false,
    showModal: false,
    ...initialStateFromProps(this.props.profile, [
      "id",
      "phone",
      "name",
      "signedPhone",
      "approvedOnce",
      "signedAvatar",
      "_avatar",
    ]),
  }

  applySignedPhone = (signedPhone) => {
    this.setState(() => ({
      signedPhone,
    }))
  }

  onAvatarUpdate = (signedAvatar) => {
    this.setState(() => ({ signedAvatar }))
  }

  onAvatarDelete = () => {
    this.setState(() => ({
      signedAvatar: "",
      _avatar: {
        url: "",
      },
    }))
  }

  onChange = (event) => {
    const { name, value } = event.target
    this.setState(() => ({ [name]: value }))
    this.props.clearError(`updateIndividualProfile.${name}`)
  }

  onModalClose = () => {
    this.setState(() => ({ showModal: false }))
  }

  onSubmit = (event) => {
    if (event) {
      event.preventDefault()
    }

    if (this.state.busy === true) {
      return
    }

    this.setState(() => ({ busy: true, isLoading: true }), this.commit)
  }

  onError = (transaction) => {
    this.setState(() => ({ busy: false, isLoading: false }))
    this.props.setErrors(transaction)

    toast.error("К сожалению возникла ошибка", {
      position: toast.POSITION.TOP_RIGHT,
    })
  }

  onCompleted = (payload) => {
    const { createIndividualProfile } = payload

    let showModal = false
    let profileId = ""

    if (createIndividualProfile) {
      const {
        profile: { id },
      } = createIndividualProfile
      if (id) {
        showModal = true
        profileId = id
      }
    }

    this.setState(
      () => ({
        busy: false,
        isLoading: false,
        showModal,
      }),
      () => {
        if (showModal) {
          this.setState(() => ({ id: profileId }))
        }
      },
    )

    if (this.props.onCompleted) {
      this.props.onCompleted(payload)
    }

    toast.success("Изменения успешно сохранены", {
      position: toast.POSITION.TOP_RIGHT,
    })
  }

  commit = () => {
    const variables = {
      input: {
        name: this.state.name,
        signedAvatar: undefined,
        signedPhone: undefined,
      },
    }

    if (this.state.signedPhone.length) {
      variables.input.signedPhone = this.state.signedPhone
    }

    if (this.state.signedAvatar.length) {
      variables.input.signedAvatar = this.state.signedAvatar
    }

    const callbacks = {
      onError: this.onError,
      onCompleted: this.onCompleted,
    }

    if (this.state.id.length > 0) {
      this.update(variables, callbacks)
    } else {
      this.create(variables, callbacks)
    }
  }

  create = (variables, callbacks) => {
    CreateIndividualProfile.commit(
      this.props.relay.environment,
      variables,
      null,
      callbacks,
    )
  }

  update = (variables, callbacks) => {
    UpdateIndividualProfile.commit(
      this.props.relay.environment,
      {
        ...variables,
        input: {
          ...variables.input,
          id: this.state.id,
        },
      },
      null,
      callbacks,
    )
  }

  /* eslint-disable no-underscore-dangle */
  renderFormContent = () => (
    <React.Fragment>
      <RowsContainer>
        <FormRow>
          <Label>
            <Translate
              i18n="individual_account.update.full_name"
              ns="components"
            />
          </Label>
          <Box width={480}>
            <TextField
              type="text"
              name="name"
              disabled={this.state.approvedOnce}
              value={this.state.name}
              onChange={this.onChange}
              error={ErrorForField(
                this.props.getError("updateIndividualProfile.name"),
              )}
            />
          </Box>
        </FormRow>
        <FormRow>
          <Label>
            <Translate
              i18n="individual_account.update.phone.label"
              ns="components"
            />
          </Label>
          <Box width={380}>
            <PhoneConfirmInput
              applySignedPhone={this.applySignedPhone}
              phone={this.state.phone}
              onChange={this.onChange}
            />
          </Box>
        </FormRow>
      </RowsContainer>
      <ImgUploader
        url={this.state._avatar.url}
        onAvatarDelete={this.onAvatarDelete}
        onAvatarUpdate={this.onAvatarUpdate}
        environment={this.props.relay.environment}
      />
    </React.Fragment>
  )
  /* eslint-enable no-underscore-dangle */

  renderFooter = () => (
    <FormControls
      isLoading={this.state.isLoading}
      onCancel={this.props.onCancel}
      onSubmit={this.onSubmit}
    />
  )

  renderHeader = () => <FormHeader inEdit={this.state.id.length} />

  render() {
    return (
      <React.Fragment>
        <Template
          layout="card"
          header={this.renderHeader}
          footer={this.renderFooter}
          content={this.renderFormContent}
        />
        {this.state.showModal && (
          <AccreditationSuggestModal
            onClose={this.onModalClose}
            link={`/accounts/${this.state.id}/accreditation`}
          />
        )}
      </React.Fragment>
    )
  }
}

const render = (props) => (
  <React.Fragment>
    <ToastContainer autoClose={4000} hideProgressBar={true} />
    <ErrorsContainer>
      {(errors) => (
        <RelayEnvironmentConsumer>
          {({ environment }) => (
            <Form {...props} {...errors} environment={environment} />
          )}
        </RelayEnvironmentConsumer>
      )}
    </ErrorsContainer>
  </React.Fragment>
)

export default Relay.createFragmentContainer(render, {
  profile: graphql`
    fragment ManageIndividual_profile on IndividualProfile {
      id
      name
      phone
      approvedOnce
      _avatar {
        url
      }
    }
  `,
})

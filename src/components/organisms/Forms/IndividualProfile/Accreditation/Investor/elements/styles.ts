import styled from "styled-components"

const FormContainer = styled.div`
  padding: 0;
  margin: 0;
`

export default FormContainer

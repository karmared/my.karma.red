import { pipe } from "src/utils"

const validateBirthDate = ({ data, errors }) => {
  if (!data.birthDate) {
    const error = { dataPath: "/birthDate", keyword: "format" }
    return { data, errors: [...errors, error] }
  }

  return { data, errors }
}

const formatErrors = (mutationName) => ({ errors }) => ({
  path: [mutationName],
  extensions: {
    validations: [...errors],
  },
})

const promisifyValidationResult = (error) => (error.extensions.validations.length
  ? Promise.reject(error)
  : Promise.resolve())

const validate = (shouldSkipValidation) => (mutationName, data) => {
  if (shouldSkipValidation) return Promise.resolve()

  const executeValidation = pipe(validateBirthDate)
  const resolveValidation = pipe(
    executeValidation,
    formatErrors(mutationName),
    promisifyValidationResult,
  )

  return resolveValidation({
    data,
    errors: [],
  })
}

export default validate

/* eslint-disable camelcase */
import { History } from "history"
import { RussianEntrepreneurFragment_profile } from "./__generated__/RussianEntrepreneurFragment_profile.graphql"

import {
  IAddress,
  IPassportChanged,
  IViewer,
  SexEnum,
} from "../Russian/types"

export type IEntrepreneurStateWithBank = RussianEntrepreneurFragment_profile & {
  checkingAccount: string | string;
  correspondentAccount: string | null;
  bankName: string | null;
  bic: string | null;
}

export type IEntrepreneurProps = {
  createEntrepreneurProfile: (value: boolean) => void;
  errors: any;
  history: History;
  profile: RussianEntrepreneurFragment_profile;
  relay: any;
  requestAccreditation: (value: string) => void;
  requestToken: (value: IEntrepreneurProfileSmallDataset) => void;
  updateIndividualProfile: (value: boolean) => void;
  viewer: IViewer;
}

export type IEntrepreneurProfileSmallDataset = {
  address: IAddress;
  bic: string | null;
  birthDate: string | null;
  birthPlace: string | null;
  checkingAccount: string | null;
  iian: string | null;
  inn: string | null;
  isPublicOfficial: boolean | null;
  isRussiaTaxResident: boolean;
  passport: IPassportChanged;
  profileId: string;
  psrnie: string | null;
  sex: SexEnum | null;
}

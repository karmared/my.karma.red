import React from "react"

import { ErrorsContainer } from "src/containers"
import { PurposedMutationProvider } from "src/context"
import {
  AddEntrepreneurProfile,
  RequestProfileAccreditation,
  RequestProfilePhoneConfirmation,
  SetEntrepreneurProfile,
  UpdateEntrepreneurProfileAsInvestor,
} from "src/mutations"
import { useMutation } from "src/hooks"
import { pipe, createChainedFunction, promisifyHookedMutation } from "src/utils"

import { profileDataFromState, investorDataFromState } from "./utils"

const processEntrepreneurProfileMutations = [
  "setProfile",
  "requestAccreditation",
  "addEntrepreneurProfile",
  "updateEntrepreneurProfileAsInvestor",
]

const ProcessEntrepreneurProfile = React.memo((props: any) => {
  const { errors } = props
  const { setErrors } = errors

  const use = pipe(useMutation, promisifyHookedMutation)

  const addProfile = use(AddEntrepreneurProfile)
  const updateProfile = use(SetEntrepreneurProfile)
  const updateInvestorData = use(UpdateEntrepreneurProfileAsInvestor)

  const requestPhoneConfirmation = use(RequestProfilePhoneConfirmation)
  const requestProfileAccreditation = use(RequestProfileAccreditation)

  const throwError = (err) => {
    throw new Error(err)
  }

  const handleErrors = createChainedFunction(setErrors, throwError)

  const createEntrepreneurProfile = (shouldSkipValidation) => (notify) => (
    formState,
  ) => {
    const input = {
      ...profileDataFromState(formState),
    }

    return addProfile({ input })
      .then((data) => {
        const {
          addEntrepreneurProfile: { profile },
        } = data

        let updatedDataInput = {}
        if (formState.bic || formState.checkingAccount) {
          updatedDataInput = {
            bic: formState.bic || "",
            checkingAccount: formState.checkingAccount || "",
            ...investorDataFromState(formState),
            profileId: profile.id,
            shouldSkipValidation,
          }
        } else {
          updatedDataInput = {
            ...investorDataFromState(formState),
            profileId: profile.id,
            shouldSkipValidation,
          }
        }

        return updateInvestorData({
          input: updatedDataInput,
        }).catch((e) => {
          setErrors(e)
          notify("error")

          return {
            data,
            errorFromUpdate: true,
          }
        })
      })
      .catch(handleErrors)
  }

  const updateEntrepreneurProfile = (shouldSkipValidation) => (notify) => (
    formState,
  ) => {
    const input = {
      profile: profileDataFromState(formState),
      accreditation: {
        ...investorDataFromState(formState),
        shouldSkipValidation,
      },
    }

    let updatedDataInput = {}
    if (formState.bic || formState.checkingAccount) {
      updatedDataInput = {
        bic: formState.bic || "",
        checkingAccount: formState.checkingAccount || "",
        ...investorDataFromState(formState),
        shouldSkipValidation,
      }
    } else {
      updatedDataInput = {
        ...investorDataFromState(formState),
        shouldSkipValidation,
      }
    }

    return updateInvestorData({
      input: updatedDataInput,
    })
      .then((data) => updateProfile(input).catch((e) => {
        setErrors(e)
        notify("error")

        return {
          data,
          errorFromUpdate: true,
        }
      }))
      .catch(handleErrors)
  }

  const requestToken = (formState) => {
    const tags = []
    if (formState.bic || formState.checkingAccount) {
      tags.push("CONFIRM_BANK_ACCOUNT_WITH_PHONE")
    }
    requestPhoneConfirmation({ input: { profileId: formState.id, kind: "REQUEST_PROFILE_ACCREDITATION", tags } })
      .then(
        ({ requestProfilePhoneConfirmation }) => requestProfilePhoneConfirmation.status,
      )
      .catch(handleErrors)
  }

  const requestAccreditation = (profileId) => ({ input: injectedInput }) => {
    const { token } = injectedInput

    const input = {
      role: "INVESTOR",
      token,
      profileId,
    }

    return requestProfileAccreditation({ input }).catch(handleErrors)
  }

  return (
    <PurposedMutationProvider value={processEntrepreneurProfileMutations}>
      {props.children({
        errors,
        requestToken,
        requestAccreditation,
        createEntrepreneurProfile,
        updateEntrepreneurProfile,
      })}
    </PurposedMutationProvider>
  )
})

export default (props) => (
  <ErrorsContainer>
    {(errors) => <ProcessEntrepreneurProfile errors={errors} {...props} />}
  </ErrorsContainer>
)

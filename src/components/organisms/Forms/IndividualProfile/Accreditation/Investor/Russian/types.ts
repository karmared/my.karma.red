/* eslint-disable camelcase */
import { History } from "history"
import { RussianIndividualFragment_profile } from "./__generated__/RussianIndividualFragment_profile.graphql"

export type SexEnum = "FEMALE" | "MALE" | "%future added value";

export type IAddress = {
  country: string | null;
  region: string | null;
  area: string | null;
  locality: string | null;
  street: string | null;
  house: string | null;
  housing: string | null;
  structure: string | null;
  flat: string | null;
  raw?: string | null;
}

export type IPassport = {
  number: string;
  series: string;
  issuedAt: string;
  scans: Array<{
    id: string;
    url: string;
    filename: string;
  } | string>;
  issuedBy: {
    code: string | null;
    name: string | null;
  } | null;
  postponeScans: boolean;
}

export type IPassportChanged = IPassport & {
  code: string;
  name: string;
}

export type IViewer = {
  id: string,
  createdAt: string
}

export type IIndividualProps = {
  createIndividualProfile: (value: boolean) => void;
  errors: any;
  history: History;
  profile: RussianIndividualFragment_profile;
  relay: any;
  requestAccreditation: (value: string) => void;
  requestToken: (value: IIndividualProfileSmallDataset) => void;
  updateIndividualProfile: (value: boolean) => void;
  viewer: IViewer;
}

export type IIndividualStateWithBank = RussianIndividualFragment_profile & {
  checkingAccount: string | string;
  correspondentAccount: string | null;
  bankName: string | null;
  bic: string | null;
}

export type IIndividualProfileSmallDataset = {
  address: IAddress;
  birthDate: string | null;
  birthPlace: string | null;
  iian: string | null;
  inn: string | null;
  isPublicOfficial: boolean | null;
  isRussiaTaxResident: boolean;
  passport: IPassportChanged;
  profileId: string;
  sex: SexEnum | null;
  readonly supportingDocuments?: Array<{
    id: string;
    attachment: {
      id: string;
      url: string;
      size: number;
      width: number | null;
      height: number | null;
      format: string | null;
      filename: string;
    };
    readonly status: "APPROVED" | "DECLINED" | "INITIAL" | "PENDING" | null;
}>;
}

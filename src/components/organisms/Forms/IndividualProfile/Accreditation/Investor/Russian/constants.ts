const Address = {
  country: "",
  region: "",
  area: "",
  locality: "",
  street: "",
  house: "",
  housing: "",
  structure: "",
  flat: "",
}

const IssuedBy = {
  code: "",
  name: "",
}

const Passport = {
  number: "",
  series: "",
  issuedAt: "",
  scans: [],
  issuedBy: { ...IssuedBy },
  postponeScans: false,
}

const InititalState = {
  id: "",
  lastName: "",
  firstName: "",
  middleName: "",
  phone: "",
  inn: "",
  iian: "",
  address: { ...Address },
  passport: { ...Passport },
  approvedOnce: false,
  birthDate: "",
  birthPlace: "",
  isPublicOfficial: false,
  isRussiaTaxResident: true,
  sex: "MALE",
}

export default InititalState

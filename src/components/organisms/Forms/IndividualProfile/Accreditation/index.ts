import Investor from "./Investor"
import Borrower from "./Borrower"

const render = () => null

render.investor = Investor
render.borrower = Borrower

export default render

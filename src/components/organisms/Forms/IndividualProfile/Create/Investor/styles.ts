import styled from "styled-components"

const FormContainer = styled.div`
  border-bottom: 1px solid ${({ theme }) => theme.colors.grey};
  margin-top: 40px;
`

export default FormContainer

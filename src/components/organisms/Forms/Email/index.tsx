import ChangeEmail from "./ChangeEmail"
import ConfirmationEmail from "./ConfirmationEmail"

export default {
  change: ChangeEmail,
  confirmation: ConfirmationEmail,
}

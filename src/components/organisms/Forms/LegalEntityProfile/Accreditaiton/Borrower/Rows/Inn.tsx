import React from "react"

import {
  Box, Label, FormRow, Translate, TextField,
} from "src/components"

export class Inn extends React.Component<any, any> {
  state = {
    inn: this.props.value,
  }

  onChange = (event) => {
    const { name, value } = event.target
    this.setState(() => ({ [name]: value }), this.forceChange)
  }

  forceChange = () => {
    this.props.onChange({
      name: this.props.name,
      value: this.state.inn,
    })
  }

  render() {
    return (
      <FormRow>
        <Box width={260}>
          <Label whiteSpace="pre-line">
            <Translate
              i18n="accreditation.individual.rows.inn.label"
              ns="components"
            />
          </Label>
        </Box>
        <Box width="100%">
          <TextField
            type="text"
            name="inn"
            value={this.state.inn}
            onChange={this.onChange}
          />
        </Box>
      </FormRow>
    )
  }
}

export default Inn

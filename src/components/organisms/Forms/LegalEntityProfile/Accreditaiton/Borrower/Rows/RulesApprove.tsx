import React from "react"

import {
  Box, FormRow, Translate, CheckBoxField,
} from "src/components"

export default ({ value, onChange }) => (
  <FormRow>
    <Box width={240}></Box>
    <Box width="100%">
      <CheckBoxField
        checked={value}
        onChange={onChange}
        name="rulesApprove"
        label={
          <Translate
            i18n="accreditation.borrower.rows.rules_approve.hint"
            ns="components"
          />
        }
      />
    </Box>
  </FormRow>
)

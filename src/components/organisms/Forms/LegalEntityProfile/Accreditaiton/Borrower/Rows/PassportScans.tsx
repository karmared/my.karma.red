import React from "react"

import {
  Box, Label, Translate, AttachmentsListInput,
} from "src/components"

class PassportScans extends React.Component<any, any> {
  state = {
    attachments: this.props.value,
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  onChange = ({ name, value }) => {
    this.setState(() => ({ attachments: value.attachments }), this.forceUpdate)
  }

  forceUpdate = () => {
    this.props.onChange({
      name: this.props.name,
      value: this.state.attachments.map(({ id }) => id),
    })
  }

  render() {
    return (
      <React.Fragment>
        <Box>
          <Label whiteSpace="pre-line">
            <Translate
              i18n="accreditation.borrower.rows.passport_scan.label"
              ns="components"
            />
          </Label>
          <Label whiteSpace="pre-line" fontSize="12px">
            <Translate
              i18n="accreditation.borrower.rows.passport_scan.hint"
              ns="components"
            />
          </Label>
        </Box>
        <Box width="100%">
          <AttachmentsListInput
            environment={this.props.environment}
            extensions={"image/jpeg,image/png"}
            onChange={this.onChange}
            value={{ attachments: this.state.attachments }}
            name="files"
          />
        </Box>
      </React.Fragment>
    )
  }
}

export default PassportScans

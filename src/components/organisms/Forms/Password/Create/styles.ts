import styled from "styled-components"

export const Container = styled.div`
  position: relative;
  font-family: Geometria, sans-serif;
  margin: 100px auto 32px;
  width: 100%;
  display: flex;
  flex-direction: column;
  background-color: white;
  color: ${(props) => props.theme.colors.primaryBlack};
  max-width: 420px;
  box-shadow: 0 3px 14px rgba(0, 0, 0, 0.25);
  border-radius: 4px;
  padding: 40px 30px 30px;

  @media screen and (max-width: 480px) {
    max-width: 100%;
    border-radius: 0;
    box-shadow: none;
    padding: 0;
  }
`

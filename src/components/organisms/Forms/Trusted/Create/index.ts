import Legal from "./Legal"
import Entrepreneur from "./Entrepreneur"

export default {
  legal: Legal,
  entrepreneur: Entrepreneur,
}

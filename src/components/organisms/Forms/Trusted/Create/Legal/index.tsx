/* eslint-disable max-len */

import React, { useState } from "react"
import is from "is_js"
import { withRouter } from "react-router-dom"
import { toast, ToastContainer } from "react-toastify"

import { ErrorsContainer } from "src/containers"
import {
  Address,
  Box,
  CheckBoxField,
  FloatingLabelInput,
  Switch,
  Tooltip,
  Translate,
} from "src/components"
import {
  RegisterUser,
  CreateLegalEntityProfile,
  UpdateLegalEntityProfileBorrowerData,
} from "src/mutations"
import { useEnvironment } from "src/hooks"

import { PassportDataField } from "../../../LegalEntityProfile/Accreditaiton/Borrower/Rows"

import { getInitialValues, formatStrings, formatScans } from "./utils"
import {
  Button,
  ButtonContainer,
  Container,
  Form,
  InputTooltip,
  Title,
  TitleTooltip,
  RedButton,
} from "../styles"

function CreateTrustedAccount(props) {
  const {
    isEditable = false,
    user: propsUser,
    profile: propsProfile,
    history,
    createOrder,
  } = props

  const [values, setValues] = useState(
    getInitialValues(propsUser, propsProfile),
  )
  const [isChecked, setChecked] = useState(false)
  const [isLoading, setLoading] = useState(false)
  const [user, setUser] = useState(
    propsUser && { registerUser: { user: propsUser } },
  )
  const [profile, setProfile] = useState(propsProfile)

  const { environment } = useEnvironment()

  function handleChange(event) {
    const { name, value } = event.target

    setValues({
      ...values,
      [name]: typeof values[name] === "boolean" ? !values[name] : value,
    })
  }

  function handleSelectChange({ name, value }) {
    setValues({
      ...values,
      [name]: value,
    })
  }

  function createUser(inputs) {
    return new Promise((resolve, reject) => {
      const variables = {
        input: {
          email: inputs.email,
        },
      }

      const callbacks = {
        onError: reject,
        onCompleted: (res) => {
          setUser(res)
          resolve(res)
        },
      }

      RegisterUser.commit(environment, variables, null, callbacks)
    })
  }

  function createProfile(inputs, { registerUser }: any) {
    const { id: userId } = registerUser.user

    return new Promise((resolve, reject) => {
      const variables = {
        input: {
          inn: inputs.inn,
          name: inputs.name,
          phone: inputs.phone,
          userId,
        },
      }

      const callbacks = {
        onError: reject,
        onCompleted: (res) => {
          setProfile(res)
          resolve(res)
        },
      }

      CreateLegalEntityProfile.commit(environment, variables, null, callbacks)
    })
  }

  function updateProfile(inputs, { createLegalEntityProfile: res }: any) {
    const { id: profileId } = res.profile

    return new Promise((resolve, reject) => {
      const variables = {
        input: {
          profileId,
          name: inputs.name,
          inn: inputs.inn,
          kpp: inputs.kpp,
          ogrn: inputs.ogrn,
          iian: inputs.iian,
          publicEmail: inputs.publicEmail,
          address: inputs.mainAddress,
          business: {
            main: inputs.main,
            aux: inputs.aux,
            legalForm: inputs.legalForm,
            startedAt: inputs.startedAt,
          },
          passport: formatScans(inputs.passport),
          executive: {
            name: inputs.ceoName,
          },
          actualAddress: inputs.addressMatch
            ? inputs.mainAddress
            : inputs.otherAddress,
        },
      }

      const callbacks = {
        onError: reject,
        onCompleted: resolve,
      }

      UpdateLegalEntityProfileBorrowerData.commit(
        environment,
        variables,
        null,
        callbacks,
      )
    })
  }

  function getError(path) {
    const error = props.getError(path)

    if (is.not.existy(error)) return null
    return <Translate i18n={`${error.path}.${error.keyword}`} ns="errors" />
  }

  function handleError(error) {
    const { setErrors } = props

    const errorMessage = error.errors?.[0]?.message || "Ошибка"
    toast.error(errorMessage, {
      position: toast.POSITION.TOP_RIGHT,
    })

    setErrors(error)
    setLoading(false)
  }

  function handleComplete({ updateLegalEntityProfileBorrowerData: res }) {
    const { id } = res.profile

    setLoading(false)
    history.push(`/trusted/${id}/info`)
  }

  function handleSubmit(event) {
    event.preventDefault()

    setLoading(true)
    const formatted = formatStrings(values)

    if (isEditable) {
      const borrowerProfile = propsProfile

      const { id: userId } = borrowerProfile

      updateProfile(formatted, {
        createLegalEntityProfile: { profile: { id: userId } },
      })
        .then(handleComplete)
        .catch(handleError)

      return
    }

    if (profile) {
      updateProfile(formatted, profile).then(handleComplete).catch(handleError)

      return
    }

    if (user) {
      createProfile(formatted, user)
        .then((res) => updateProfile(formatted, res))
        .then(handleComplete)
        .catch(handleError)

      return
    }

    createUser(formatted)
      .then((res) => createProfile(formatted, res))
      .then((res) => updateProfile(formatted, res))
      .then(handleComplete)
      .catch(handleError)
  }

  return (
    <Form onSubmit={handleSubmit}>
      <Container>
        <Title>Анкетные данные</Title>
        <FloatingLabelInput
          value={values.name}
          onChange={handleChange}
          name="name"
          label="Наименование"
          required
          error={getError("createLegalEntityProfile.name")}
          hasError={getError("createLegalEntityProfile.name")}
        />
        <FloatingLabelInput
          value={values.phone}
          onChange={handleChange}
          name="phone"
          label="Телефон"
          mask="+79999999999"
          required
          error={getError("createLegalEntityProfile.phone")}
          hasError={getError("createLegalEntityProfile.phone")}
        />
        <FloatingLabelInput
          value={values.email}
          onChange={handleChange}
          name="email"
          label="Email"
          required
          disabled={propsUser}
          error={getError("registerUser.email")}
          hasError={getError("registerUser.email")}
        />
        <FloatingLabelInput
          value={values.publicEmail}
          onChange={handleChange}
          name="publicEmail"
          label="Публичный Email"
        />
        <FloatingLabelInput
          value={values.inn}
          onChange={handleChange}
          name="inn"
          label="ИНН"
          required
          mask="999999999999"
          error={getError("createLegalEntityProfile.inn")}
          hasError={getError("createLegalEntityProfile.inn")}
        />
        <FloatingLabelInput
          value={values.kpp}
          onChange={handleChange}
          name="kpp"
          label="КПП"
          mask="9999999999"
        />
        <FloatingLabelInput
          value={values.ogrn}
          onChange={handleChange}
          name="ogrn"
          label="ОГРН"
          mask="9999999999999"
        />

        <Title>Данные о бизнесе</Title>
        <FloatingLabelInput
          value={values.legalForm}
          onChange={handleChange}
          name="legalForm"
          label="Организационно правовая форма"
        />
        <FloatingLabelInput
          value={values.startedAt}
          onChange={handleChange}
          name="startedAt"
          label="С какого времени существует бизнес"
        />
        <InputTooltip>
          <Translate
            i18n="accreditation.borrower.rows.start_date.hint"
            ns="components"
          />
          <Tooltip text="tooltips.start_date" />
        </InputTooltip>

        <Address.autoSuggest
          value={values.mainAddress}
          onChange={handleSelectChange}
          name="mainAddress"
          label="Юридический адрес организации"
        />

        <Title>
          Фактический адрес органицации совпадает юридическим адрессом
          организации, указанным в ЕГРЮЛ?
        </Title>
        <Box mb={20}>
          <Switch
            variant="tablet"
            name="addressMatch"
            value={values.addressMatch}
            onChange={handleChange}
          />
        </Box>
        <Address.autoSuggest
          disabled={values.addressMatch}
          name="otherAddress"
          value={values.otherAddress}
          onChange={handleSelectChange}
          label="Иной адрес"
        />

        <TitleTooltip>
          <Translate
            i18n="accreditation.borrower.rows.activity_kind.label"
            ns="components"
          />
          <Tooltip text="tooltips.activity_kind" />
        </TitleTooltip>
        <FloatingLabelInput
          value={values.main}
          onChange={handleChange}
          name="main"
          label="Основной"
        />
        <FloatingLabelInput
          value={values.aux}
          onChange={handleChange}
          name="aux"
          label="Дополнительный"
        />

        <Title>Данные руководителя</Title>
        <FloatingLabelInput
          value={values.ceoName}
          onChange={handleChange}
          name="ceoName"
          label="ФИО"
        />
        <FloatingLabelInput
          value={values.iian}
          onChange={handleChange}
          name="iian"
          label="СНИЛС"
          mask="99999999999"
        />

        <Title>Сканы паспорта</Title>
        <PassportDataField
          name="passport"
          value={values.passport}
          onChange={handleSelectChange}
          environment={environment}
          withoutHeader
        />
      </Container>

      <Box mt={60}>
        <CheckBoxField
          value={isChecked}
          onChange={() => setChecked(!isChecked)}
          color="#4a4a4a"
          fontSize="18px"
          fontWeight="bold"
          alignItems="flex-start"
          label={
            "Настоящим я подтверждаю, что вся введенная выше информация соответствует действительности и проверена мной лично."
          }
        />
      </Box>

      <ButtonContainer>
        <Button disabled={!isChecked || isLoading} type="submit">
          {isEditable && "Сохранить"}
          {!isEditable && "Создать"}
        </Button>

        {propsProfile?.user?.email && createOrder && (
          <RedButton onClick={createOrder} type="button">
            Создать заявку
          </RedButton>
        )}
      </ButtonContainer>
    </Form>
  )
}

function Render(props) {
  return (
    <>
      <ToastContainer autoClose={4000} hideProgressBar={true} />
      <ErrorsContainer>
        {({ getError, setErrors, clearError }) => (
          <CreateTrustedAccount
            {...props}
            getError={getError}
            setErrors={setErrors}
            clearError={clearError}
          />
        )}
      </ErrorsContainer>
    </>
  )
}

export default withRouter(Render)

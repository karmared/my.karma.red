export { default as BalanceBlock } from "./Balance"
export { default as InvestmentsBlock } from "./Investments"
export { default as ProfitBlock } from "./Profit"
export { default as RiskBlock } from "./Risk"

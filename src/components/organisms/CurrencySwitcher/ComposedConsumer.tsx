import React from "react"

import {
  LocaleConsumer,
  CurrencyConsumer,
  ViewerDataConsumer,
  ExchangeRateConsumer,
} from "src/context"

export default (props) => {
  const { children } = props

  return (
    <ExchangeRateConsumer>
      {(exchangeRateData) => (
        <ViewerDataConsumer>
          {({ country }) => (
            <LocaleConsumer>
              {(locale) => (
                <CurrencyConsumer>
                  {(currency) => children({
                    locale,
                    country,
                    currency,
                    exchangeRateData,
                  })
                  }
                </CurrencyConsumer>
              )}
            </LocaleConsumer>
          )}
        </ViewerDataConsumer>
      )}
    </ExchangeRateConsumer>
  )
}

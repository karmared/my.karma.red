import Loans from "./Loans"
import Investments from "./Investments"

const render = () => null

render.loans = Loans
render.investments = Investments

export default render

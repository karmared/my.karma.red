import React from "react"
import { Route } from "react-router"

import {
  Text, Button, Template, Translate,
} from "src/components"

const handleRoute = (history) => () => history.push("/orders/new")

const Action = () => (
  <Route>
    {({ history }) => (
      <Button variant="blueWide" onClick={handleRoute(history)}>
        <Text fontSize="16px">
          <Translate i18n="components:order_list.loans.emptyList.COMPLETED.button" />
        </Text>
      </Button>
    )}
  </Route>
)

export default () => (
  <Template
    layout="empty"
    header="components:order_list.loans.emptyList.COMPLETED.header"
    image="/images/orderLists/empty/loans.svg"
    description="components:order_list.loans.emptyList.COMPLETED.description"
    action={<Action />}
  />
)

import styled from "styled-components"

export const OrderContainer = styled.div`
  display: flex;
  min-height: 50px;
  margin-bottom: 20px;
  max-height: 250px;
  border-radius: 5px;
  flex-direction: column;
  align-items: center;
  position: relative;
  border: 1px solid ${({ theme }) => theme.colors.grey};
`

export const CreateDateWrapper = styled.div``

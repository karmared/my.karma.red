import React from "react"
import styled from "styled-components"
import { useMediaPredicate } from "react-media-hook"

const Wrapper = styled.div`
  position: relative;
  width: 100%;
  height: auto;
`

export const DesktopWrapper = (props) => {
  const mobile = useMediaPredicate("(max-width: 800px)")
  return !mobile && <Wrapper {...props}/>
}

export const MobileWrapper = (props) => {
  const mobile = useMediaPredicate("(max-width: 800px)")
  return mobile && <Wrapper {...props}/>
}

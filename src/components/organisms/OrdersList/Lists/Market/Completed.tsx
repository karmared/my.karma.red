import React from "react"

import {
  LoaderBox,
  MiniLoader,
  OrderFactory,
  LoadMore,
  CustomTooltip,
} from "src/components"
import { MarketCompletedOrdersListHugeQueryContainer } from "src/query"
import { parseHugeOrdersData } from "src/utils"

import {
  List,
  OrderContainerItems,
  HeadTh,
  HeadTr,
  TableHead,
  Table,
  TableLoader,
  DesktopWrapper,
  MobileWrapper,
  TooltipText,
  TooltipLink,
  TooltipIcon,
  NotFoundBlock,
} from "./styles"

import {
  MOCK_ORDERS, MOBILE_MOCK_ORDERS, TABLE, MOBILE,
} from "./constants"

import { Header } from "./Elements/Header"

const Render = (props) => {
  const { isFetching } = props

  const [isLoading, setLoading] = React.useState(false)

  React.useEffect(() => {
    setLoading(false)
  }, [props.data?.orders?.edges])

  const handleLoad = () => {
    setLoading(true)
    props.load.handle()
  }
  const orders = parseHugeOrdersData(props.data)

  const hasFilter = Boolean(Object.values(props.filter).find(Boolean))
  const hasOrders = orders.length > 0

  const showTable = isFetching || hasOrders || hasFilter
  const showNotFound = !isFetching && hasFilter && !hasOrders

  return (
    <>
      {showTable && (
        <>
          <Header
            title="Завершенные сделки"
            filter={props.filter}
            setFilter={props.setFilter}
          />
          <OrderContainerItems>
            <DesktopWrapper>
              {isFetching && (
                <TableLoader>
                  <MiniLoader margin="auto" />
                </TableLoader>
              )}
              <Table cellPadding="5">
                <TableHead>
                  <HeadTr>
                    <HeadTh width="18%">Название</HeadTh>
                    <HeadTh>№ заявки</HeadTh>
                    <HeadTh>
                      Рейтинг{" "}
                      <CustomTooltip
                        placement="bottom"
                        overlay={
                          <div style={{ width: "180px" }}>
                            <TooltipText>
                              Подробную информацию о расчете рейтинга можно
                              узнать{" "}
                              <TooltipLink to="/invest/scoring">
                                здесь
                              </TooltipLink>
                            </TooltipText>
                          </div>
                        }
                      >
                        <span>
                          <TooltipIcon />
                        </span>
                      </CustomTooltip>
                    </HeadTh>
                    <HeadTh>Сумма</HeadTh>
                    <HeadTh>Ставка</HeadTh>
                    <HeadTh>Дата получения</HeadTh>
                    <HeadTh>Дата погашения</HeadTh>
                    <HeadTh>Статус</HeadTh>
                  </HeadTr>
                </TableHead>
                <tbody>
                  {!isFetching
                    && hasOrders
                    && orders.map((data, index) => {
                      const Item = OrderFactory.createCompletedOrder(
                        data,
                        TABLE,
                      )
                      return <Item key={index} />
                    })}

                  {isFetching
                    && [...Array(MOCK_ORDERS)].map((item, i) => {
                      const Item = OrderFactory.createCompletedMock({}, TABLE)
                      return <Item key={i} />
                    })}
                </tbody>
              </Table>
            </DesktopWrapper>

            <MobileWrapper>
              <List>
                {!isFetching
                  && hasOrders
                  && orders.map((data, i) => {
                    const Item = OrderFactory.createMobileCompletedOrder(
                      data,
                      MOBILE,
                    )
                    return <Item key={i} />
                  })}

                {isFetching
                  && Array(MOBILE_MOCK_ORDERS)
                    .fill(0)
                    .map((data, i) => {
                      const Item = OrderFactory.createMobileCompletedMock(
                        {},
                        MOBILE,
                      )
                      return <Item key={i} />
                    })}
              </List>
            </MobileWrapper>
            {showNotFound && <NotFoundBlock>Не найдено</NotFoundBlock>}
          </OrderContainerItems>

          <LoaderBox>
            {props.load.isNext && !isLoading && (
              <LoadMore onClick={handleLoad}>+ загрузить еще</LoadMore>
            )}
            {props.load.isNext && isLoading && <MiniLoader />}
          </LoaderBox>
        </>
      )}
    </>
  )
}

export default () => (
  <MarketCompletedOrdersListHugeQueryContainer render={Render} />
)

import React from "react"
import PropTypes from "prop-types"

class Dynamic extends React.Component<any, any> {
  static propTypes = {
    load: PropTypes.func.isRequired,
  }

  state = {
    component: null,
  }

  componentDidMount() {
    this.props.load().then((module) => {
      this.setState(() => ({ component: module.default || module }))
    })
  }

  render() {
    if (this.state.component === null) return null

    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const { load, ...props } = this.props

    return <this.state.component {...props} />
  }
}

export default Dynamic

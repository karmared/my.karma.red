import styled from "styled-components"

const Divider = styled.div`
  width: 20px;
  height: 10%;
`

export default Divider

import React from "react"

import {
  Container,
  Image,
  MainContainer,
  ButtonContainer,
  Title,
  Description,
  Link,
  Button,
  MobileButton,
} from "./styles"

function PersonalInformationBanner() {
  return (
    <Container>
      <Image />
      <MainContainer>
        <Title>Обновление условия передачи персональных данных</Title>
        <ButtonContainer>
          <Description>
            В связи с изменениями в законодательстве, мы внесли в
            пользовательское соглашение дополнительный <Link to="/">пункт</Link>
            . Чтобы продолжить пользоваться сервисом подпишите его с помощью
            смс.
          </Description>
          <Button>подтвердить по смс</Button>
        </ButtonContainer>
      </MainContainer>
      <MobileButton>подтвердить по смс</MobileButton>
    </Container>
  )
}

export default PersonalInformationBanner

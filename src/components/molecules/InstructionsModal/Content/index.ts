import Fee from "./Fee"
import Invest from "./Invest"

export default {
  fee: Fee,
  invest: Invest,
}

import styled from "styled-components"

const HeaderWrapper = styled.div`
  padding-bottom: 14px;
  border-bottom: 1px solid ${({ theme }) => theme.colors.grey};
`

export default HeaderWrapper

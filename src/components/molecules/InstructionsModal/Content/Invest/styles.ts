import styled from "styled-components"

export const ButtonWrapper = styled.div`
  display: flex;
  justify-content: flex-end;
  padding-top: 16px;
`

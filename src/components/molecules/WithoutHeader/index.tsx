import React from "react"
import { createGlobalStyle } from "styled-components"

export const GlobalStyles = createGlobalStyle`
  #app-root > div {
    min-width: inherit;
  }
`

const render = (props) => <GlobalStyles>{props.children}</GlobalStyles>

export default React.memo(render)

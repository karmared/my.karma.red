import styled from "styled-components"

const IconWrapper = styled.div<any>`
  width: 14px;
  height: 10px;
  transform: translateY(-30%) ${({ forward }) => forward && "rotateY(180deg)"};
`

export default IconWrapper

import styled from "styled-components"

export const TableWrap = styled.div`
  overflow: auto;
  max-height: 338px;
`

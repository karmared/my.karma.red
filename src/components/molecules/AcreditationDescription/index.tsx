import React from "react"

import {
  Text, Translate,
} from "src/components"

import Container from "./styles"

export default (props) => (
  <Container {...props}>
    <Text pb={16} fontSize={16}>
      <Translate i18n="profile.manage.accreditation.title" ns="components" />
    </Text>
    <Text pb={30} fontSize={14} whiteSpace="pre-line">
      <Translate
        i18n="profile.manage.accreditation.description"
        ns="components"
      />
    </Text>
    <Text pb={16} fontSize={14} whiteSpace="pre-line">
      <Translate i18n="profile.manage.accreditation.gutter" ns="components" />
    </Text>
  </Container>
)

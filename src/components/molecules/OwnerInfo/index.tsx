import React from "react"

import { Box, Text, Translate } from "src/components"

export default ({ phone }) => (phone ? (
    <Box pt={16} pb={10}>
      <Text textTransform="capitalize" fontSize="13px" color="greyDark">
        <Translate i18n="account.header.labels.phone" ns="components" />
      </Text>

      <Box py="10px">
        <Text fontSize={14}>{phone}</Text>
      </Box>
    </Box>
) : null)

export const PATH = {
  LOANS: "/orders/my/loans",
  INVESTMENTS: "/orders/my/investments",
  PAYMENTS: "/orders/my/payments",
  ANALYTICS: "/orders/my/analytics",
  AUTOINVEST: "/orders/my/autoinvest",
}

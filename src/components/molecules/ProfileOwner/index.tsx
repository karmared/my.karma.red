import React from "react"

import { Box, Text, Translate } from "src/components"

export default ({ type, name }) => (
  <Box>
    <Text fontSize={16} color="greyDark" textTransform="uppercase">
      <Translate
        i18n={`user-page-header.profile_type.${type}`}
        ns="components"
      />
    </Text>
    <Box py="10px">
      <Text fontSize={24}>{name}</Text>
    </Box>
  </Box>
)

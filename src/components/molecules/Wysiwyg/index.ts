import Editor from "./Editor"
import Render from "./Render"

export default {
  Editor,
  Render,
}

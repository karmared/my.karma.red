import { getProperty, addressObjectToString } from "src/utils"

export function getGroups(profile) {
  const registrationAddress = getProperty(
    profile,
    "registrationAddress",
    null,
  )
  const scans = getProperty(profile, "passport.scans", [])

  const series = getProperty(profile, "passport.series", "")
  const passportNumber = getProperty(profile, "passport.number", "")
  const passport = series && passportNumber ? `${series} / ${passportNumber}` : ""
  const passportIssuedAt = getProperty(profile, "passport.issuedAt", "")

  return [
    {
      title: "Анкетные данные",
      fields: [
        {
          type: "text",
          label: "ф.и.о.",
          value: getProperty(profile, "name", profile.name),
        },
        {
          type: "text",
          label: "телефон",
          value: profile.phone,
        },
        {
          type: "text",
          label: "инн",
          value: getProperty(profile, "inn", ""),
        },
        {
          type: "text",
          label: "снилс",
          value: getProperty(profile, "iian", ""),
        },
        {
          type: "text",
          label: "дата рождения",
          value: getProperty(profile, "birthDate", "")
            .split("-")
            .reverse()
            .join("-"),
        },
        {
          type: "text",
          label: "место рождения",
          value: getProperty(profile, "birthPlace", ""),
        },
      ],
    },
    {
      title: "Паспортные данные гражданина РФ",
      fields: [
        {
          type: "text",
          label: "серия и номер",
          value: passport,
        },
        {
          type: "text",
          label: "дата выдачи",
          value: passportIssuedAt.split("-").reverse().join("-"),
        },
        {
          type: "text",
          label: "код подразделения",
          value: getProperty(profile, "passport.issuedBy.code", ""),
        },
        {
          type: "text",
          label: "кем выдан",
          value: getProperty(profile, "passport.issuedBy.name", ""),
        },
      ],
    },
    {
      title: "Сканы паспорта",
      fields:
        scans.length > 1000
          ? scans.map((item) => ({ type: "attachment", value: item }))
          : [{ type: "attachment" }],
    },
    {
      title: "Действующий адрес регистрации",
      fields: [
        {
          type: "text",
          label: "адрес",
          value:
            registrationAddress && addressObjectToString(registrationAddress),
        },
      ],
    },
    {
      title: "Являетесь ли вы налоговым резидентом Российской Федерации?",
      fields: [
        {
          type: "text",
          label: "ответ",
          value: getProperty(profile, "isRussiaTaxResident", false)
            ? "Да"
            : "Нет",
        },
      ],
    },
    {
      /* eslint-disable */
      title:
        "Являетесь ли вы публичным должностным лицом или его близким родственником (супруг(а), мать, отец, полнородные или неполнородные брат, сестра, сын, дочь, усыновитель, усыновленный) или действуете от имени указанных лиц?",
      /* eslint-enable */
      fields: [
        {
          type: "text",
          label: "ответ",
          value: getProperty(profile, "isPublicOfficial", false)
            ? "Да"
            : "Нет",
        },
      ],
    },
    {
      title: "components:account.bank_account.info.requisites",
      fields: [
        {
          type: "text",
          label: "components:account.bank_account.info.account_info.name",
          value: getProperty(profile, "bankAccounts[0].bank.name", ""),
        },
        {
          type: "text",
          label: "components:account.bank_account.info.account_info.bic",
          value: getProperty(profile, "bankAccounts[0].bank.bic", ""),
        },
        {
          type: "text",
          label: "components:account.bank_account.info.account_info.checking_account",
          value: getProperty(profile, "bankAccounts[0].checkingAccount", ""),
        },
        {
          type: "text",
          label: "components:account.bank_account.info.account_info.correspondent_account",
          value: getProperty(profile, "bankAccounts[0].correspondentAccount", ""),
        },
      ],
    },
  ]
}

import React from "react"

const OrderPositionData = React.createContext({})

export const OrderPositionProvider = OrderPositionData.Provider
export const OrderPositionConsumer = OrderPositionData.Consumer

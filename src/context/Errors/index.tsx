import React from "react"

export const FormErrorsContext: any = React.createContext({})

export const ErrorsProvider = FormErrorsContext.Provider
export const ErrorsConsumer = FormErrorsContext.Consumer

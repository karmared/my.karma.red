import React from "react"

const ProfileData = React.createContext({})

export const ProfileDataProvider = ProfileData.Provider
export const ProfileDataConsumer = ProfileData.Consumer

import React from "react"

export const PurposedMutationContext = React.createContext([])

export const PurposedMutationProvider = PurposedMutationContext.Provider
export const PurposedMutationConsumer = PurposedMutationContext.Consumer

import React from "react"

import { ErrorsContainer } from "src/containers"
import { LocaleConsumer, RelayEnvironmentConsumer } from "src/context"

const ComposedConsumer = (props) => (
  <LocaleConsumer>
    {({ refetchViewerLocale }) => (
      <ErrorsContainer>
        {(errors) => (
          <RelayEnvironmentConsumer>
            {({ environment, refreshEnvironment }) => props.children({
              errors,
              environment,
              refetchViewerLocale,
              refreshEnvironment,
            })
            }
          </RelayEnvironmentConsumer>
        )}
      </ErrorsContainer>
    )}
  </LocaleConsumer>
)

export default ComposedConsumer

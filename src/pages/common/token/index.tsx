import React from "react"
import { Link } from "react-router-dom"
import {
  Box, Text, FlexBox, Heading, Translate,
} from "src/components"

import { HighlightLayout } from "src/layouts"

import { setAuthToken } from "src/utils"
import { LoginWithAuthenticationToken } from "src/mutations"

import ComposedConsumer from "./ComposedConsumer"

class TokenPage extends React.Component<any, any> {
  commit = (token) => {
    const variables = {
      input: {
        token,
      },
    }

    const callbacks = {
      onError: this.onError,
      onCompleted: this.onCompleted,
    }

    LoginWithAuthenticationToken.commit(
      this.props.environment,
      variables,
      null,
      callbacks,
    )
  }

  onError = (transaction) => {
    this.props.setErrors(transaction)
  }

  onCompleted = ({ loginWithAuthenticationToken }) => {
    setAuthToken(loginWithAuthenticationToken.token).then(
      this.props.refetchViewerLocale,
    )

    this.props.refreshEnvironment()
    this.props.history.push("/password")
  }

  componentDidMount() {
    const { token } = this.props.match.params
    this.commit(token || "")
  }

  render() {
    const error = this.props.getError("loginWithAuthenticationToken.token")

    if (!error) return null

    return (
      <HighlightLayout>
        <Box mt={80}>
          <Heading.h1>Упс</Heading.h1>
        </Box>
        <Box is="h4" mt={40} maxWidth={768} width="100%">
          <Text fontSize={30} textAlign="center">
            <Translate i18n={`${error.path}.${error.keyword}`} ns="errors" />
          </Text>
        </Box>
        <FlexBox flex={1} />
        <Box is="p" m={0} mt={40} mb={80}>
          <Translate i18n="guest.reset-complete.mistake.before" ns="pages" />
          <Link to="/password/reset" data-current>
            <Translate i18n="guest.reset-complete.mistake.link" ns="pages" />
          </Link>
          <Translate i18n="guest.reset-complete.mistake.after" ns="pages" />
        </Box>
      </HighlightLayout>
    )
  }
}

const render = (props) => (
  <ComposedConsumer>
    {(renderProps) => {
      const {
        errors,
        environment,
        refetchViewerLocale,
        refreshEnvironment,
      } = renderProps

      return (
        <TokenPage
          {...props}
          {...errors}
          environment={environment}
          refreshEnvironment={refreshEnvironment}
          refetchViewerLocale={refetchViewerLocale}
        />
      )
    }}
  </ComposedConsumer>
)

export default render

import styled from "styled-components"
import { Link } from "react-router-dom"

import { Box } from "src/components"

export const TableContainer = styled.div`
  border: 1px solid ${({ theme }) => theme.colors.grey};
  border-radius: 5px;
`

export const TableHeaderContainer = styled.div`
  padding: 18px 40px;
  background-color: ${({ theme }) => theme.colors.blueBright};
`

export const TableBody = styled.div`
  border-top: 1px solid ${({ theme }) => theme.colors.grey};
  padding: 28px 40px;
`

export const DocLink = styled(Link)`
  font-size: 16px;
  color: ${({ theme }) => theme.colors.black};
  cursor: pointer;
  text-decoration: none;
  &:hover {
    text-decoration: underline;
  }
`

export const StyledDayBox = styled(Box)`
  margin-right: 60px;
  width: 80px;
  boxsizing: content-box;
`

export const StyledBox = styled(Box)`
  box-sizing: content-box;
  overflow: hidden;
  width: 50px;
`

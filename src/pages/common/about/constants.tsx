import React from "react"

import { Translate } from "src/components"
import { personalDataAgreementUrl, userAgreementUrl } from "src/constants"

export const karmaDocuments = [
  {
    withIcon: false,
    title: <Translate i18n="pages:about.karma_docs.documents.user_agreement" />,
    url: userAgreementUrl,
  },
  {
    withIcon: false,
    title: <Translate i18n="pages:about.karma_docs.documents.personal_data" />,
    url: personalDataAgreementUrl,
  },
  {
    withIcon: false,
    title: <Translate i18n="pages:about.karma_docs.documents.tariffs" />,
    url:
      "https://docs.google.com/document/d/1If8EztV-kjA727o5ECLK0QgLqJtFG4TqfNk9jDeWHI0/edit",
  },
  {
    withIcon: false,
    title: <Translate i18n="pages:about.karma_docs.documents.company_card" />,
    url:
      "https://docs.google.com/document/d/1SINjhztXh_QBm0w5U1-3Lp5Km8x_ACbZQyjy7DUmd3s/edit",
  },
  {
    withIcon: false,
    title: (
      <Translate i18n="pages:about.karma_docs.documents.nominal_account" />
    ),
    url:
      "https://docs.google.com/document/d/19Gyhh89ffgjWtCvxXDJXQFk5pI4TQR2ykSuEjnOxME0/edit?usp=sharing",
  },
  {
    withIcon: false,
    title: <Translate i18n="pages:about.karma_docs.documents.offer" />,
    url:
      "https://docs.google.com/document/d/1QMOFs6qCl0lLJT8hd5DnrkwVwCopnuhwnJtEUu7wC6s/edit",
  },
  {
    withIcon: false,
    title: <Translate i18n="pages:about.karma_docs.documents.offer_entrepreneur" />,
    url:
      "https://docs.google.com/document/d/1k0akIXnuKKIwk0oXl1S7ygXhK7p-2Ss96KyRKq5Y7LA/edit",
  },
  {
    withIcon: false,
    title: (
      <Translate i18n="pages:about.karma_docs.documents.offer_individual" />
    ),
    url:
      "https://docs.google.com/document/d/1eVzTgjxLY4G7tst8TdOKifvCMxQbLLfKjEG5j5pWE-w/edit",
  },
  {
    withIcon: false,
    title: <Translate i18n="pages:about.karma_docs.documents.offer_juristic" />,
    url:
      "https://docs.google.com/document/d/1ja3BBv297GZq1PUuQ0FnrYzOL1WEiHwN-uTZ8O5KGIc/edit",
  },
  {
    withIcon: false,
    title: (
      <Translate i18n="pages:about.karma_docs.documents.personal_data_policy" />
    ),
    url:
      "https://docs.google.com/document/d/1c2V-Zx89OPhc8OmAO-zrWS05MhmCduLcwLuAwV7gv2o/edit",
  },
  {
    withIcon: false,
    title: <Translate i18n="pages:about.karma_docs.documents.karma_rules" />,
    url: "https://drive.google.com/open?id=17NlVRi1PfjIy1eTcZLsdBwNZaQtcoQ-t",
  },
  {
    withIcon: false,
    title: (
      <Translate i18n="pages:about.karma_docs.documents.conflict_policy" />
    ),
    url:
      "https://docs.google.com/document/d/1y4ebM6IHRpljYmsZ0T6xKFynTEoDo7ByWC6JNxRPHqQ/edit",
  },
  {
    withIcon: false,
    title: (
      <Translate i18n="pages:about.karma_docs.documents.risk_declaration" />
    ),
    url:
      "https://docs.google.com/document/d/1a5HLS_7vB2tPsIyb9rPnY9y1FMPj6QQf7lPN9BCG5q0/edit",
  },
  {
    withIcon: false,
    title:
      "Информация об органах управления и контролирующих лицах Кармы Технолоджи",
    url:
      "https://docs.google.com/document/d/1Ig-BL8bZRBkHAJ_7NBfJbFhLepHkRoCpt0_TUXnuQck/edit",
  },
]

export const usefulDocuments = [
  {
    glyph: "question",
    title: <Translate i18n="pages:about.useful_docs.documents.investor_info" />,
    url: "https://my.karma.red/invest/info",
  },
  {
    glyph: "star",
    title: <Translate i18n="pages:about.useful_docs.documents.scoring" />,
    url: "https://my.karma.red/invest/scoring",
  },
  {
    glyph: "medium",
    title: <Translate i18n="pages:about.useful_docs.documents.medium" />,
    url: "https://medium.com/karmatrust",
  },
  {
    glyph: "telegram",
    title: <Translate i18n="pages:about.useful_docs.documents.telegram" />,
    url: "https://t.me/karmainvest",
  },
  {
    glyph: "info",
    title: <Translate i18n="pages:about.useful_docs.documents.faq" />,
    url: "http://karmatrust.ru/",
  },
  {
    glyph: "document",
    title: "Архив документов",
    url:
      "https://drive.google.com/drive/folders/11QNevRF15hHIn26XCXcJdn9HOFb8EYkv?ths=true",
  },
]

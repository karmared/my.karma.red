import React from "react"

import {
  Box, Text, Document, Translate, PageContainer,
} from "src/components"

import { karmaDocuments, usefulDocuments } from "./constants"

const DocumentGroup = (props) => {
  const { title, documents } = props

  return (
    <Box mb="30px">
      <Box mb="20px">
        <Text fontSize="16px" lineHeight="1.5" fontWeight="bold">
          <Translate i18n={title} />
        </Text>
      </Box>
      {documents.map((doc, idx) => (
        <Box mb="20px" key={idx}>
          <Document doc={doc} withIcon={doc.withIcon} />
        </Box>
      ))}
    </Box>
  )
}

const About = () => (
  <PageContainer>
    <Box mb={35}>
      <Text fontSize="24px" lineHeight="1.5" fontWeight="bold">
        <Translate i18n="about.title" ns="pages" />
      </Text>
    </Box>
    <DocumentGroup
      documents={karmaDocuments}
      title="pages:about.karma_docs.title"
    />
    <DocumentGroup
      documents={usefulDocuments}
      title="pages:about.useful_docs.title"
    />
  </PageContainer>
)

export default About

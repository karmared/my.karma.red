import React from "react"

import { ScoringInfo, PageContainer } from "src/components"

export default () => (
  <PageContainer>
    <ScoringInfo />
  </PageContainer>
)

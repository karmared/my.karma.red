import React from "react"

import {
  Box,
  Translate,
  FloatingLabelInput,
} from "src/components"

import { setAuthToken, getIndividualInvestorProfiles } from "src/utils"

import i18n from "src/i18n"
import { useEnvironment } from "src/hooks"
import { MutationContainer } from "src/containers"
import { LoginWithEmailAndPassword } from "src/mutations"

import { useTokenInput } from "./utils"
import { useNavigation, steps } from "../navigation"
import {
  InputsContainer,
  HeaderContainer,
  ReturnLink,
  HeaderTitle,
  PrimaryButton,
} from "../styles"

import {
  createGTMNotifier,
  getEmailFromStorage,
  removeEmailFromStorage,
} from "../utils"

const TokenInput = (props) => {
  const placeholder = React.useMemo(
    () => i18n.t("components:join.investor.email_confirmation.placeholder"),
    [i18n.language],
  )

  return (
    <InputsContainer>
      <FloatingLabelInput
        autoFocus
        required
        withoutBorder
        name="token"
        type="password"
        disabled={props.busy}
        errorColor="mainRed"
        value={props.token.value}
        label={placeholder}
        onChange={props.token.onChange}
        error={
          props.getError("loginWithEmailAndPassword.password")
          || props.getError("loginWithEmailAndPassword.email")
        }
        hasError={
          props.getError("loginWithEmailAndPassword.password")
          || props.getError("loginWithEmailAndPassword.email")
        }
      />
    </InputsContainer>
  )
}

const EmailConfirmation = (props) => {
  const { commit, viewer } = props

  const emailFromStorage = getEmailFromStorage()
  const { environment, refreshEnvironment } = useEnvironment()
  const navigate = useNavigation(props.history)
  const [busy, setBusy] = React.useState(false)
  const token = useTokenInput(props.clearError)

  const notifyGTM = React.useMemo(() => createGTMNotifier({ eventAction: "code-approved", eventLabel: "step-2" }), [])

  const onCompleted = (data) => {
    notifyGTM()
    setAuthToken(data.loginWithEmailAndPassword.token)
    removeEmailFromStorage()
    refreshEnvironment()
    navigate(steps.emailConfirmation.next)
  }

  const onError = () => {
    setBusy(false)
  }

  const onSubmit = React.useCallback(
    (e) => {
      e.preventDefault()

      const callbacks = { onCompleted, onError }
      const variables = {
        input: {
          email: emailFromStorage || viewer.email,
          password: token.value,
        },
      }

      setBusy(true)
      commit({ environment, variables, callbacks })
    },
    [token.value],
  )

  return (
    <>
      <ReturnLink to={steps.emailConfirmation.previous}>{"< Назад"}</ReturnLink>

      <HeaderContainer>
        <HeaderTitle>
          <Translate i18n="components:join.investor.email_confirmation.header" />
          &nbsp;
          {viewer ? viewer.email : emailFromStorage}
        </HeaderTitle>
      </HeaderContainer>

      <form onSubmit={onSubmit}>
        <Translate
          render={() => (
            <TokenInput busy={busy} token={token} getError={props.getError} />
          )}
        />

        <Box mt="5px" display="flex" justifyContent="center">
          <PrimaryButton disabled={busy}>
            <Translate i18n="components:join.investor.email_confirmation.button" />
          </PrimaryButton>
        </Box>
      </form>
    </>
  )
}

export default (props) => {
  const email = getEmailFromStorage()
  const navigate = useNavigation(props.history)

  if (!props.viewer && !email) {
    navigate(steps.emailConfirmation.missingData)
    return null
  }

  const hasInvestorProfile = props.viewer && getIndividualInvestorProfiles(props.viewer).length
  if (hasInvestorProfile) {
    navigate(steps.emailConfirmation.exceedData)
    return null
  }

  return (
    <MutationContainer mutation={LoginWithEmailAndPassword}>
      {({ commit, getError, clearError }) => (
        <EmailConfirmation
          {...props}
          commit={commit}
          getError={getError}
          clearError={clearError}
        />
      )}
    </MutationContainer>
  )
}

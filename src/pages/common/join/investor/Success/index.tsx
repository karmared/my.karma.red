import React from "react"

import { Box, Flex, Translate } from "src/components"
import { getIndividualInvestorProfiles } from "src/utils"

import {
  Social,
  AppStore,
  Subheader,
  GooglePlay,
  Background,
  AppContainer,
} from "./styles"
import { steps, useNavigation } from "../navigation"
import { HeaderContainer, HeaderTitle, PrimaryButton } from "../styles"

const Success = () => {
  const navigateToAccounts = React.useCallback(() => {
    window.location.href = steps.success.next
  }, [])

  return (
    <>
      <Box pt="16px">
        <HeaderContainer>
          <HeaderTitle>
            <Translate i18n="components:join.investor.success.header.0" />
            <br />
            <Translate i18n="components:join.investor.success.header.1" />
          </HeaderTitle>
        </HeaderContainer>
      </Box>

      <Box mt={-24}>
        <Subheader>
          <Translate i18n="components:join.investor.success.help" />
        </Subheader>
      </Box>

      <Box m="32px 0" display="flex" justifyContent="center">
        <PrimaryButton p={0} onClick={navigateToAccounts}>
          <Translate i18n="components:join.investor.success.button" />
        </PrimaryButton>
      </Box>

      <Background>
        <Social icon="tg" href="https://t.me/karmainvest">
          <Translate i18n="components:join.investor.success.socials.0" />
        </Social>
        <Social icon="email" href="mailto:help@karma.red">
          <Translate i18n="components:join.investor.success.socials.1" />
        </Social>
        <Social icon="web" href="https://my.karma.red">
          <Translate i18n="components:join.investor.success.socials.2" />
        </Social>
      </Background>
      <Box my="40px">
        <Subheader>
          <Translate i18n="components:join.investor.success.apps" />
        </Subheader>
      </Box>
      <Flex width="150px" mx="auto" my="40px" justifyContent="space-between">
        <AppContainer href="https://itunes.apple.com/us/app/karma-investments/id1451475987">
          <AppStore />
        </AppContainer>
        <AppContainer href="https://play.google.com/store/apps/details?id=ru.karma.android">
          <GooglePlay />
        </AppContainer>
      </Flex>
    </>
  )
}

export default (props) => {
  const navigate = useNavigation(props.history)

  const hasInvestorProfile = props.viewer && getIndividualInvestorProfiles(props.viewer).length
  if (!hasInvestorProfile) {
    navigate(steps.success.missingData)
    return null
  }

  return <Success {...props} />
}

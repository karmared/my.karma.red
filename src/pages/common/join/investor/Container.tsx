import React from "react"
import Relay, { graphql } from "react-relay"

import { getIndividualInvestorProfiles } from "src/utils"
import { SignupHeader, QueryRenderer } from "src/components"

import { StyledBox, GlobalStyles } from "./styles"

const query = graphql`
  query ContainerInfoQuery {
    viewer {
      ...Container_viewer
    }
  }
`

const Container = ({ children, ...props }: any) => {
  const initialProfileData = props.viewer
    ? getIndividualInvestorProfiles(props.viewer)[0]
    : null
  const [profileData, setProfileData] = React.useState(initialProfileData)

  const showHeader = props.route && props.route.header !== false

  return (
    <>
      <GlobalStyles />
      <StyledBox>
        {showHeader && <SignupHeader />}
        {children({ ...props, profileData, setProfileData })}
      </StyledBox>
    </>
  )
}

const RefetchContainer = Relay.createRefetchContainer(
  (props) => <Container {...props} />,
  {
    viewer: graphql`
      fragment Container_viewer on User {
        id
        email
        country
        profiles {
          __typename
          id
          name
          investor {
            canBeInvestor
          }
          ... on IndividualProfile {
            phone
          }
          ... on LegalEntityProfile {
            phone
          }
          accreditation(role: INVESTOR) {
            status
          }
        }
        foreignProfiles {
          __typename
          id
          firstName
          lastName
          phone
          investor {
            canBeInvestor
          }
          accreditation(role: INVESTOR) {
            status
          }
        }
      }
    `,
  },
  query,
)

const render = (props) => (
  <QueryRenderer
    {...props}
    query={query}
    render={(rendererProps) => <RefetchContainer {...rendererProps} />}
  />
)

export default React.memo(render)

import React from "react"
import { Redirect } from "react-router"

import { Box, Translate } from "src/components"

import { StyledBox } from "../Welcome/styles"
import { steps, useNavigation } from "../navigation"
import {
  HeaderContainer,
  HeaderTitle,
  PrimaryButton,
  StyledLink,
} from "../styles"

const Start = (props) => {
  React.useEffect(() => {
    const queryParams = new URLSearchParams(window.location.search)
    queryParams.set("deposit", "false")

    props.history.push({ search: `?${queryParams.toString()}` })
  }, [])

  const navigate = useNavigation(props.history)
  const onNextClick = React.useCallback(() => navigate(steps.start.next), [])

  if (props.viewer) {
    return <Redirect to={steps.start.exceedData} />
  }

  return (
    <>
      <Box pt="16px">
        <HeaderContainer>
          <HeaderTitle>
            <Translate i18n="components:join.investor.start.header.0" />
            <br />
            <Translate i18n="components:join.investor.start.header.1" />
          </HeaderTitle>
        </HeaderContainer>
      </Box>

      <Box mt="5px" display="flex" justifyContent="center">
        <PrimaryButton onClick={onNextClick}>
          <Translate i18n="components:join.investor.start.button" />
        </PrimaryButton>
      </Box>

      <StyledBox mt="14px" m="auto">
        <StyledLink to="/login">
          <Translate i18n="components:join.investor.start.has_account" />
        </StyledLink>
      </StyledBox>
    </>
  )
}

export default Start

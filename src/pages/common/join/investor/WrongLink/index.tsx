import React from "react"

import { Box, SignupStepTitle } from "src/components"

import { HeaderContainer } from "../styles"

const WrongLink = () => (
  <Box>
    <HeaderContainer is={SignupStepTitle}>
      Кажется, вы используете неверную ссылку :( Попробуйте вернуться на
      страницу заявки и нажать кнопку "Инвестировать" снова.
    </HeaderContainer>
  </Box>
)

export default WrongLink

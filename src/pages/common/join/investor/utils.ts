import React from "react"
import { fetchQuery, graphql } from "relay-runtime"

import { notifyGTM } from "src/utils"
import { useEnvironment } from "src/hooks"

const emailKey = "karma-user-email"
export const getEmailFromStorage = () => window.localStorage.getItem(emailKey)
export const setEmailToStorage = (value) => window.localStorage.setItem(emailKey, value)
export const removeEmailFromStorage = () => window.localStorage.removeItem(emailKey)

export const createGTMNotifier = (data) => notifyGTM({
  ...data,
  event: "landing-registry",
})

const orderQuery = graphql`
  query utilsOrderQuery($id: ID!) {
    node(id: $id) {
      id
      ... on Order {
        chain {
          id
        }
        application {
          shortTitle
        }
      }
    }
  }
`

export const useOrder = () => {
  const [order, setOrder] = React.useState(null)
  const [busy, setBusy] = React.useState(true)
  const { environment } = useEnvironment()

  const urlParams = new URLSearchParams(window.location.search)
  const orderId = urlParams.get("order")

  React.useEffect(() => {
    if (!orderId) {
      setBusy(false)
      return
    }

    fetchQuery(environment, orderQuery, { id: orderId })
      .then(({ node }) => {
        setOrder(node)
        setBusy(false)
      })
      .catch(() => {
        setBusy(false)
      })
  }, [])

  return [order, busy]
}

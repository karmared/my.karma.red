import React from "react"

import {
  Box,
  Translate,
  FloatingLabelInput,
} from "src/components"

import { ConfirmPhone, RequestPhoneConfirmation } from "src/mutations"

import i18n from "src/i18n"
import { useEnvironment } from "src/hooks"
import { getIndividualInvestorProfiles } from "src/utils"
import { MutationContainer } from "src/containers"

import { createGTMNotifier } from "../utils"
import { ResendLabel, ResendLink, ResendContainer } from "./styles"
import { useNavigation, steps } from "../navigation"
import { getMutationAndVariables, useCountDown, useTokenInput } from "./utils"
import {
  HeaderContainer,
  InputsContainer,
  HeaderTitle,
  ReturnLink,
  PrimaryButton,
} from "../styles"

const TokenInput = (props) => {
  const placeholder = React.useMemo(
    () => i18n.t("components:join.investor.phone_confirmation.placeholder"),
    [i18n.language],
  )

  return (
    <InputsContainer>
      <FloatingLabelInput
        required
        autoFocus
        name="token"
        type="password"
        disabled={props.busy}
        value={props.token.value}
        label={placeholder}
        onChange={props.token.onChange}
        error={props.getError("confirmPhone.token")}
        hasError={props.getError("confirmPhone.token")}
        withoutBorder
        errorColor="mainRed"
      />
    </InputsContainer>
  )
}

const PhoneConfirmation = (props) => {
  const { commit } = props

  const { environment } = useEnvironment()
  const navigate = useNavigation(props.history)
  const [busy, setBusy] = React.useState(false)
  const token = useTokenInput(props.clearError)
  const [secondsLeft, startCounter] = useCountDown()

  const requestPhoneConfirmation = () => {
    if (secondsLeft) {
      return
    }

    const variables = {
      input: {
        phone: props.profileData.phone,
      },
    };

    (startCounter as any)()
    RequestPhoneConfirmation.commit(environment, variables, null, {})
  }

  React.useEffect(() => {
    (startCounter as any)()
  }, [])

  const notifyGTM = React.useMemo(() => createGTMNotifier({
    eventAction: "phone-approved",
    eventLabel: "step-5",
  }), [])

  const onCompleted = (data) => {
    notifyGTM()
    const { profile } = data.addIndividualProfile || data.createForeignIndividualProfile
    props.setProfileData({ ...profile })

    setBusy(false)
    navigate(steps.phoneConfirmation.next)
  }

  const onError = () => {
    setBusy(false)
  }

  const onTokenCompleted = () => {
    const [mutation, variables] = getMutationAndVariables(
      props.viewer,
      props.profileData,
    );

    (mutation as any).commit(environment, variables, null, {
      onCompleted,
      onError,
    })
  }

  const onSubmit = React.useCallback(
    (e) => {
      e.preventDefault()

      const callbacks = { onCompleted: onTokenCompleted, onError }
      const variables = {
        input: {
          token: token.value,
          phone: props.profileData.phone,
        },
      }

      setBusy(true)
      commit({ environment, variables, callbacks })
    },
    [token.value],
  )

  return (
    <>
      <ReturnLink to={steps.phoneConfirmation.previous}>{"< Назад"}</ReturnLink>

      <HeaderContainer>
        <HeaderTitle>
          <Translate i18n="components:join.investor.phone_confirmation.header.0" />
          &nbsp;⭐ <br />
          <Translate i18n="components:join.investor.phone_confirmation.header.1" />
        </HeaderTitle>
      </HeaderContainer>

      <form onSubmit={onSubmit}>
        <Translate
          render={() => (
            <TokenInput busy={busy} token={token} getError={props.getError} />
          )}
        />

        <ResendContainer>
          <ResendLabel>
            <Translate i18n="components:join.investor.phone_confirmation.resend.0" />
          </ResendLabel>

          <ResendLink onClick={requestPhoneConfirmation}>
            <Translate i18n="components:join.investor.phone_confirmation.resend.1" />
            {!!secondsLeft && (
              <>
                &nbsp;
                <Translate i18n="components:join.investor.phone_confirmation.resend.2" />
                &nbsp;
                {secondsLeft}
              </>
            )}
          </ResendLink>
        </ResendContainer>

        <Box mt="30px" display="flex" justifyContent="center">
          <PrimaryButton disabled={busy}>
            <Translate i18n="components:join.investor.phone_confirmation.button" />
          </PrimaryButton>
        </Box>
      </form>
    </>
  )
}

export default (props) => {
  const navigate = useNavigation(props.history)

  if (!props.profileData || !props.profileData.phone) {
    navigate(steps.phoneConfirmation.missingData)
    return null
  }

  const hasInvestorProfile = props.viewer && getIndividualInvestorProfiles(props.viewer).length
  if (hasInvestorProfile) {
    navigate(steps.phoneConfirmation.exceedData)
    return null
  }

  return (
    <MutationContainer mutation={ConfirmPhone}>
      {({ commit, getError, clearError }) => (
        <PhoneConfirmation
          {...props}
          commit={commit}
          getError={getError}
          clearError={clearError}
        />
      )}
    </MutationContainer>
  )
}

import styled from "styled-components"

export const ResendContainer = styled.div`
  position: relative;
  display: flex;
  justify-content: space-between;
  align-items: center;
  width: 250px;
  margin: 0 auto;
  flex-wrap: wrap;

  @media screen and (max-width: 480px) {
    width: 100%;
  }
`

export const ResendLabel = styled.span`
  display: block;
  font-size: 12px;
  font-family: Geometria, sans-serif;
  color: ${({ theme }) => theme.colors.primaryBlacks[8]};
`

export const ResendLink = styled.a`
  font-size: 12px;
  font-family: Geometria, sans-serif;
  color: ${({ theme }) => theme.colors.primaryBlacks[8]};
  text-decoration: underline;
  cursor: pointer;
  user-select: none;

  &:active,
  &:visited {
    color: ${({ theme }) => theme.colors.primaryBlacks[8]};
  }
`

import React from "react"

export const useDecorators = () => {
  const onFocus = React.useCallback((e) => {
    const value = e.target.value.trim()

    if (value[0] !== "+") {
      e.target.value = `+${e.target.value.trim()}`
    }
  }, [])

  const onBlur = React.useCallback((e) => {
    const value = e.target.value.trim()

    if (value === "+") {
      e.target.value = ""
    }

    if (value && value[0] !== "+") {
      e.target.value = `+${value}`
    }
  }, [])

  return [onFocus, onBlur]
}

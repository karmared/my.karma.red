import React from "react"

import {
  Box,

  Translate,

  FloatingLabelInput,
} from "src/components"

import { useInput, useEnvironment } from "src/hooks"
import { MutationContainer } from "src/containers"
import { RequestPhoneConfirmation } from "src/mutations"
import {
  getIndividualInvestorProfiles,
  createChainedFunction,
  normalizePhone,
} from "src/utils"
import { RUSSIAN_PHONE_MASK } from "src/constants"

import { useNavigation, steps } from "../navigation"
import {
  HeaderContainer,
  InputsContainer,

  PrimaryButton,
  HeaderTitle,
  ReturnLink,
} from "../styles"

const Phone = (props) => {
  const { commit } = props

  const { environment } = useEnvironment()
  const navigate = useNavigation(props.history)
  const phone = useInput("+7")

  const [busy, setBusy] = React.useState(false)
  const [mask, setMask] = React.useState(RUSSIAN_PHONE_MASK)

  phone.onChange = React.useCallback(
    createChainedFunction(
      () => props.clearError("requestPhoneConfirmation.phone"),
      phone.onChange,
    ),
    [],
  )

  React.useEffect(() => {
    const digits = normalizePhone(phone.value)
    const newMask = digits?.[0] === "7" ? RUSSIAN_PHONE_MASK : null

    if (newMask !== mask) {
      setMask(newMask)
      phone.setValue(`+${digits}`)
    }
  }, [phone.value, mask])

  const onCompleted = () => {
    props.setProfileData({ ...props.profileData, phone: phone.value })

    navigate(steps.phone.next)
  }

  const onError = () => setBusy(false)

  const onSubmit = React.useCallback(
    (e) => {
      e.preventDefault()

      const variables = {
        input: {
          phone: phone.value,
        },
      }
      const callbacks = {
        onCompleted,
        onError,
      }

      setBusy(true)
      commit({ environment, variables, callbacks })
    },
    [phone.value],
  )

  let error = props.getError("requestPhoneConfirmation.phone")
  error = error && <Translate i18n={`join_${error.props.i18n}`} ns="errors" />

  return (
    <>
      <ReturnLink to={steps.phone.previous}>{"< Назад"}</ReturnLink>

      <HeaderContainer>
        <HeaderTitle>
          <Translate i18n="components:join.investor.phone.header" />
        </HeaderTitle>
      </HeaderContainer>

      <form onSubmit={onSubmit}>
        <InputsContainer>
          <FloatingLabelInput
            autoFocus
            required
            withoutBorder
            name="phone"
            type="text"
            value={phone.value}
            mask={mask}
            label="Номер телефона"
            disabled={busy}
            onChange={phone.onChange}
            error={error}
            hasError={error}
          />
        </InputsContainer>

        <Box mt="5px" display="flex" justifyContent="center">
          <PrimaryButton disabled={busy}>
            <Translate i18n="components:join.investor.phone.button" />
          </PrimaryButton>
        </Box>
      </form>
    </>
  )
}

export default (props) => {
  const navigate = useNavigation(props.history)

  if (!props.profileData || !props.profileData.firstName) {
    navigate(steps.phone.missingData)
    return null
  }

  const hasInvestorProfile = props.viewer && getIndividualInvestorProfiles(props.viewer).length
  if (hasInvestorProfile) {
    navigate(steps.phone.exceedData)
    return null
  }

  return (
    <MutationContainer mutation={RequestPhoneConfirmation}>
      {({ commit, getError, clearError }) => (
        <Phone
          {...props}
          commit={commit}
          getError={getError}
          clearError={clearError}
        />
      )}
    </MutationContainer>
  )
}

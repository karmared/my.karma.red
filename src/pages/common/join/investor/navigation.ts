import React from "react"

export const steps = {
  start: {
    next: "/join/investor/step-1",
    exceedData: "/market",
  },
  welcome: {
    next: "/join/investor/step-2",
    exceedData: "/join/investor/success",
  },
  emailConfirmation: {
    next: "/join/investor/step-3",
    previous: "/join/investor/step-1",
    missingData: "/join/investor/step-1",
    exceedData: "/join/investor/success",
  },
  name: {
    next: "/join/investor/step-4",
    previous: "/join/investor/step-2",
    missingData: "/join/investor/step-2",
    exceedData: "/join/investor/success",
  },
  phone: {
    next: "/join/investor/step-5",
    previous: "/join/investor/step-3",
    missingData: "/join/investor/step-3",
    exceedData: "/join/investor/success",
  },
  phoneConfirmation: {
    next: "/join/investor/success",
    previous: "/join/investor/step-4",
    missingData: "/join/investor/step-4",
    exceedData: "/join/investor/success",
  },
  requisites: {
    next: "/join/investor/success",
    nextStandalone: "/join/investor/success-existing",
    previous: "/join/investor/step-5",
    missingData: "/join/investor/step-5",
  },
  success: {
    next: "/market",
    previous: "/join/investor/step-5",
    missingData: "/join/investor/step-5",
  },
  successExisting: {
    next: "/market",
    missingData: "/join/investor/deposit-existing",
  },
  redirector: {
    next: "/join/investor/step-1",
    exceedData: "/join/investor/deposit-existing",
  },
}

export const useNavigation = (history) => React.useCallback(
  (pathname) => {
    history.push(pathname + window.location.search)
  },
  [window.location.search],
)

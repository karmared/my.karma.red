import React from "react"

import {
  Box,
  Translate,
  FloatingLabelInput,
  Switch,
} from "src/components"

import i18n from "src/i18n"
import { useInput } from "src/hooks"
import { getIndividualInvestorProfiles, isRussianUser } from "src/utils"

import { createGTMNotifier } from "../utils"
import { useNavigation, steps } from "../navigation"
import {
  HeaderContainer,
  InputsContainer,
  ReturnLink,
  HeaderTitle,
  PrimaryButton,
  NameForm,
  SwitchContainer,
  SwitchLabel,
} from "../styles"

const Inputs = (props) => {
  const [firstNamePH, lastNamePH, patronymicPH] = React.useMemo(
    () => [
      i18n.t("components:join.investor.name.first_name"),
      i18n.t("components:join.investor.name.last_name"),
      i18n.t("components:join.investor.name.patronymic"),
    ],
    [i18n.language],
  )

  return (
    <>
      <InputsContainer>
        <FloatingLabelInput
          autoFocus
          required
          withoutBorder
          name="lastName"
          type="text"
          value={props.lastName.value}
          label={lastNamePH}
          onChange={props.lastName.onChange}
        />

        <FloatingLabelInput
          required
          withoutBorder
          name="firstName"
          type="text"
          value={props.firstName.value}
          label={firstNamePH}
          onChange={props.firstName.onChange}
        />

        <FloatingLabelInput
          withoutBorder
          name="patronymic"
          type="text"
          value={props.patronymic.value}
          label={patronymicPH}
          onChange={props.patronymic.onChange}
        />
      </InputsContainer>

      {props.isRussian && (
        <SwitchContainer>
          <SwitchLabel>Тип профиля:</SwitchLabel>

          <Switch
            leftLabel="ФЛ"
            rightLabel="ИП"
            variant="rocker"
            monochrome
            value={props.isEntrepreneur.value}
            onChange={props.isEntrepreneur.onChange}
          />
        </SwitchContainer>
      )}
    </>
  )
}

const Name = (props) => {
  const { viewer } = props
  const navigate = useNavigation(props.history)

  const firstName = useInput("")
  const lastName = useInput("")
  const patronymic = useInput("")
  const isEntrepreneur = useInput(false)

  const notifyGTM = React.useMemo(
    () => createGTMNotifier({ eventAction: "next", eventLabel: "step-3" }),
    [],
  )

  const onSubmit = React.useCallback(
    (e) => {
      e.preventDefault()

      notifyGTM()

      props.setProfileData({
        ...props.profileData,
        firstName: firstName.value.trim(),
        lastName: lastName.value.trim(),
        middleName: patronymic.value.trim() || null,
        isEntrepreneur: isRussianUser(viewer) && isEntrepreneur.value,
      })

      navigate(steps.name.next)
    },
    [firstName.value, lastName.value, patronymic.value, isEntrepreneur.value],
  )

  return (
    <>
      <ReturnLink to={steps.name.previous}>{"< Назад"}</ReturnLink>

      <HeaderContainer>
        <HeaderTitle>
          <Translate i18n="components:join.investor.name.header" />
        </HeaderTitle>
      </HeaderContainer>

      <NameForm onSubmit={onSubmit}>
        <Translate
          render={() => (
            <Inputs
              firstName={firstName}
              lastName={lastName}
              patronymic={patronymic}
              isEntrepreneur={isEntrepreneur}
              isRussian={isRussianUser(viewer)}
            />
          )}
        />

        <Box mt="5px" display="flex" justifyContent="center">
          <PrimaryButton>
            <Translate i18n="components:join.investor.name.button" />
          </PrimaryButton>
        </Box>
      </NameForm>
    </>
  )
}

export default (props) => {
  const navigate = useNavigation(props.history)

  if (!props.viewer) {
    navigate(steps.name.missingData)
    return null
  }

  if (getIndividualInvestorProfiles(props.viewer).length) {
    navigate(steps.name.exceedData)
    return null
  }

  return <Name {...props} />
}

import React from "react"
import styled, { createGlobalStyle } from "styled-components"
import { space } from "styled-system"
import { NavLink } from "react-router-dom"

import {
  Box, Button,
} from "src/components"
import { useNavigation } from "./navigation"

export const StyledBox = styled(Box)`
  position: relative;
  width: 100%;
  max-width: 1200px;
  margin: 0 auto;

  font-family: "Montserrat", Arial;

  @media (max-width: 1199px) {
    max-width: 960px;
  }

  @media (max-width: 959px) {
    max-width: 640px;
  }

  @media (max-width: 639px) {
    max-width: 480px;
  }

  @media (max-width: 479px) {
    max-width: 100%;
  }
`

export const ContentContainer = styled(Box)`
  padding: 0 10px;

  @media (max-width: 639px) {
    padding: 0 90px;
  }

  @media (max-width: 479px) {
    padding: 0 10px;
  }
`

export const GlobalStyles = createGlobalStyle`
  #app-root > div {
    min-width: inherit;
  }
`

export const HeaderContainer = styled(Box)`
  padding: 0;
  margin: 25px auto 45px auto !important;
  max-width: 800px;
  width: 100%;
  font-size: 25px !important;

  @media screen and (max-width: 480px) {
    margin-bottom: 35px;
  }
`

export const HeaderTitle = styled.h5`
  font-family: Geometria, sans-serif;
  font-style: normal;
  font-weight: 600;
  font-size: 28px;
  line-height: 35px;
  text-align: center;
  margin: 0;
  color: ${({ theme }) => theme.colors.greyShuttle};
`

export const StyledLink = styled(NavLink)`
  font-family: Geometria, sans-serif;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 18px;
  text-align: center;
  text-decoration-line: underline;
  color: ${({ theme }) => theme.colors.primaryBlacks[8]};
`

export const PrimaryButton = styled.button<any>`
  position: relative;
  border: none;
  border-radius: 4px;
  background-color: ${({ theme }) => theme.colors.mainRed};
  height: 45px;
  box-sizing: border-box;
  max-width: 300px;
  padding: 0 80px;
  color: white;
  font-family: Geometria, sans-serif;
  font-size: 12px;
  font-weight: 600;
  text-transform: uppercase;
  min-width: 250px;
  ${space};

  @media screen and (max-width: 480px) {
    width: 100%;
    max-width: 100%;
    padding: 0;
    height: 60px;
    font-size: 14px;
  }
`

export const ReturnLink = styled(NavLink)`
  font-family: Geometria, sans-serif;
  font-style: normal;
  font-weight: 600;
  font-size: 16px;
  line-height: 20px;
  text-decoration: none;
  color: ${({ theme }) => theme.colors.primaryBlack};
`

const StyledButton = styled(Button)`
  position: absolute;
  left: 45px;
  top: 65px;
  font-family: "Open Sans", Arial, sans-serif;
  font-size: 14px;
  color: ${({ theme }) => theme.colors.greyShuttle};
  cursor: pointer;

  @media (max-width: 639px) {
    left: 15px;
  }
`

export const BackButton = ({ to, history }) => {
  const navigate = useNavigation(history)
  const onClick = React.useCallback(() => navigate(to), [to])

  return (
    <StyledButton variant="empty" onClick={onClick}>
      Назад
    </StyledButton>
  )
}

const StyledNextButton = styled(Button)`
  display: block;
  min-width: 230px;
  height: 50px;
  margin: auto;

  @media (max-width: 639px) {
    min-width: 340px;
  }

  @media (max-width: 479px) {
    min-width: 300px;
  }
`

export const NextButton = (props) => (
  <StyledNextButton variant="landingRed" {...props} />
)

export const NameForm = styled.form`
  max-width: 790px;
  width: 100%;
  margin: 0 auto;
  
  @media screen and (max-width: 639px) {
    max-width: 340px;
  }
`

export const SwitchContainer = styled(Box)`
  display: flex;
  align-items: center;
  margin-bottom: 32px;
  
  @media screen and (max-width: 639px) {
    justify-content: space-between;
  }
`

export const SwitchLabel = styled.p`
  font-size: 16px;
  font-family: Geometria, sans-serif;
  color: ${({ theme }) => theme.colors.primaryBlack};
  margin: 0 32px 0 0;
  
  @media screen and (max-width: 639px) {
    margin-right: 16px;
  }
`

export const InputsContainer = styled(Box)`
  display: flex;
  justify-content: center;

  @media (max-width: 639px) {
    flex-direction: column;
    align-items: center;
    justify-content: initial;
  }

  > * {
    width: 250px;
    margin-right: 20px;
    margin-bottom: 25px;

    &:last-child {
      margin-right: 0;
    }

    @media (max-width: 639px) {
      width: 340px;
      margin: 0;
      margin-bottom: 25px;
    }

    @media (max-width: 480px) {
      width: 100%;
    }
  }
`

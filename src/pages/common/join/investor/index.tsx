import React from "react"
import { renderRoutes } from "react-router-config"

import Name from "./Name"
import Phone from "./Phone"
import Start from "./Start"
import Welcome from "./Welcome"
import Success from "./Success"
import Container from "./Container"
import WrongLink from "./WrongLink"
import Redirector from "./Redirector"
import Requisites from "./Requisites"
import SuccessStandalone from "./SuccessStandalone"
import PhoneConfirmation from "./PhoneConfirmation"
import EmailConfirmation from "./EmailConfirmation"

const routes = [
  {
    path: "/join/investor/start",
    component: Start,
  },
  {
    path: "/join/investor/step-1",
    component: Welcome,
  },
  {
    path: "/join/investor/step-2",
    component: EmailConfirmation,
  },
  {
    path: "/join/investor/step-3",
    component: Name,
  },
  {
    path: "/join/investor/step-4",
    component: Phone,
  },
  {
    path: "/join/investor/step-5",
    component: PhoneConfirmation,
  },
  {
    path: "/join/investor/deposit",
    component: Requisites,
  },
  {
    path: "/join/investor/deposit-existing",
    component: Requisites,
    header: false,
  },
  {
    path: "/join/investor/success",
    component: Success,
  },
  {
    path: "/join/investor/success-existing",
    component: SuccessStandalone,
    header: false,
  },
  {
    path: "/join/investor/wrong",
    component: WrongLink,
  },
  {
    component: Redirector,
  },
]

const JoinInvestor = (props) => {
  React.useEffect(() => {
    const viewportMeta = document.querySelector("meta[name~=\"viewport\"]")

    if (!viewportMeta) return

    const initialViewportContent = viewportMeta.getAttribute("content")
    viewportMeta.setAttribute(
      "content",
      "width=device-width, initial-scale=1.0, maximum-scale=1.0",
    )

    // eslint-disable-next-line
    return () => {
      if (viewportMeta) {
        viewportMeta.setAttribute("content", initialViewportContent)
      }
    }
  }, [])

  const route = React.useMemo(
    () => routes.find((item) => item.path === props.location.pathname),
    [props.location.pathname],
  )

  return (
    <Container viewer={props.viewer} history={props.history} route={route}>
      {(containerProps) => renderRoutes(routes, { ...props, ...containerProps })
      }
    </Container>
  )
}

export default JoinInvestor

import React from "react"

import i18n from "src/i18n"
import { pipe } from "src/utils"

import {
  Box,
  FloatingLabelInput,
  CheckBoxField,
  Translate,
} from "src/components"

import { userAgreementUrl, personalDataAgreementUrl } from "src/constants"
import { ArrowIcon } from "../icons"

import { InputsContainer } from "../styles"
import {
  CheckBoxesContainer,
  StyledLink,
  StyledSelect,
  SelectContainer,
  ArrowContainer,
} from "./styles"
import { decorateEmailUniquenessError, useCountries } from "./utils"

export const CheckBoxes = ({ attributes }) => (
  <CheckBoxesContainer>
    <Box width="520px" mb="20px">
      <CheckBoxField
        fill="mainRed"
        fontSize="12px"
        color="greyDarker"
        required
        checked={attributes.personalDataAgreement.value}
        onChange={attributes.personalDataAgreement.onChange}
        label={
          <>
            <Translate i18n="components:join.investor.welcome.personal_data_agreement.0" />
            &nbsp;
            <StyledLink href={personalDataAgreementUrl} target="_blank">
              <Translate i18n="components:join.investor.welcome.personal_data_agreement.1" />
            </StyledLink>
          </>
        }
      />
    </Box>

    <Box width="520px" mb="20px">
      <CheckBoxField
        fill="mainRed"
        fontSize="12px"
        color="greyDarker"
        required
        checked={attributes.termsOfUse.value}
        onChange={attributes.termsOfUse.onChange}
        label={
          <>
            <Translate i18n="components:join.investor.welcome.user_agreement.0" />
            &nbsp;
            <StyledLink href={userAgreementUrl} target="_blank">
              <Translate i18n="components:join.investor.welcome.user_agreement.1" />
            </StyledLink>
          </>
        }
      />
    </Box>

    <Box width="520px" mb="20px">
      <CheckBoxField
        fill="mainRed"
        fontSize="12px"
        color="greyDarker"
        checked={attributes.shouldSubscribeToMailingList.value}
        onChange={attributes.shouldSubscribeToMailingList.onChange}
        label={<Translate i18n="components:join.investor.welcome.mailing" />}
      />
    </Box>
  </CheckBoxesContainer>
)

export const Inputs = (props) => {
  const { busy, getError, attributes } = props

  const getEmailError = pipe(
    () => getError("registerUser.email"),
    decorateEmailUniquenessError,
  )

  const countries = useCountries(i18n.language)
  const emailPlaceholder = React.useMemo(
    () => i18n.t("components:join.investor.welcome.email"),
    [i18n.language],
  )

  return (
    <InputsContainer>
      <FloatingLabelInput
        autoFocus
        required
        withoutBorder
        name="email"
        type="email"
        disabled={busy}
        errorColor="mainRed"
        value={attributes.email.value}
        label={emailPlaceholder}
        onChange={attributes.email.onChange}
        error={getEmailError()}
        hasError={getEmailError()}
      />

      <SelectContainer>
        <StyledSelect
          required
          disabled={busy}
          options={countries}
          value={attributes.country.value}
          onChange={attributes.country.onChange}
        />
        <ArrowContainer>
          <ArrowIcon />
        </ArrowContainer>
      </SelectContainer>
    </InputsContainer>
  )
}

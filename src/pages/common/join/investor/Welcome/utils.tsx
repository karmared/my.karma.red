import React from "react"
import { fetchQuery, graphql } from "relay-runtime"

import { registrationSources } from "src/constants"
import { useEnvironment, useInput } from "src/hooks"
import {
  createChainedFunction,
  getCid,
  getUtmContent,
  getQueryParam,
} from "src/utils"

import { ErrorLink } from "./styles"

const welcomePageQuery = graphql`
  query utilsWelcomePageQuery($locale: LocaleEnum!) {
    countries(locale: $locale) {
      code
      name
    }
  }
`

export const useCountries = (locale) => {
  const { environment } = useEnvironment()
  const [countries, setCountries] = React.useState([])

  React.useEffect(() => {
    fetchQuery(environment, welcomePageQuery, {
      locale: locale.toUpperCase(),
    }).then((data: any) => {
      setCountries(
        data.countries.map((item) => ({ value: item.code, label: item.name })),
      )
    })
  }, [locale])

  return countries
}

const isEmailUniquenessError = (error) => error && error.props.i18n === "registerUser.email.unique"

export const decorateEmailUniquenessError = (error) => {
  if (!error) return null

  return isEmailUniquenessError(error) ? (
    <>
      {error}. &nbsp;
      <ErrorLink to="/login">Войти</ErrorLink>
    </>
  ) : (
    error
  )
}

export const useInputs = (viewer, clearError) => {
  const clearFieldError = (path) => () => clearError(path)

  const termsOfUse = useInput(false)
  const personalDataAgreement = useInput(false)
  const shouldSubscribeToMailingList = useInput(false)

  const email = useInput(viewer ? viewer.email : "")
  email.onChange = React.useCallback(
    createChainedFunction(
      clearFieldError("registerUser.email"),
      email.onChange,
    ),
    [],
  )

  const country = useInput(viewer ? viewer.country : "RU")
  country.onChange = React.useCallback(
    createChainedFunction(
      clearFieldError("registerUser.country"),
      country.onChange,
    ),
    [],
  )

  return {
    email,
    country,
    termsOfUse,
    personalDataAgreement,
    shouldSubscribeToMailingList,
  }
}

export const getVariablesFromAttributes = (attributes, order) => {
  const input: any = {
    email: attributes.email.value,
    country: attributes.country.value,
    shouldSubscribeToMailingList: attributes.shouldSubscribeToMailingList.value,
    analytics: {
      cid: getCid(),
      utms: getUtmContent(),
      userAgent: window.navigator ? window.navigator.userAgent : null,
    },
  }

  const utmPool = getQueryParam("utm_pool")

  if (utmPool) {
    input.pools = [utmPool]
  }

  if (order) {
    input.source = `${registrationSources.LANDING},${order.id}`
  } else {
    input.source = registrationSources.WEB
  }

  return { input }
}

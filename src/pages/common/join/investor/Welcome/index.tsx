import React from "react"

import { Box, Flex, Translate } from "src/components"

import { useEnvironment } from "src/hooks"
import { RegisterUser } from "src/mutations"
import { MutationContainer } from "src/containers"
import { removeAuthToken, getIndividualInvestorProfiles } from "src/utils"

import { useInputs, getVariablesFromAttributes } from "./utils"

import { StyledBox } from "./styles"
import { CheckBoxes, Inputs } from "./elements"
import { steps, useNavigation } from "../navigation"
import {
  HeaderContainer,
  HeaderTitle,
  StyledLink,
  PrimaryButton,
  ReturnLink,
} from "../styles"
import { createGTMNotifier, useOrder, setEmailToStorage } from "../utils"

const Welcome = (props) => {
  const { viewer, commit } = props
  const { environment, refreshEnvironment } = useEnvironment()
  const navigate = useNavigation(props.history)
  const [order] = useOrder()

  const [busy, setBusy] = React.useState(false)
  const attributes = useInputs(viewer, props.clearError)

  const notifyGTM = React.useMemo(
    () => createGTMNotifier({ eventAction: "next", eventLabel: "step-1" }),
    [],
  )

  const onCompleted = () => {
    setBusy(false)
    removeAuthToken()
    refreshEnvironment()
    setEmailToStorage(attributes.email.value)
    navigate(steps.welcome.next)
  }

  const onError = () => setBusy(false)

  const onSubmit = React.useCallback(
    (e) => {
      e.preventDefault()

      notifyGTM()

      if (viewer && attributes.email.value === viewer.email) {
        setEmailToStorage(attributes.email.value)
        navigate(steps.welcome.next)
        return
      }

      const callbacks = { onCompleted, onError }
      const variables = getVariablesFromAttributes(attributes, order)

      setBusy(true)
      commit({ environment, variables, callbacks })
    },
    [attributes.email.value, viewer, attributes.country.value],
  )

  const nextDisabled = busy
    || !attributes.personalDataAgreement.value
    || !attributes.termsOfUse.value

  return (
    <>
      <ReturnLink to="/login">{"< Назад"}</ReturnLink>

      <HeaderContainer>
        <HeaderTitle>
          <Translate i18n="components:join.investor.welcome.header" />
        </HeaderTitle>
      </HeaderContainer>

      <form onSubmit={onSubmit}>
        <Flex flexDirection="column" alignItems="center">
          <Translate
            render={() => (
              <Inputs
                busy={busy}
                attributes={attributes}
                getError={props.getError}
              />
            )}
          />
          <Box is={CheckBoxes} attributes={attributes} />
        </Flex>

        <Box mt="60px" display="flex" justifyContent="center">
          <PrimaryButton disabled={nextDisabled}>
            <Translate i18n="components:join.investor.welcome.button" />
          </PrimaryButton>
        </Box>

        <StyledBox mt="14px" m="auto">
          <StyledLink to="/login">
            <Translate i18n="components:join.investor.welcome.has_account" />
          </StyledLink>
        </StyledBox>
      </form>
    </>
  )
}

export default (props) => {
  const navigate = useNavigation(props.history)

  if (props.viewer) {
    const approvedInvestorProfiles = getIndividualInvestorProfiles(
      props.viewer,
    ).filter((item) => item.approvedAsInvestor === true)

    if (approvedInvestorProfiles.length) {
      navigate(steps.welcome.exceedData)
      return null
    }
  }

  return (
    <MutationContainer mutation={RegisterUser}>
      {({ commit, getError, clearError }) => (
        <Welcome
          {...props}
          commit={commit}
          getError={getError}
          clearError={clearError}
        />
      )}
    </MutationContainer>
  )
}

import styled from "styled-components"

import { Text } from "src/components"

export const Subheader = styled(Text)`
  font-size: 20px;
  line-height: 1.55;
  margin: auto;
  margin-bottom: 15px;
  text-align: center;
  max-width: 560px;
`

import React from "react"

import {
  Box, Flex, Text, Logo, Translate, SignupStepTitle,
} from "src/components"
import { getIndividualInvestorProfiles } from "src/utils"

import { Subheader } from "./styles"
import { steps, useNavigation } from "../navigation"
import { HeaderContainer, NextButton } from "../styles"

const Success = () => {
  const navigateToMarket = React.useCallback(() => {
    // props.history.push(steps.successExisting.next)
    window.location.href = steps.successExisting.next
  }, [])

  return (
    <>
      <Flex mb="100px" mt="40px" justifyContent="center">
        <Logo size="34px" />
      </Flex>
      <HeaderContainer is={SignupStepTitle} mb="0 !important">
        <Translate i18n="components:join.investor.success_existing.header" />
      </HeaderContainer>
      <Box
        width="320px"
        mb="80px"
        is={Subheader}
        fontSize="16px"
        fontWeight="600"
      >
        <Translate i18n="components:join.investor.success_existing.subheader" />
      </Box>
      <Text
        fontSize="14px"
        color="greyShuttle"
        textAlign="center"
        fontWeight="600"
      >
        <Translate i18n="components:join.investor.success_existing.info" />
      </Text>
      <Box
        m="15px auto 30px auto"
        is={NextButton}
        px="20px"
        onClick={navigateToMarket}
      >
        <Translate i18n="components:join.investor.success_existing.button" />
      </Box>
    </>
  )
}

export default (props) => {
  const navigate = useNavigation(props.history)

  const hasInvestorProfile = props.viewer && getIndividualInvestorProfiles(props.viewer).length
  if (!hasInvestorProfile) {
    navigate(steps.successExisting.missingData)
    return null
  }

  return <Success {...props} />
}

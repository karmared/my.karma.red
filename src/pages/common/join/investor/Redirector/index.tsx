import React from "react"
import { Redirect } from "react-router"

import { getIndividualInvestorProfiles, isApprovedProfile } from "src/utils"

import { steps } from "../navigation"

export default (props) => {
  const investorProfiles = props.viewer ? getIndividualInvestorProfiles(props.viewer) : []
  const hasApprovedProfile = investorProfiles.some(isApprovedProfile)

  if (hasApprovedProfile) {
    const urlParams = new URLSearchParams(window.location.search)
    const orderId = urlParams.get("order")
    const path = orderId ? `/market/${orderId}?action=invest` : "/market"

    return <Redirect to={path} />
  }

  if (investorProfiles.length) {
    return (
      <Redirect
        to={`${steps.redirector.exceedData}${window.location.search}`}
      />
    )
  }

  return <Redirect to={`${steps.redirector.next}${window.location.search}`} />
}

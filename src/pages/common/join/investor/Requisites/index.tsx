import React from "react"
import QRCode from "qrcode.react"
import { fetchQuery, graphql } from "relay-runtime"

import i18n from "src/i18n"
import { useEnvironment } from "src/hooks"
import { getIndividualInvestorProfiles } from "src/utils"
import {
  Box, Text, Loader, Translate, SignupStepTitle,
} from "src/components"

import { HeaderContainer, NextButton } from "../styles"
import {
  InfoText,
  Subheader,
  DangerText,
  Instructions,
  InfoContainer,
  InstructionsLink,
  BankRequisitesItem,
  RequisitesContainer,
  BackgroundContainer,
} from "./styles"

import { useOrder } from "../utils"
import { useNavigation, steps } from "../navigation"
import { buildQRCodeString, mailRequisites } from "./utils"

const query = graphql`
  query RequisitesQuery($profileId: ID!, $hasOrder: Boolean!) {
    localizedStrings {
      karmaPaymentRequisites {
        name
        value
        key
      }
      virtualDepositPurpose(profileId: $profileId) @skip(if: $hasOrder) {
        name
        value
        key
      }
    }
  }
`

const Requisites = (props) => {
  const { environment } = useEnvironment()
  const [order, orderBusy] = useOrder()
  const navigate = useNavigation(props.history)
  const [requisites, setRequisites] = React.useState(null)
  const [requisitesString, setRequisitesString] = React.useState(null)

  React.useEffect(() => {
    if (orderBusy) {
      return
    }

    const investor = getIndividualInvestorProfiles(props.viewer)[0]
    mailRequisites(investor, order, environment)
  }, [order, orderBusy])

  React.useEffect(() => {
    if (orderBusy) {
      return
    }

    const investor = getIndividualInvestorProfiles(props.viewer)[0]

    const variables = {
      hasOrder: false,
      profileId: investor.id,
    }

    if (order) {
      variables.hasOrder = true
    }

    fetchQuery(environment, query, variables).then((data: any) => {
      setRequisites(data.localizedStrings)

      const reqStr = buildQRCodeString([
        ...data.localizedStrings.karmaPaymentRequisites,
        ...data.localizedStrings.virtualDepositPurpose,
      ])

      setRequisitesString(reqStr)
    })
  }, [order, orderBusy, i18n.language])

  const isStandalone = props.route.header === false

  const onClick = React.useCallback(() => {
    const path = isStandalone
      ? steps.requisites.nextStandalone
      : steps.requisites.next
    navigate(path)
  }, [isStandalone])

  if (!requisites) {
    return (
      <Box mt="200px">
        <Loader fontSize="14px" />
      </Box>
    )
  }

  return (
    <>
      <HeaderContainer is={SignupStepTitle}>
        {isStandalone ? (
          <Text fontSize="36px" is="span">
            <Translate i18n="components:join.investor.deposit.investment" />
          </Text>
        ) : (
          <>
            <Translate i18n="components:join.investor.deposit.header.0" /> ❤️
          </>
        )}
        <br />
        <br />
        {order ? (
          <>
            <Translate i18n="components:join.investor.deposit.header.1" />{" "}
            {order.application.shortTitle}
          </>
        ) : (
          <Translate i18n="components:join.investor.deposit.header.2" />
        )}
      </HeaderContainer>
      <RequisitesContainer>
        <BackgroundContainer>
          <Box mt="-20px" mb="40px" is={Subheader}>
            <Translate i18n="components:join.investor.deposit.manual" />
          </Box>
          {requisites.karmaPaymentRequisites.map(({ name, value }, idx) => (
            <BankRequisitesItem key={idx}>
              {name}: {value}
            </BankRequisitesItem>
          ))}
          {requisites.virtualDepositPurpose.map(({ name, value }, idx) => (
            <BankRequisitesItem key={idx}>
              {name}: {value}
            </BankRequisitesItem>
          ))}
          <DangerText>
            <Translate i18n="components:join.investor.deposit.min_amount.0" />
            <br />
            <Translate i18n="components:join.investor.deposit.min_amount.1" />
          </DangerText>
        </BackgroundContainer>
        <BackgroundContainer>
          <Box mt="-20px" mb="40px" is={Subheader}>
            <Translate i18n="components:join.investor.deposit.qr" />
          </Box>
          <Box width="fit-content" m="auto" mb="10px">
            {requisitesString && (
              <QRCode value={requisitesString} level="H" size={300} />
            )}
          </Box>
          <Text textAlign="center">
            <InstructionsLink href="#instructions">
              <Translate i18n="components:join.investor.deposit.instructions" />
            </InstructionsLink>
          </Text>
        </BackgroundContainer>
      </RequisitesContainer>

      <Box m="20px auto 30px auto" is={NextButton} onClick={onClick}>
        <Translate i18n="components:join.investor.deposit.button" />
      </Box>
      <InfoContainer>
        <InfoText>
          <Translate i18n="components:join.investor.deposit.info.0" />
        </InfoText>
        <br />
        <br />
        <InfoText>
          <Translate i18n="components:join.investor.deposit.info.1" />
        </InfoText>
        <InfoText>
          <Translate i18n="components:join.investor.deposit.info.2" />
        </InfoText>
        <InfoText>
          <Translate i18n="components:join.investor.deposit.info.3" />
        </InfoText>
        <InfoText>
          <Translate i18n="components:join.investor.deposit.info.4" />
        </InfoText>
      </InfoContainer>
      <Instructions />
    </>
  )
}

export default (props) => {
  const navigate = useNavigation(props.history)

  const hasInvestorProfile = props.viewer && getIndividualInvestorProfiles(props.viewer).length
  if (!hasInvestorProfile) {
    navigate(steps.requisites.missingData)
    return null
  }

  const queryParams = new URLSearchParams(window.location.search)
  const shouldSkipDeposit = queryParams.get("deposit") === "false"
  if (shouldSkipDeposit) {
    navigate(steps.requisites.next)
    return null
  }

  return <Translate render={() => <Requisites {...props} />} />
}

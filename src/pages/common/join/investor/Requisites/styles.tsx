import React from "react"
import styled from "styled-components"

import {
  Box, Flex, Text, Translate,
} from "src/components"

export const Subheader = styled(Text)`
  text-align: ${({ textAlign }) => textAlign || "center"};
  margin-bottom: 10px;
  font-size: 18px;
  line-height: 1.55;
  font-weight: 700;
  color: ${({ theme }) => theme.colors.greyDarkest};
`

export const Header = styled(Text)`
  text-align: center;
  font-size: 24px;
  line-height: 1.55;
  font-weight: 600;
  padding: 0 10px;
`

const Background = styled.div`
  width: 564px;
  max-width: 100%;
  padding: 50px;
  text-align: left;
  background-color: ${({ theme }) => theme.colors.greyLighter};

  @media (max-width: 479px) {
    padding: 50px 10px;
  }
`

export const BgContainer = styled.div`
  max-width: 100%;
`

export const BackgroundContainer = (props) => (
  <BgContainer>
    <Background {...props} />
  </BgContainer>
)

export const BankRequisitesItem = styled(Text)`
  color: ${({ theme }) => theme.colors.greyShuttle};
  font-size: 16px;
  line-height: 1.55;
  font-weight: 400;
`

export const DangerText = styled(Text)`
  margin-top: 15px;
  color: ${({ theme }) => theme.colors.red};
  font-size: 14px;
  line-height: 1.55;
  font-weight: bold;
`

export const InfoContainer = styled.div`
  width: 564px;
  max-width: 100%;
  margin: auto;
  margin-bottom: 40px;
`

export const InfoText = styled(Text)`
  padding-left: 10px;
  color: ${({ theme }) => theme.colors.greyShuttle};
  font-size: 14px;
  line-height: 1.8;
`

export const RequisitesContainer = styled(Flex)`
  justify-content: space-around;

  @media (max-width: 959px) {
    flex-direction: column;

    > * {
      margin: auto;
      margin-bottom: 30px;
    }
  }
`

const ListItem = styled.li`
  margin-bottom: 10px;
  font-size: 12px;

  &:before {
    counter-increment: list;
    content: counter(list);
    display: inline-block;
    padding: 3px;
    font-size: 12px;
    line-height: 1;
    vertical-align: middle;
    text-align: center;
    width: 20px;
    height: 20px;
    background-color: ${({ color }) => color || "#f6ce50"};
    border-radius: 50%;
    margin-right: 10px;
  }
`

const List = styled.ol`
  list-style: none;
  counter-reset: list;
  margin: 0;
  padding: 0;
`

export const InstructionContainer = styled(Flex)`
  @media (max-width: 479px) {
    flex-direction: column;

    > * {
      margin: auto !important;
      margin-bottom: 20px !important;
    }
  }
`

export const Instruction = ({
  src, title, items, color,
}: any) => (
  <InstructionContainer>
    <Box is="img" width="200px" height="410px" mr="40px" src={src} />
    <Box>
      <Box is={Subheader} mb="20px" textAlign="left">
        <Translate i18n={title} />
      </Box>
      <List>
        {items.map((item, idx) => (
          <ListItem key={idx} color={color}>
            <Translate i18n={item} />
          </ListItem>
        ))}
      </List>
    </Box>
  </InstructionContainer>
)

const InstructionsContainer = styled(Flex)`
  padding: 0 15px;
  margin-top: 50px;

  @media (max-width: 959px) {
    flex-direction: column;

    > * {
      margin: auto;
      margin-bottom: 40px;
    }
  }
`

export const Instructions = () => (
  <Box my="70px">
    <Header id="instructions">
      <Translate i18n="components:join.investor.deposit.instructions_title" />
    </Header>
    <InstructionsContainer>
      <Instruction
        src="/images/qr_tinkoff.png"
        title="components:join.investor.deposit.tinkoff.title"
        items={[
          "components:join.investor.deposit.tinkoff.items.0",
          "components:join.investor.deposit.tinkoff.items.1",
          "components:join.investor.deposit.tinkoff.items.2",
          "components:join.investor.deposit.tinkoff.items.3",
          "components:join.investor.deposit.tinkoff.items.4",
        ]}
      />
      <Instruction
        src="/images/qr_sber.png"
        title="components:join.investor.deposit.sberbank.title"
        color="#0ca94e"
        items={[
          "components:join.investor.deposit.sberbank.items.0",
          "components:join.investor.deposit.sberbank.items.1",
          "components:join.investor.deposit.sberbank.items.2",
          "components:join.investor.deposit.sberbank.items.3",
        ]}
      />
    </InstructionsContainer>
  </Box>
)

export const InstructionsLink = styled.a`
  text-decoration: none;
  color: ${({ theme }) => theme.colors.greyDarkest};
  border-bottom: 1px solid ${({ theme }) => theme.colors.greyDarkest};

  &:visited,
  &:active {
    color: ${({ theme }) => theme.colors.greyDarkest};
  }
`

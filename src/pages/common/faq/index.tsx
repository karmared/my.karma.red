import React from "react"

import { QueryParamsConsumer } from "src/context"

import {
  Return, Title, ReturnIcon, ItemsContainer, Item,
} from "./styles"
import contents from "./Contents"

function Faq(props) {
  function getReturnPath() {
    const { getQueryParam } = props
    const source = getQueryParam("source")

    if (source) return `/${source}`

    return "/market"
  }

  return (
    <>
      <Return to={getReturnPath()}>
        <ReturnIcon />
        Назад
      </Return>
      <Title>Часто задаваемые вопросы</Title>

      <ItemsContainer>
        {contents.map((item, idx) => (
          <Item key={idx} title={item.title}>
            {item.content()}
          </Item>
        ))}
      </ItemsContainer>
    </>
  )
}

function Render(props) {
  return (
    <QueryParamsConsumer>
      {({ getQueryParam }) => <Faq {...props} getQueryParam={getQueryParam} />}
    </QueryParamsConsumer>
  )
}

export default Render

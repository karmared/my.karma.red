import styled from "styled-components"

export const Image = styled.img`
  width: 100%;
  height: auto;
  display: block;
  box-sizing: content-box;
`

export const Text = styled.p`
  margin: 0;
  white-space: pre-line;
  font-family: Geometria, sans-serif;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 145%;
  color: ${({ theme }) => theme.colors.primaryBlack};
`

export const Bold = styled.span`
  font-family: Geometria, sans-serif;
  font-style: normal;
  font-weight: bold;
  font-size: 14px;
  line-height: 145%;
  color: ${({ theme }) => theme.colors.primaryBlack};
`

export const List = styled.ul`
  padding: 0 0 0 16px;
  margin: 0;
  font-family: Geometria, sans-serif;
  font-size: 14px;
  line-height: 145%;
  color: ${({ theme }) => theme.colors.primaryBlack};
`

export const Link = styled.a`
  text-decoration: none;
  font-family: Geometria, sans-serif;
  font-size: 14px;
  line-height: 145%;
  display: inline-block;
  color: ${({ theme }) => theme.colors.mainRed};

  :hover {
    text-decoration: underline;
  }
`

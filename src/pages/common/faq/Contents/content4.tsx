/* eslint-disable max-len */

import React from "react"

import { Text, Image } from "./styles"

export default () => (
  <>
    <Image src="images/faq/4.png" alt="table" />
    <Text>
      {
        "\nКаждый процентный пункт дохода имеет существенное значение на долгосрочном периоде, смотрите пример в таблице."
      }
    </Text>
  </>
)

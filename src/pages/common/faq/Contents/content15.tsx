/* eslint-disable max-len */

import React from "react"
import { userAgreementUrl } from "src/constants"

import { Text, Link } from "./styles"

export default () => (
  <Text>
    {
      "Карма (ООО \"Карма Технолоджи\") предоставляет IT услуги участникам краудлендинга: заемщикам и инвесторам. Оказание данной услуги осуществляется в соответствии с "
    }
    <Link
      href={userAgreementUrl}
      target="_blank"
    >
      Правилами платформы.
    </Link>
    {
      "\n\nПлатформа Карма внесена в реестр ЦБ 26 августа 2020 года и полностью соответствует требованиям закона 259-ФЗ.\n\nПо большинству сделок Карма (ООО \"Карма Лизинг\") выступает соинвестором."
    }
  </Text>
)

import Content1 from "./content1"
import Content2 from "./content2"
import Content3 from "./content3"
import Content4 from "./content4"
import Content5 from "./content5"
import Content6 from "./content6"
import Content7 from "./content7"
import Content8 from "./content8"
import Content9 from "./content9"
import Content10 from "./content10"
import Content11 from "./content11"
import Content12 from "./content12"
import Content13 from "./content13"
import Content14 from "./content14"
import Content15 from "./content15"
import Content16 from "./content16"
import Content17 from "./content17"
import Content18 from "./content18"
import Content19 from "./content19"
import Content20 from "./content20"
import Content21 from "./content21"

const contents = [
  {
    title: "1. Что такое Карма?",
    content: Content1,
  },
  {
    title: "2. Сколько можно заработать на платформе? Какие расходы?",
    content: Content2,
  },
  {
    title:
      "3. Зачем заемщики обращаются в Карму? Почему не берут кредит в банке по ставкам ниже?",
    content: Content3,
  },
  {
    title:
      "4. Доходность ОФЗ около 8%, а по корпоративным облигациям 15%, зачем рисковать ради дополнительных 5%-10%?",
    content: Content4,
  },
  {
    title: "5. Как осуществляется отбор заемщиков, как выставляется рейтинг?",
    content: Content5,
  },
  {
    title:
      "6. Скоринг в России не работает. Заемщик может просто не вернуть деньги и ему за это ничего не будет!",
    content: Content6,
  },
  {
    title: "7. А что, если с Кармой что-то случится или проект закроется?",
    content: Content7,
  },
  {
    title: "8. Может ли Карма украсть мои деньги?",
    content: Content8,
  },
  {
    title: "9. Что делать если заемщик не возвращает заём?",
    content: Content9,
  },
  {
    title: "10. Как стать инвестором?",
    content: Content10,
  },
  {
    title: "11. Каких заемщиков вы можете порекомендовать?",
    content: Content11,
  },
  {
    title: "12. Что такое номинальный счет и как он работает?",
    content: Content12,
  },
  {
    title:
      "13. Как оформляется сделка, что такое оферта и как она подписывается?",
    content: Content13,
  },
  {
    title: "14. Какие документы получает инвестор после сделки?",
    content: Content14,
  },
  {
    title:
      "15. Какое участие в сделках принимает Карма? Есть ли у компании лицензия ЦБ?",
    content: Content15,
  },
  {
    title: "16. Как и какие налоги должен платить инвестор?",
    content: Content16,
  },
  {
    title:
      "17. Что будет если первичная заявка не соберет минимальную сумму инвестиций?",
    content: Content17,
  },
  {
    title: "18. Можно ли досрочно вернуть свою инвестицию?",
    content: Content18,
  },
  {
    title: "19. Как рассчитывается доходность в цессии?",
    content: Content19,
  },
  {
    title: "20. Квалификация инвесторов с начала 2020 года",
    content: Content20,
  },
  {
    title: "21. Я не нашел/нашла ответа на интересующий меня вопрос",
    content: Content21,
  },
]

export default contents

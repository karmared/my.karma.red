/* eslint-disable max-len */

import React from "react"

import { Text } from "./styles"

export default () => (
  <Text>
    Напишите нам свой вопрос на почту help@karma.red Мы вернемся к вам с ответом
    в максимально короткие сроки.
  </Text>
)

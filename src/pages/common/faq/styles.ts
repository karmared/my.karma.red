import styled from "styled-components"
import { NavLink } from "react-router-dom"

import { Icons, Collapse } from "src/components"

export const Return = styled(NavLink)`
  text-decoration: none;
  font-family: Geometria, sans-serif;
  font-size: 12px;
  line-height: 24px;
  color: ${({ theme }) => theme.colors.primaryBlacks[8]};
  cursor: pointer;
  margin-top: -40px;

  @media screen and (max-width: 1280px) {
    margin-top: 0;
  }

  @media screen and (max-width: 767px) {
    margin-top: 90px;
  }

  @media screen and (max-width: 480px) {
    font-size: 16px;
    line-height: 20px;
    color: ${({ theme }) => theme.colors.primaryBlack};
    font-weight: 600;
    display: flex;
    align-items: flex-end;
    margin-left: -8px;
  }
`

export const Title = styled.h2`
  font-family: Geometria, sans-serif;
  font-style: normal;
  font-weight: 700;
  font-size: 36px;
  line-height: 43px;
  color: ${({ theme }) => theme.colors.darkCoffe};
  margin: 26px 0 0;

  @media screen and (max-width: 480px) {
    font-size: 30px;
    line-height: 36px;
    margin-top: 42px;
  }
`

export const ReturnIcon = styled(Icons.LeftArrow)`
  display: none;
  fill: ${({ theme }) => theme.colors.primaryBlack};

  @media screen and (max-width: 480px) {
    display: inline-block;
  }
`

export const ItemsContainer = styled.div`
  position: relative;
  margin: 40px 0;
`

export const Item = styled(Collapse)`
  :first-child {
    border-top: 1px solid ${({ theme }) => theme.colors.fullBlacks[1]};
  }
`

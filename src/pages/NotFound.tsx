import React from "react"
import styled from "styled-components"

import {
  Box, Flex, Text, Link, Logo, RedirectButton,
} from "src/components"

const Background = styled.div`
  background-image: url(/images/sheeps.jpg);
  background-position: center bottom;
  background-repeat: no-repeat;
  background-size: cover;
  position: fixed;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  z-index: -1;
`

const render = () => (
  <Flex
    alignItems="center"
    flexDirection="column"
    px={50}
    py={20}
    height="100%"
  >
    <Background />
    <Box alignSelf="flex-start">
      <Link to="/">
        <Flex color="white" alignItems="center">
          <Logo />
          <Text ml={10} color="currentColor" fontSize={3} fontWeight={500}>
            My.karma.red
          </Text>
        </Flex>
      </Link>
    </Box>
    <Text mt={80} fontSize={3}>
      Страница не существует или была удалена
    </Text>
    <Box mt={24}>
      <RedirectButton to="/">Вернуться на главную</RedirectButton>
    </Box>
  </Flex>
)

export default render

import React from "react"
import { Switch, Route, Redirect } from "react-router"

import { dynamic, catchError } from "src/utils"

const Login = dynamic(() => import("./login").catch(catchError))
const Orders = dynamic(() => import("./orders").catch(catchError))
const Password = dynamic(() => import("./password").catch(catchError))
const Registration = dynamic(() => import("./registration").catch(catchError))
const Join = dynamic(() => import("../common/join").catch(catchError))
const Token = dynamic(() => import("../common/token").catch(catchError))
const About = dynamic(() => import("../common/about").catch(catchError))
const Scoring = dynamic(() => import("../common/scoring").catch(catchError))
const InvestorInfo = dynamic(() => import("../common/info").catch(catchError))

function Render(props) {
  return (
    <Switch>
      <Route
        path="/"
        exact={true}
        render={(routerProp) => <Orders {...routerProp} {...props} />}
      />
      <Route
        path="/registration"
        render={(routerProp) => <Registration {...routerProp} {...props} />}
      />
      <Route
        path="/password"
        render={(routerProp) => <Password {...routerProp} {...props} />}
      />
      <Route
        path="/token/:token?"
        exact={true}
        render={(routerProp) => <Token {...routerProp} {...props} />}
      />
      <Route
        path="/about"
        exact={true}
        render={(routerProp) => <About {...routerProp} {...props} />}
      />
      <Route
        path="/login"
        exact={true}
        render={(routerProp) => <Login {...routerProp} {...props} />}
      />
      <Route
        path="/invest/info"
        render={(routerProp) => <InvestorInfo {...routerProp} {...props} />}
      />
      <Route
        path="/invest/scoring"
        render={(routerProp) => <Scoring {...routerProp} {...props} />}
      />
      <Route
        path="/join"
        render={(routerProp) => <Join {...routerProp} {...props} />}
      />
      <Redirect to="/" />
    </Switch>
  )
}

export default Render

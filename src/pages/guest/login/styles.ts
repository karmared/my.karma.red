import styled from "styled-components"
import { NavLink } from "react-router-dom"

import { KarmaLogo } from "./icons"

export const Container = styled.div`
  margin: 0 auto;
  max-width: 350px;
  width: 100%;
  height: auto;
`

export const LogoContainer = styled.div`
  display: flex;
  width: 100%;
  margin: 90px 0 70px;

  @media screen and (max-width: 800px) {
    margin-top: 60px;
  }
`

export const Logo = styled(KarmaLogo)`
  margin: 0 auto;
  width: 60px;
  height: 60px;
`

export const NavBar = styled.div`
  position: relative;
  display: flex;
  width: 100%;
  margin-bottom: 20px;

  @media screen and (max-width: 480px) {
    margin-bottom: 40px;
  }
`

export const NavItem = styled(NavLink)`
  text-decoration: none;
  font-family: Geometria, sans-serif;
  font-style: normal;
  font-weight: 300;
  font-size: 16px;
  line-height: 20px;
  color: ${({ theme }) => theme.colors.primaryBlack};
  padding: 0 0 4px;

  @media screen and (max-width: 800px) {
    font-size: 18px;
    line-height: 23px;
  }
`

export const ActiveNavItem = styled(NavItem)`
  font-weight: 600;
  border-bottom: 2px solid ${({ theme }) => theme.colors.mainRed};
  margin-right: 34px;
`

import React from "react"
import { Route } from "react-router-dom"
import { RelayEnvironmentConsumer } from "src/context"
import {
  pipe,
  notifyYM,
  notifyGTM,
  getRedirectPath,
  removeRedirectPath,
} from "src/utils"

import {
  LoginForm,
} from "src/components"

import {
  Container,
  LogoContainer,
  Logo,
  NavBar,
  NavItem,
  ActiveNavItem,
} from "./styles"

const handleLogin = ({ history, refreshEnvironment, redirectPath }) => () => {
  refreshEnvironment()
  history.push(redirectPath)
}

const handleLoginPipe = ({ history, refreshEnvironment, redirectPath }) => pipe(
  handleLogin({ history, refreshEnvironment, redirectPath }),
  notifyGTM({
    eventAction: "login",
    eventLabel: "success",
    event: "karmatrust",
    eventCategory: "karmatrust",
  }),

  notifyYM("login-success"),
)

// TODO: unused - вернуть позже или удалить окончательно
/* const onRegistrationClick = pipe(
  notifyGTM({ eventAction: "registration", eventLabel: "click" }),
  notifyYM("reg-click"),
)
 const renderTitle = (props) => (
  <Box m="auto">
    <Heading.h1>
      <Translate i18n="guest-login.title" ns="pages" />
    </Heading.h1>
  </Box>
)

const renderDescription = (props) => (
  <Text fontSize={3} whiteSpace="pre-line">
    <Translate i18n="guest-login.description" ns="pages" />
  </Text>
)

const RegistrationButton = (props) => (
  <RedirectButton onClick={onRegistrationClick} to="/join" white>
    <Translate i18n="registration" ns="common" />
  </RedirectButton>
) */

/* const render = (props) => (
  <GuestLayout button={RegistrationButton}>
    <Template layout="v_1_auto_1" renderTop={renderTitle}>
      <Template
        layout="h_6_4"
        renderLeft={renderDescription}
        renderRight={RenderForm}
        m="auto"
        width={1024}
      />
    </Template>
  </GuestLayout>
) */

/* eslint-disable react-hooks/rules-of-hooks */
const RenderForm = () => {
  const [redirectPath, setRedirectPath] = React.useState(null)

  React.useEffect(() => {
    setRedirectPath(getRedirectPath() || "/")
    removeRedirectPath()
  }, [])

  if (!redirectPath) {
    return null
  }

  return (
    <RelayEnvironmentConsumer>
      {({ refreshEnvironment }) => (
        <Route>
          {({ history }) => (
            <LoginForm
              onCompleted={handleLoginPipe({
                history,
                refreshEnvironment,
                redirectPath,
              })}
            />
          )}
        </Route>
      )}
    </RelayEnvironmentConsumer>
  )
}

function Render(props) {
  return (
    <Container>
      <LogoContainer>
        <Logo />
      </LogoContainer>

      <NavBar>
        <ActiveNavItem to="/login">Вход</ActiveNavItem>
        <NavItem to="/join/investor/step-1">Регистрация</NavItem>
      </NavBar>

      <RenderForm {...props} />
    </Container>
  )
}

export default Render

/* eslint-disable max-len */

import React from "react"

export const KarmaLogo = (props) => (
  <svg {...props} viewBox="0 0 60 60" xmlns="http://www.w3.org/2000/svg">
    <path
      d="M30 60C46.5685 60 60 46.5685 60 30C60 13.4315 46.5685 0 30 0C13.4315 0 0 13.4315 0 30C0 46.5685 13.4315 60 30 60Z"
      fill="#F70000"
    />
    <path
      d="M16.2499 15V45H26.1499V38.4915L28.7499 35.4407L34.4499 45H46.2499L35.8499 27.9153L46.2499 15H34.5499L26.2499 26.1864V15H16.2499Z"
      fill="white"
    />
  </svg>
)

KarmaLogo.defaultProps = {
  width: "60px",
  height: "60px",
  fill: "none",
}

import React from "react"
import { Route } from "react-router-dom"
import { GuestLayout } from "src/layouts"
import { pipe, notifyYM, notifyGTM } from "src/utils"

import {
  Box,
  Text,
  Forms,
  Heading,
  Template,
  Translate,
  RedirectButton,
  AsideFormContainer,
} from "src/components"

const handleComplete = ({ history }) => ({ email }) => {
  history.push(`/registration/complete#${email}`)
}

const completePipe = ({ history }) => pipe(
  handleComplete({ history }),
  notifyGTM({
    eventAction: "registration",
    eventLabel: "success",
    event: "karmatrust",
    eventCategory: "karmatrust",
  }),
  notifyYM("reg-success"),
)

const onLoginClick = pipe(notifyGTM({ eventLabel: "login", eventAction: "click" }), notifyYM("login-click"))

const LoginButton = () => (
  <RedirectButton to="/login" white onClick={onLoginClick}>
    <Translate i18n="login" ns="common" />
  </RedirectButton>
)

const renderTitle = () => (
  <Box m="auto">
    <Heading.h1>
      <Translate i18n="guest.signup.title" ns="pages" />
    </Heading.h1>
  </Box>
)

const renderDescription = () => (
  <React.Fragment>
    <Heading.h3>
      <Translate i18n="guest.signup.description-title" ns="pages" />
    </Heading.h3>
    <Text mt={20} fontSize={3} whiteSpace="pre-line">
      <Translate i18n="guest.signup.description-content" ns="pages" />
    </Text>
  </React.Fragment>
)

const renderForm = () => (
  <Route>
    {({ history }) => (
      <AsideFormContainer variant="dark">
        <Forms.registration onCompleted={completePipe({ history })} />
      </AsideFormContainer>
    )}
  </Route>
)

const render = () => (
  <GuestLayout button={LoginButton}>
    <Template layout="v_1_auto_1" renderTop={renderTitle}>
      <Template
        layout="h_6_4"
        renderLeft={renderDescription}
        renderRight={renderForm}
        m="auto"
        width={1024}
      />
    </Template>
  </GuestLayout>
)

export default render

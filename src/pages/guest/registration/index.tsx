import React from "react"
import { Switch, Route } from "react-router"

import NotFound from "src/pages/NotFound"
import Main from "./main"
import Complete from "./complete"

function Render(props) {
  return (
    <Switch>
      <Route
        path="/registration"
        exact={true}
        render={(routerProp) => <Main {...routerProp} {...props} />}
      />
      <Route
        path="/registration/complete"
        exact={true}
        render={(routerProp) => <Complete {...routerProp} {...props} />}
      />
      <Route component={NotFound} />
    </Switch>
  )
}

export default Render

import React from "react"
import { Link } from "react-router-dom"
import {
  Box, Text, Heading, FlexBox, Translate,
} from "src/components"
import { HighlightLayout } from "src/layouts"

const render = () => {
  const { location } = window
  const email = location.hash.split("#").pop()

  return (
    <HighlightLayout>
      <Box is="h1" m={0} mt={80}>
        <Translate i18n="guest.registration-complete.title" ns="pages" />
      </Box>

      <Box is="img" src="/images/airdrop.png" m={0} mt={80} />

      <Box m={0} mt={40} width={768}>
        <Heading.h4 textAlign="center">
          <Translate
            i18n="guest.registration-complete.email-sent"
            email={email}
            ns="pages"
          />
        </Heading.h4>
      </Box>

      <FlexBox m={0} mt={20} flex={1} width={768} justifyContent="center">
        <Text fontSize={20} textAlign="center" whiteSpace="pre-line">
          <Translate
            i18n="guest.registration-complete.description"
            ns="pages"
          />
        </Text>
      </FlexBox>

      <Box is="p" m={0} mt={40} mb={80}>
        <Translate
          i18n="guest.registration-complete.mistake.before"
          ns="pages"
        />
        <Link to="/registration" data-current>
          <Translate
            i18n="guest.registration-complete.mistake.link"
            ns="pages"
          />
        </Link>
        <Translate
          i18n="guest.registration-complete.mistake.after"
          ns="pages"
        />
      </Box>
    </HighlightLayout>
  )
}

export default render

import React from "react"
import { Switch, Route, Redirect } from "react-router-dom"

import Reset from "./reset"
import Complete from "./complete"

const render = (props) => (
  <Switch>
    <Route path="/password/reset/complete" exact>
      {(route) => <Complete {...props} {...route} />}
    </Route>

    <Route path="/password/reset" exact>
      {(route) => <Reset {...props} {...route} />}
    </Route>

    <Route>{() => <Redirect to="/login" />}</Route>
  </Switch>
)

export default render

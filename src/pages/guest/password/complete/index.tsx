import React from "react"
import { Link } from "react-router-dom"
import {
  Box, Text, FlexBox, Heading, Translate,
} from "src/components"

import { HighlightLayout } from "src/layouts"

const render = () => {
  const { location } = window
  const email = location.hash.split("#").pop()

  return (
    <HighlightLayout>
      <Box mt={80}>
        <Heading.h1>
          <Translate i18n="guest.reset-complete.title" ns="pages" />
        </Heading.h1>
      </Box>
      <Box is="img" src="/images/illustration_send.svg" mt={80} width={156} />
      <Box is="h4" mt={40} maxWidth={768} width="100%">
        <Text
          fontSize={30}
          textAlign="center"
          style={{ overflowWrap: "anywhere" }}
        >
          <Translate
            i18n="guest.reset-complete.email-sent"
            email={email}
            ns="pages"
          />
        </Text>
      </Box>
      <FlexBox mt={30} flex={1} maxWidth={768} width="100%" textAlign="center">
        <Text fontSize={20} whiteSpace="pre-line" textAlign="center">
          <Translate i18n="guest.reset-complete.description" ns="pages" />
        </Text>
      </FlexBox>
      <Box is="p" m={0} mt={40} mb={80}>
        <Translate i18n="guest.reset-complete.mistake.before" ns="pages" />
        <Link to="/password/reset" data-current>
          <Translate i18n="guest.reset-complete.mistake.link" ns="pages" />
        </Link>
        <Translate i18n="guest.reset-complete.mistake.after" ns="pages" />
      </Box>
    </HighlightLayout>
  )
}

export default render

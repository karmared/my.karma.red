import React from "react"
import { GuestLayout } from "src/layouts"

import { Route } from "react-router"

import {
  Box,
  Text,
  Forms,
  Heading,
  Translate,
  Template,
  RedirectButton,
  AsideFormContainer,
} from "src/components"

const onPasswordResetRequestCompleted = (history) => (email) => {
  history.push(`/password/reset/complete#${email}`)
}

/* eslint-disable arrow-body-style */
const renderForm = () => {
  return (
    <Route>
      {({ history }) => (
        <AsideFormContainer variant="dark">
          <Forms.password.reset
            onCompleted={onPasswordResetRequestCompleted(history)}
          />
        </AsideFormContainer>
      )}
    </Route>
  )
}

const renderTitle = () => {
  return (
    <Box m="auto">
      <Heading.h1>
        <Translate i18n="password.reset.title" ns="pages" />
      </Heading.h1>
    </Box>
  )
}

const renderDescription = () => {
  return (
    <React.Fragment>
      <Box pb={28}>
        <Heading.h4>
          <Translate i18n="password.reset.sub-title" ns="pages" />
        </Heading.h4>
      </Box>
      <Text fontSize={3} whiteSpace="pre-line">
        <Translate i18n="password.reset.description" ns="pages" />
      </Text>
    </React.Fragment>
  )
}

const EnterButton = () => {
  return (
    <RedirectButton to="/login" white>
      <Translate i18n="enter" ns="common" />
    </RedirectButton>
  )
}

export default () => {
  return (
    <GuestLayout button={EnterButton}>
      <Template layout="v_1_auto_1" renderTop={renderTitle}>
        <Template
          layout="h_6_4"
          renderLeft={renderDescription}
          renderRight={renderForm}
          m="auto"
          width={1024}
        />
      </Template>
    </GuestLayout>
  )
}

import React, { useMemo, useState } from "react"
import { graphql, QueryRenderer } from "react-relay"
import { Redirect, Route, Switch } from "react-router"

import { isRussianEntrepreneur } from "src/utils"
import { CreateOrder } from "src/mutations"
import { useEnvironment } from "src/hooks"
import {
  PageContainer,
  JuristicProfileData,
  EditTrustedProfile,
  TrustedProfileOrders,
  TrustedBankAccount,
  TrustedSelectAccountKind,
} from "src/components"

import {
  Menu,
  MenuItem,
  InfoBlock,
  Email,
  Name,
  ReturnLink,
  ArrowIcon,
  Status,
  CreateProfileWrapper,
  Button,
} from "./styles"
import { accreditationStatuses } from "./constants"

function TrustedAccount(props) {
  const {
    match, location, node: profile, history,
  } = props
  const { email } = profile.user
  const accreditationStatus = useMemo(
    () => accreditationStatuses[profile?.accreditation.status] || {
      text: "-",
      color: "rgba(74,74,74,0.7)",
    },
    [profile],
  )

  const { environment } = useEnvironment()
  const [showKindProfile, setShowKindProfile] = useState(false)

  const { id } = match.params
  const tab = location.pathname.split("/")[3]

  const withMenu = tab !== "edit"

  function handleClick(newTab) {
    history.push(`/trusted/${id}/${newTab}`)
  }

  function createOrder() {
    if (!profile?.id || !profile?.user?.id) return

    const { id: userId } = profile.user
    const { id: profileId } = profile

    const variables = {
      input: {
        userId,
        profileId,
      },
    }

    const callbacks = {
      onCompleted: ({ createOrder: res }) => {
        const { id: orderId } = res.order
        history.push(`/orders/${orderId}`)
      },
    }

    CreateOrder.commit(environment, variables, null, callbacks)
  }

  function handleCreateProfile(e) {
    history.push(`/trusted/create/${profile.user.id}/${e.target.name}`)
  }

  return (
    <PageContainer>
      <ReturnLink to="/trusted/list">
        <ArrowIcon />К списку профилей
      </ReturnLink>

      {withMenu && (
        <>
          <InfoBlock>
            <Name>{profile?.name || "-"}</Name>
            <Email>{email}</Email>
            <Status color={accreditationStatus.color}>
              {accreditationStatus.text}
            </Status>
          </InfoBlock>

          <CreateProfileWrapper>
            <Button onClick={() => setShowKindProfile(true)}>
              Добавить профиль
            </Button>
            <TrustedSelectAccountKind
              show={showKindProfile}
              onClose={() => setShowKindProfile(false)}
            >
              <TrustedSelectAccountKind.option
                name="legal"
                onClick={handleCreateProfile}
              >
                Юридическое лицо
              </TrustedSelectAccountKind.option>
              <TrustedSelectAccountKind.option
                name="entrepreneur"
                disabled={profile.user.profiles.find(isRussianEntrepreneur)}
                onClick={handleCreateProfile}
              >
                Индивидуальный предприниматель
              </TrustedSelectAccountKind.option>
            </TrustedSelectAccountKind>
          </CreateProfileWrapper>

          <Menu>
            <MenuItem
              onClick={() => handleClick("info")}
              isActive={tab === "info"}
            >
              Данные о заёмщике
            </MenuItem>
            <MenuItem
              onClick={() => handleClick("orders")}
              isActive={tab === "orders"}
            >
              Заявки на займ
            </MenuItem>
            <MenuItem
              onClick={() => handleClick("bank")}
              isActive={tab === "bank"}
              disabled={!profile || !profile.id}
            >
              Банковские реквизиты
            </MenuItem>
          </Menu>
        </>
      )}

      <Switch>
        <Route
          path="/trusted/:id/info"
          component={(routeProps) => (
            <JuristicProfileData
              createOrder={createOrder}
              profile={profile}
              {...routeProps}
            />
          )}
        />

        <Route
          path="/trusted/:id/orders"
          component={(routeProps) => (
            <TrustedProfileOrders
              createOrder={createOrder}
              profile={profile}
              {...routeProps}
            />
          )}
        />

        <Route
          path="/trusted/:id/edit"
          component={(routeProps) => (
            <EditTrustedProfile
              createOrder={createOrder}
              profile={profile}
              {...routeProps}
            />
          )}
        />

        <Route
          path="/trusted/:id/bank"
          component={(routeProps) => (
            <TrustedBankAccount profile={profile} {...routeProps} />
          )}
        />

        <Redirect to="/trusted/:id/info" />
      </Switch>
    </PageContainer>
  )
}

const query = graphql`
  query accountQuery($id: ID!) {
    node(id: $id) {
      ... on UserProfile {
        id
        __typename
        name
        user {
          id
          email
          profiles {
            __typename
          }
        }
        accreditation(role: BORROWER) {
          status
        }
        ...JuristicProfileData_profile
        ...EditTrustedProfile_profile
        ...TrustedProfileOrders_profile
        ...TrustedBankAccount_profile
      }
    }
  }
`

function Render(props) {
  const { match, history } = props
  const { id } = match.params

  const { environment } = useEnvironment()

  return (
    <QueryRenderer
      environment={environment}
      query={query}
      variables={{ id }}
      render={({ error, props: renderProps }) => {
        if (error) {
          history.replace("/market")
        }

        if (renderProps) {
          return <TrustedAccount {...props} {...renderProps} />
        }

        return <></>
      }}
    />
  )
}

export default Render

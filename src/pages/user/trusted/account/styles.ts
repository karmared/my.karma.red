import styled, { css } from "styled-components"
import { color } from "styled-system"
import { NavLink } from "react-router-dom"

import { Flex, Icons } from "src/components"

export const Menu = styled(Flex)`
  position: relative;
  margin: 40px 0 50px;
`

export const MenuItem = styled.button<any>`
  padding: 0 0 8px;
  box-sizing: border-box;
  border: none;
  font-family: Geometria, sans-serif;
  font-style: normal;
  font-weight: 500;
  font-size: 15px;
  line-height: 19px;
  color: ${({ theme }) => theme.colors.primaryBlacks[9]};

  :not(:last-child) {
    margin-right: 24px;
  }

  :disabled {
    opacity: 0.5;
    cursor: not-allowed;
  }

  ${({ isActive }) => isActive
    && css`
      border-bottom: 3px solid ${({ theme }) => theme.colors.mainRed};
      font-weight: 600;
    `};
`

export const InfoBlock = styled.div`
  position: relative;
  padding: 20px 32px;
  min-width: 320px;
  background-color: #f8f8f8;
  border-radius: 5px;
  display: inline-block;
`

export const Email = styled.p`
  font-family: Geometria, sans-serif;
  font-size: 18px;
  line-height: 22px;
  color: ${({ theme }) => theme.colors.primaryBlack};
  margin: 0;
`

export const Name = styled(Email)`
  font-size: 12px;
  line-height: 14px;
  margin-bottom: 6px;
  color: ${({ theme }) => theme.colors.primaryBlacks[7]};
`

export const Status = styled(Name)`
  font-size: 14px;
  margin: 6px 0 0;
  ${color};
`

export const ReturnLink = styled(NavLink)`
  font-family: Geometria, sans-serif;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 17px;
  display: flex;
  align-items: center;
  color: ${({ theme }) => theme.colors.primaryBlack};
  text-decoration: none;
  max-width: 170px;
  margin: -24px 0 50px;
`

export const ArrowIcon = styled(Icons.Arrow)`
  fill: ${({ theme }) => theme.colors.primaryBlack};
`

export const CreateProfileWrapper = styled.div`
  position: relative;
  display: inline-block;
  margin-left: 20px;
`

export const Button = styled.button`
  width: 180px;
  background-color: ${({ theme }) => theme.colors.mainRed};
  border-radius: 4px;
  font-family: Geometria, sans-serif;
  font-style: normal;
  font-weight: 600;
  font-size: 10px;
  line-height: 12px;
  text-align: center;
  text-transform: uppercase;
  color: white;
  border: none;
  padding: 15px 0 13px;
`

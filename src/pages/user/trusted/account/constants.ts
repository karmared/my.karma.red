export const accreditationStatuses = {
  INITIAL: {
    text: "Черновик",
    color: "#3A5BBC",
  },
  PENDING: {
    text: "На модерации",
    color: "#FF9900",
  },
  APPROVED: {
    text: "Аккредитован",
    color: "#6FA84B",
  },
  DECLINED: {
    text: "Отклонен",
    color: "#F70000",
  },
}

import React from "react"
import { Redirect, Route, Switch } from "react-router"
import { graphql } from "react-relay"

import { QueryRenderer } from "src/components"

import Account from "./account"
import Create from "./create"
import AccountList from "./accountList"

function Trusted(props) {
  const { viewer } = props

  return (
    <Switch>
      {viewer && !viewer.trust.isAvailable && <Redirect to="/market" />}
      <Route exact path="/trusted/list" component={AccountList} />
      <Route path="/trusted/create" component={Create} />
      <Route path="/trusted/:id" component={Account} />
      <Redirect to="/trusted/list" />
    </Switch>
  )
}

const query = graphql`
  query trustedQuery {
    viewer {
      trust {
        isAvailable
      }
    }
  }
`

function Render(props) {
  return <QueryRenderer {...props} query={query} render={Trusted} />
}

export default Render

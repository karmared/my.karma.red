import React, { useState } from "react"
import { graphql } from "relay-runtime"

import {
  PageContainer,
  QueryRenderer,
  TrustedAccountList,
  TrustedSelectAccountKind,
} from "src/components"

import {
  Container, Title, Button, Text, CreateProfileWrapper,
} from "./styles"

function AccountList(props) {
  const { viewer, history } = props
  const isEmpty = !viewer.trustedUsers.length

  const [showSelectKind, setShowSelectKind] = useState(false)

  function toCreate(e) {
    history.push(`/trusted/create/${e.target.name}`)
  }

  return (
    <PageContainer>
      <Container isEmpty={isEmpty}>
        <Title>Аккаунты</Title>
        {isEmpty && <Text>Аккаунтов ещё нет. Добавьте первый.</Text>}

        <CreateProfileWrapper>
          <Button onClick={() => setShowSelectKind(true)}>
            Добавить аккаунт
          </Button>
          <TrustedSelectAccountKind
            show={showSelectKind}
            onClose={() => setShowSelectKind(false)}
          >
            <TrustedSelectAccountKind.option name="legal" onClick={toCreate}>
              Юридическое лицо
            </TrustedSelectAccountKind.option>
            <TrustedSelectAccountKind.option
              name="entrepreneur"
              onClick={toCreate}
            >
              Индивидуальный предприниматель
            </TrustedSelectAccountKind.option>
          </TrustedSelectAccountKind>
        </CreateProfileWrapper>
      </Container>

      {!isEmpty && <TrustedAccountList accounts={viewer} />}
    </PageContainer>
  )
}

const query = graphql`
  query accountListQuery {
    viewer {
      ...TrustedAccountList_accounts
      trustedUsers {
        id
      }
    }
  }
`

function Render(props) {
  return (
    <QueryRenderer
      {...props}
      query={query}
      render={(props) => <AccountList {...props} />}
    />
  )
}

export default Render

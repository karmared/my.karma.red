import styled, { css } from "styled-components"

export const Container = styled.div<any>`
  position: relative;
  display: flex;
  justify-content: space-between;
  align-items: center;
  width: 100%;
  margin-bottom: 80px;

  ${({ isEmpty }) => isEmpty
    && css`
      flex-direction: column;
      align-items: flex-start;
    `};
`

export const Title = styled.h3`
  font-family: Geometria, sans-serif;
  font-style: normal;
  font-weight: normal;
  font-size: 24px;
  line-height: 29px;
  color: ${({ theme }) => theme.colors.primaryBlack};
  margin: 0;
`

export const CreateProfileWrapper = styled.div`
  position: relative;
`

export const Button = styled.button`
  width: 180px;
  background-color: ${({ theme }) => theme.colors.mainRed};
  border-radius: 4px;
  font-family: Geometria, sans-serif;
  font-style: normal;
  font-weight: 600;
  font-size: 10px;
  line-height: 12px;
  text-align: center;
  text-transform: uppercase;
  color: white;
  border: none;
  padding: 15px 0 13px;
`

export const Text = styled.p`
  font-family: Geometria, sans-serif;
  font-style: normal;
  font-weight: 700;
  font-size: 19px;
  line-height: 23px;
  color: ${({ theme }) => theme.colors.primaryBlack};
  margin: 50px 0 16px;
  max-width: 200px;
`

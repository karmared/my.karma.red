import React from "react"

import { PageContainer, Forms } from "src/components"

import { Title } from "../styles"

function CreateTrustedAccount(props) {
  const { user = null } = props
  return (
    <PageContainer>
      <Title>Данные заёмщика ИП</Title>
      <Forms.trusted.create.entrepreneur user={user}/>
    </PageContainer>
  )
}

export default CreateTrustedAccount

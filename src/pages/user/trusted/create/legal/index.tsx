import React from "react"

import { PageContainer, Forms } from "src/components"

import { Title } from "../styles"

function CreateTrustedAccount(props) {
  const { user = null } = props
  return (
    <PageContainer>
      <Title>Данные заёмщика ЮЛ</Title>
      <Forms.trusted.create.legal user={user}/>
    </PageContainer>
  )
}

export default CreateTrustedAccount

import React from "react"
import { graphql, QueryRenderer } from "react-relay"
import { Route, Switch } from "react-router"

import { useEnvironment } from "src/hooks"

import CreateLegal from "./legal"
import CreateEntrepreneur from "./entrepreneur"

const query = graphql`
  query createTrustedProfileForUserQuery($id: ID!) {
    user: node(id: $id) {
      ... on User {
        id
        email
      }
    }
  }
`

function CreateTrustedAccount({ history }) {
  const { environment } = useEnvironment()

  const render = (Render) => ({ error, props }) => {
    if (error) {
      history.replace("/market")
    }

    if (!props) {
      return <></>
    }
    return <Render {...props} />
  }

  return (
    <Switch>
      <Route
        exact
        path="/trusted/create/entrepreneur"
        component={CreateEntrepreneur}
      />
      <Route exact path="/trusted/create/legal" component={CreateLegal} />

      <Route
        path="/trusted/create/:id/entrepreneur"
        render={(routeProps: any) => (
          <QueryRenderer
            environment={environment}
            query={query}
            variables={{ id: routeProps.match.params.id }}
            render={render(CreateEntrepreneur)}
          />
        )}
      />
      <Route
        path="/trusted/create/:id/legal"
        render={(routeProps: any) => (
          <QueryRenderer
            environment={environment}
            query={query}
            variables={{ id: routeProps.match.params.id }}
            render={render(CreateLegal)}
          />
        )}
      />
    </Switch>
  )
}

export default CreateTrustedAccount

import React from "react"

import {
  Forms,
  Heading,
  Template,
  Translate,
  HistoryBack,
} from "src/components"
import { ProfileType } from "src/constants"

import HeadingContainer from "../individual/styles"

const Header = () => (
  <HeadingContainer>
    <Heading.h2 fontWeight="300" fontSize="32px" letterSpacing="-0.5px">
      <Translate ns="pages" i18n="profiles.add.individual.header" />
    </Heading.h2>
  </HeadingContainer>
)

const Content = () => (
  <React.Fragment>
    <Header />
    <Forms.individualProfile.accreditation.investor
      type={ProfileType.foreignIndividual}
    />
  </React.Fragment>
)

const render = () => (
  <Template
    layout="containerWIthControls"
    leftControl={HistoryBack}
    content={() => <Content />}
  />
)

export default render

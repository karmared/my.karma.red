import React from "react"

import {
  Text,
  Forms,
  Translate,
  ReturnButton,
  PageContainer,
  AccountHeader,
} from "src/components"

import { AccountHeaderQueryContainer } from "src/query"

const RenderAccountHeader = AccountHeaderQueryContainer((props) => (
  <AccountHeader {...props} />
))

const render = () => (
  <React.Fragment>
    <RenderAccountHeader>
      <ReturnButton path="/accounts">
        <Text fontSize={12}>
          <Translate i18n="account.header.buttons.return" ns="components" />
        </Text>
      </ReturnButton>
    </RenderAccountHeader>
    <PageContainer>
      <Forms.legalEntityProfile.manage profile={null} />
    </PageContainer>
  </React.Fragment>
)

export default render

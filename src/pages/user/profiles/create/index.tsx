import React from "react"
import { Route, Switch } from "react-router"

import Juristic from "./jurisitic"
import Individual from "./individual"
import Foreign from "./foreign"

function CreateProfiles(props) {
  return (
    <Switch>
      <Route
        path="/profiles/create/juristic"
        exact={true}
        render={(routerProps) => <Juristic {...routerProps} {...props} />}
      />
      <Route
        path="/profiles/create/individual"
        exact={true}
        render={(routerProps) => <Individual {...routerProps} {...props} />}
      />
      <Route
        path="/profiles/create/foreign"
        exact={true}
        render={(routerProps) => <Foreign {...routerProps} {...props} />}
      />
    </Switch>
  )
}

export default CreateProfiles

import React from "react"
import { graphql } from "react-relay"

import { QueryRenderer } from "src/components"

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const fragment = graphql`
  fragment UpdateProfile_viewer on User {
    profiles {
      __typename
      ...ManageIndividual_profile
      ...ManageLegalEntity_profile
    }
  }
`

const query = graphql`
  query UpdateProfilePageQuery {
    viewer {
      ...UpdateProfile_viewer @relay(mask: false)
    }
  }
`

export default (render) => (props) => (
  <QueryRenderer query={query} render={render} {...props} />
)

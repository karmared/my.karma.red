import React from "react"

import { Forms, PageContainer } from "src/components"

import { ProfileType } from "src/constants"
import { getNodeIdFromUri } from "src/utils"

import queryContainer from "./UpdateProfile"

const renderForm = (props) => {
  const id = getNodeIdFromUri(props.location.pathname)
  const profile = props.viewer.profiles.find(({ __id }) => __id === id)
  const { __typename: type } = profile

  if (type === ProfileType.individual) {
    return <Forms.individualProfile.manage profile={profile || null} />
  }

  if (type === ProfileType.juristic) {
    return <Forms.legalEntityProfile.manage profile={profile || null} />
  }

  return null
}

const RenderFormContainer = queryContainer(renderForm)

const render = (props) => (
  <React.Fragment>
    <PageContainer>
      <RenderFormContainer {...props} />
    </PageContainer>
  </React.Fragment>
)

export default render

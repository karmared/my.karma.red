import React from "react"

import { Route, Switch } from "react-router-dom"

import Borrower from "./borrower"
import Investor from "./investor"

const render = (props) => (
  <Switch>
    <Route
      path="/profiles/:id/accreditation/borrower"
      render={(renderProps) => <Borrower {...props} {...renderProps} />}
      exact
    />
    <Route
      path="/profiles/:id/accreditation/investor"
      render={(renderProps) => <Investor {...props} {...renderProps} />}
      exact
    />
  </Switch>
)

export default render

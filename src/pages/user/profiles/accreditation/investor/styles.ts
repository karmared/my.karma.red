import styled from "styled-components"

const HeadingContainer = styled.div`
  padding-bottom: 30px;
`

export default HeadingContainer

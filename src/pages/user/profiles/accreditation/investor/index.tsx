import React from "react"

import { AccreditationInfoQueryContainer } from "src/query"

import { ProfileType } from "src/constants"

import {
  Forms, Heading, Template, Translate, HistoryBack,
} from "src/components"

import HeadingContainer from "./styles"

const Header = () => (
  <HeadingContainer>
    <Heading.h2 fontWeight="300" fontSize="32px" letterSpacing="-0.5px">
      <Translate ns="pages" i18n="profiles.add.individual.header" />
    </Heading.h2>
  </HeadingContainer>
)

const Accreditation = AccreditationInfoQueryContainer(({ node }) => {
  if (!node) {
    return null
  }

  const { __typename: type } = node

  if (
    type === ProfileType.individual
    || type === ProfileType.foreignIndividual
  ) {
    return (
      <React.Fragment>
        <Header />
        <Forms.individualProfile.accreditation.investor profileId={node.id} />
      </React.Fragment>
    )
  }

  if (type === ProfileType.juristic) {
    return (
      <React.Fragment>
        <Header />
        <Forms.legalEntityProfile.accreditaion.investor profle={node || null} />
      </React.Fragment>
    )
  }

  return null
})

export default (props) => {
  const { id } = props.match.params

  return (
    <Template
      layout="containerWIthControls"
      leftControl={HistoryBack}
      content={() => <Accreditation profileId={id} />}
    />
  )
}

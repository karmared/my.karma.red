import React from "react"

import {
  AccountHeaderQueryContainer,
  AccreditationInfoQueryContainer,
} from "src/query"

import { ProfileType } from "src/constants"

import {
  Text,
  Forms,
  Translate,
  ReturnButton,
  AccountHeader,
  PageContainer,
} from "src/components"

const RenderAccountHeader = AccountHeaderQueryContainer((props) => (
  <AccountHeader {...props} />
))

const Accreditation = AccreditationInfoQueryContainer(({ node }) => {
  if (!node) return null

  const { __typename: type } = node

  if (type === ProfileType.individual) {
    return (
      <Forms.individualProfile.accreditation.borrower profile={node || null} />
    )
  }
  if (type === ProfileType.juristic) {
    return (
      <Forms.legalEntityProfile.accreditation.borrower profile={node || null} />
    )
  }

  return null
})

const Back = () => (
  <ReturnButton path="/accounts">
    <Text fontSize={12}>
      <Translate
        i18n="account.header.buttons.return_to_manage"
        ns="components"
      />
    </Text>
  </ReturnButton>
)

export default (props) => {
  const { id } = props.match.params

  return (
    <React.Fragment>
      <RenderAccountHeader>
        <Back />
      </RenderAccountHeader>
      <PageContainer>
        <Accreditation profileId={id} />
      </PageContainer>
    </React.Fragment>
  )
}

import React from "react"
import { Route, Switch } from "react-router"

import Create from "./create"

import Update from "./update"
import Accreditation from "./accreditation"

function Profiles(props) {
  return (
    <Switch>
      <Route
        path="/profiles/create"
        render={(routerProps) => <Create {...routerProps} {...props} />}
      />
      <Route
        path="/profiles/:profile/accreditation"
        render={(routerProps) => <Accreditation {...routerProps} {...props} />}
      />
      <Route
        path="/profiles/:profile/update"
        render={(routerProps) => <Update {...routerProps} {...props} />}
      />
    </Switch>
  )
}

export default Profiles

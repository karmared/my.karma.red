import React from "react"

import { PageContainer, Portfolio } from "src/components"

function OrdersPage(props) {
  return (
    <PageContainer>
      <Portfolio {...props} />
    </PageContainer>
  )
}

export default OrdersPage

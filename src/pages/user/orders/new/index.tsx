import React from "react"

import { ProfilesListQueryContainer } from "src/query"

import { PageContainer, NewOrderStepper } from "src/components"

const NewOrder = (props) => (
  <PageContainer>
    <NewOrderStepper {...props} />
  </PageContainer>
)

export default ProfilesListQueryContainer((props) => <NewOrder {...props} />)

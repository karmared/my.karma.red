import React from "react"
import { Route, Switch, Redirect } from "react-router"

import New from "./new"
import List from "./list"
import Manage from "./manage"
import Trusted from "./trusted"

function Orders(props) {
  return (
    <Switch>
      <Route
        path="/orders/my"
        render={(routerProps) => <List {...routerProps} {...props} />}
      />
      <Route
        path="/orders/new"
        render={(routerProps) => <New {...routerProps} {...props} />}
      />
      <Route
        path="/new-order"
        render={(routerProps) => <New {...routerProps} {...props} />}
      />
       <Route
        path="/orders/:id"
        exact={true}
        render={(routerProps) => <Manage {...routerProps} {...props} />}
       />
      <Route
        path="/orders/:id/trusted"
        exact={true}
        render={(routerProps) => <Trusted {...routerProps} {...props} />}
      />

      <Redirect to="/orders/my" />
    </Switch>
  )
}

export default Orders

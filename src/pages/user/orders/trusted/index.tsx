import React from "react"

import { OrderInfoQueryContainer } from "src/query"

import { SwitchBar, TrustedOrder, PageContainer } from "src/components"

import { getNodeIdFromUri } from "src/utils"
import { TrustedSwitchBarSchema } from "src/constants"

const OrderQuery = OrderInfoQueryContainer((props) => (
  <TrustedOrder {...props} />
))

class ManageTrustedOrder extends React.Component<any, any> {
  state = {
    activeTab: "application",
    orderId: null,
  }

  onChange = ({ activeTab }) => {
    this.setState(() => ({ activeTab }))
  }

  componentDidMount() {
    this.setState(() => ({
      orderId: getNodeIdFromUri(this.props.location.pathname),
    }))
  }

  render() {
    if (!this.state.orderId) return null

    return (
      <>
        <SwitchBar
          defaultTabIndex={0}
          onChange={this.onChange}
          schema={TrustedSwitchBarSchema}
        />
        <PageContainer>
          <OrderQuery orderId={this.state.orderId} tab={this.state.activeTab} />
        </PageContainer>
      </>
    )
  }
}

const render = (props) => <ManageTrustedOrder {...props} />

export default render

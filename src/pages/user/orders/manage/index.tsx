import React from "react"

import { PageContainer, NewOrderStepper } from "src/components"

import { getNodeIdFromUri } from "src/utils"
import {
  ProfilesListQueryContainer,
  CreateOrderInfoQueryContainer,
} from "src/query"

const OrderStepper = CreateOrderInfoQueryContainer((props) => (
  <NewOrderStepper {...props} />
))

const ManageOrder = (props) => {
  const orderId = getNodeIdFromUri(props.location.pathname)

  return (
    <PageContainer>
      <OrderStepper orderId={orderId} viewer={props.viewer} />
    </PageContainer>
  )
}

export default ProfilesListQueryContainer((props) => <ManageOrder {...props} />)

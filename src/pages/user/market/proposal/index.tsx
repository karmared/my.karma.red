import React, { useContext } from "react"
import { useHistory } from "react-router"

import { getBootstrappedOrder, getNodeIdFromUri, parseOrder } from "src/utils"
import { CurrentProfileContext, QueryParamsConsumer } from "src/context"
import { ProposalInfoQueryContainer } from "src/query"

import {
  PageContainer,
  ProposalBody,
  ProposalHeader,
  ScrollToTopOnMount,
  ProposalMockLoader,
  ProposalReturnButton,
  ProposalSpecification,
} from "src/components"

function Proposal(props) {
  const { data, getQueryParam } = props
  const currentProfile = useContext(CurrentProfileContext) as any

  const history = useHistory()

  const tempOrder = parseOrder(data.node)
  const order = getBootstrappedOrder(tempOrder)
  const { id: profileId } = order.profile
  const isOwner = !!currentProfile.profile && profileId === currentProfile.profile.id
  const isInvestor = !!currentProfile.profile && !!currentProfile.profile.approvedAsInvestor

  function handleReturn() {
    history.goBack()
  }

  return (
    <PageContainer>
      <ProposalReturnButton onClick={handleReturn}>Назад</ProposalReturnButton>
      <ProposalHeader data={order} isOwner={isOwner} history={history} />
      <ProposalSpecification history={history} data={order} isOwner={isOwner} />
      <ProposalBody
        getQueryParam={getQueryParam}
        data={order}
        isOwner={isOwner}
        isInvestor={isInvestor}
      />
    </PageContainer>
  )
}

function Render(props) {
  return (
    <QueryParamsConsumer>
      {({ getQueryParam }) => (
        <Proposal {...props} getQueryParam={getQueryParam} />
      )}
    </QueryParamsConsumer>
  )
}

export default function (props) {
  const { location } = props
  const orderId = getNodeIdFromUri(location.pathname)

  const ProposalContainer = ProposalInfoQueryContainer(
    (orderProps) => <Render {...props} {...orderProps} />,
    () => <ProposalMockLoader {...props} />,
  )

  return (
    <>
      <ScrollToTopOnMount />
      <ProposalContainer orderId={orderId} />
    </>
  )
}

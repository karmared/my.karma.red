import React from "react"
import { Route, Switch } from "react-router"

import List from "./list"
import Proposal from "./proposal"

function Market(props) {
  return (
    <Switch>
      <Route
        path="/market"
        exact={true}
        render={(routerProps) => <List {...routerProps} {...props} />}
      />
      <Route
        path="/market/:id"
        render={(routerProps) => <Proposal {...routerProps} {...props} />}
      />
    </Switch>
  )
}

export default Market

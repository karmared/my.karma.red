import React from "react"
import { graphql } from "relay-runtime"

import { BottomAchieveContainer } from "src/containers"

import {
  OrdersList,
  PageContainer,
  OrderBanners,
  QueryRenderer,
} from "src/components"

const query = graphql`
  query listQuery {
    orders(filter: { status: [TRANSFER, CONFIRMED] }) {
      count
    }
  }
`

function Render(props) {
  const { count = 0 } = props.orders

  return (
    <PageContainer>
      <OrderBanners />

      <BottomAchieveContainer>
        {!!count && (
          <>
            <OrdersList.market.active />
            <OrdersList.market.cession />
          </>
        )}

        {!count && <OrdersList.empty />}

        <OrdersList.market.repayment />
        <OrdersList.market.completed />
      </BottomAchieveContainer>
    </PageContainer>
  )
}

export default () => <QueryRenderer query={query} render={Render} />

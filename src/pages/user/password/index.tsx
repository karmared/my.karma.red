import React from "react"

import { Forms } from "src/components"

const SetNewPassword = () => <Forms.password.create />

export default SetNewPassword

import React from "react"
import { Switch, Route, Redirect } from "react-router"

import { SiteHeader, ReferralPage, BorrowerRating } from "src/components"
import { CurrencyProvider, CurrentProfileProvider } from "src/context"
import { dynamic, catchError } from "src/utils"

const Logout = dynamic(() => import("./logout").catch(catchError))
const Market = dynamic(() => import("./market").catch(catchError))
const Profiles = dynamic(() => import("./profiles").catch(catchError))
const Password = dynamic(() => import("./password").catch(catchError))
const Account = dynamic(() => import("./account").catch(catchError))
const Orders = dynamic(() => import("./orders").catch(catchError))
const Join = dynamic(() => import("../common/join").catch(catchError))
const Invest = dynamic(() => import("./invest").catch(catchError))
const Faq = dynamic(() => import("../common/faq").catch(catchError))
const Trusted = dynamic(() => import("./trusted").catch(catchError))
const Token = dynamic(() => import("../common/token").catch(catchError))
const About = dynamic(() => import("../common/about").catch(catchError))
const Email = dynamic(() => import("./email").catch(catchError))

const Render = (props) => {
  const { location } = window

  return (
    <CurrencyProvider>
      <CurrentProfileProvider>
        <SiteHeader.User viewer={props.viewer || null} location={location} />
        <Switch>
          <Route
            path="/market"
            render={(routerProp) => <Market {...routerProp} {...props} />}
          />
          <Route
            path="/logout"
            render={(routerProp) => <Logout {...routerProp} {...props} />}
          />
          <Route
            path="/invest"
            render={(routerProp) => <Invest {...routerProp} {...props} />}
          />
          <Route
            path="/profiles"
            render={(routerProp) => <Profiles {...routerProp} {...props} />}
          />
          <Route
            path="/accounts"
            render={(routerProp) => <Account {...routerProp} {...props} />}
          />
          <Route
            path="/orders"
            render={(routerProp) => <Orders {...routerProp} {...props} />}
          />
          <Route
            path="/password"
            render={(routerProp) => <Password {...routerProp} {...props} />}
          />
          <Route
            path="/email"
            render={(routerProp) => <Email {...routerProp} {...props} />}
          />
          <Route
            path="/trusted"
            render={(routerProp) => <Trusted {...routerProp} {...props} />}
          />
          <Route
            path="/new-order"
            render={(routerProp) => <Orders {...routerProp} {...props} />}
          />
          <Route
            path="/token/:token?"
            exact={true}
            render={(routerProp) => <Token {...routerProp} {...props} />}
          />
          <Route
            path="/join"
            render={(routerProp) => <Join {...routerProp} {...props} />}
          />
          <Route
            path="/about"
            exact={true}
            render={(routerProp) => <About {...routerProp} {...props} />}
          />
          <Route
            path="/faq"
            render={(routerProp) => <Faq {...routerProp} {...props} />}
          />
          <Route
            path="/rating/borrower"
            render={(routerProp) => (
              <BorrowerRating {...routerProp} {...props} />
            )}
          />
          <Route path="/referral/:id" component={ReferralPage} />
          <Redirect to="/market" />
        </Switch>
      </CurrentProfileProvider>
    </CurrencyProvider>
  )
}

export default Render

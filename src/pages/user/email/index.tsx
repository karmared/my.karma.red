import React from "react"
import { Switch, Route, Redirect } from "react-router"

import { Forms } from "src/components"

const EmailPage = () => (
  <Switch>
    <Route exact path="/email">
      <Forms.email.change />
    </Route>

    <Route exact path="/email/confirmation">
      <Forms.email.confirmation />
    </Route>

    <Redirect to="/email" />
  </Switch>
)

export default EmailPage

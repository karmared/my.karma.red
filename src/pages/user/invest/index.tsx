import React from "react"
import { Route, Switch } from "react-router"

import Scoring from "src/pages/common/scoring"
import InvestorInfo from "src/pages/common/info"
import { InvestmentPage } from "src/components"

function Invest(props) {
  return (
    <Switch>
      <Route
        path="/invest/scoring"
        render={(routerProps) => <Scoring {...routerProps} {...props} />}
      />
      <Route
        path="/invest/info"
        render={(routerProps) => <InvestorInfo {...routerProps} {...props} />}
      />
      <Route
        path="/invest/:id"
        render={(routerProps) => <InvestmentPage {...routerProps} {...props} />}
      />
    </Switch>
  )
}

export default Invest

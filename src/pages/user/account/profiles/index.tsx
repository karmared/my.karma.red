import React, { useState } from "react"
import memoize from "memoize-one"
import { Route, Switch } from "react-router"

import { QueryParamsConsumer } from "src/context"
import { AccountInfoQueryContainer } from "src/query"
import {
  Flex,
  Loader,
  LoaderBox,
  ProfileInfo,
  ProfileData,
  PageContainer,
  AccreditationProfile,
} from "src/components"

import EmptyView from "./Empty"
import Redirect from "./Redirect"

import { TAB } from "../../../../components/molecules/ProfileMenu/constants"

import CreateProfile from "./Create"

const renderNull = () => (
  <PageContainer >
    <LoaderBox>
      <Loader margin="90px auto"/>
    </LoaderBox>
  </PageContainer>
)

const ensureCreateForeignInvestorProfileComponent = memoize(
  (viewer) => () => <CreateProfile.foreign viewer={viewer} />,
  (prev, next) => prev[0].id === next[0].id,
)

const ensureCreateInvestorProfileComponent = memoize(
  (viewer) => () => <CreateProfile.individual viewer={viewer} />,
  (prev, next) => prev[0].id === next[0].id,
)

const ensureCreateEntrepreneurProfileComponent = memoize(
  (viewer) => () => <CreateProfile.entrepreneur viewer={viewer} />,
  (prev, next) => prev[0].id === next[0].id,
)

function getInitialTab(qs) {
  if (qs === TAB.HISTORY) return TAB.HISTORY
  if (qs === TAB.BANK) return TAB.BANK
  if (qs === TAB.INFORMATION) return TAB.INFORMATION
  if (qs === TAB.TAXES) return TAB.TAXES

  return ""
}

function Profiles(props) {
  const {
    node, viewer, getQueryParam, history,
  } = props

  const queryString = getQueryParam("type")
  const [activeTab, setActiveTab] = useState(getInitialTab(queryString))

  function changeTab(tab) {
    setActiveTab(tab)

    if (tab) history.push(`/accounts/${node.id}?type=${tab}`)
  }

  function handleReturn() {
    history.push(`/accounts/${node.id}`)
    changeTab("")
  }

  return (
    <PageContainer>
      <Flex>
        <ProfileInfo
          foreignProfile={null}
          profile={node}
          viewer={viewer}
          tab={activeTab}
          changeTab={changeTab}
          history={history}
        />

        <Route
          exact
          path="/accounts"
          render={() => <Redirect profile={node} />}
        />

        <Switch>
          <Route exact path="/accounts" component={EmptyView} />

          <Route
            exact
            path="/accounts/create/individual"
            component={ensureCreateInvestorProfileComponent(viewer)}
          />

          <Route
            exact
            path="/accounts/create/entrepreneur"
            component={ensureCreateEntrepreneurProfileComponent(viewer)}
          />

          <Route
            exact
            path="/accounts/create/foreign"
            component={ensureCreateForeignInvestorProfileComponent(viewer)}
          />

          <Route
            exact
            path="/accounts/:id"
            render={(params) => (
              <ProfileData
                {...params}
                tab={activeTab}
                queryString={queryString}
                foreignProfile={null}
                profile={node}
                handleReturn={handleReturn}
              />
            )}
          />

          <Route
            exact
            path="/accounts/:id/accreditation"
            render={() => (
              <AccreditationProfile
                profile={node}
                handleReturn={handleReturn}
              />
            )}
          />
        </Switch>
      </Flex>
    </PageContainer>
  )
}

function Render(props) {
  return (
    <QueryParamsConsumer>
      {({ getQueryParam, setQueryParam }) => (
        <Route>
          {({ history }) => (
            <Profiles
              {...props}
              getQueryParam={getQueryParam}
              setQueryParam={setQueryParam}
              history={history}
            />
          )}
        </Route>
      )}
    </QueryParamsConsumer>
  )
}

export default AccountInfoQueryContainer((props) => <Render {...props} />, renderNull)

import Foreign from "./Foreign"
import Individual from "./Individual"
import Entrepreneur from "./Entrepreneur"

const render = () => {}

render.foreign = Foreign
render.individual = Individual
render.entrepreneur = Entrepreneur

export default render

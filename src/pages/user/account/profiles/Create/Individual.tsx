import React from "react"
import { Redirect } from "react-router"

import { Forms } from "src/components"
import { ProfileType } from "src/constants"
import { isRussianInvestor, isRussianUser } from "src/utils"

import { Divider, Container, Wrapper } from "./styles"
import { Header, InitialProfile } from "./elements"

const render = ({ viewer }) => {
  const hasIndividualProfile = viewer.profiles.some(isRussianInvestor)

  if (!isRussianUser(viewer) || hasIndividualProfile) {
    return <Redirect to={"/accounts"} />
  }

  if (!viewer.profiles.length) {
    return <InitialProfile viewer={viewer} />
  }

  return (
    <Wrapper>
      <Container>
        <Header />
        <Divider />
        <Forms.individualProfile.accreditation.investor
          type={ProfileType.individual}
        />
      </Container>
    </Wrapper>
  )
}

export default render

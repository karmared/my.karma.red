import styled, { css } from "styled-components"

import { Box, CardWrapper } from "src/components"

export const IconContainer = styled(Box)`
  width: 14px;
  height: 10px;
  margin-top: 16px;
  margin-right: 15px;
  cursor: pointer;
`

export const Wrapper = styled.div`
  width: 100%;
  max-width: calc(100% - 300px);
  position: relative;

  @media screen and (max-width: 800px) {
    max-width: calc(100% - 240px);
  }

  @media screen and (max-width: 800px) {
    max-width: 100%;
  }
`

export const Container = styled(CardWrapper)`
  padding: 28px;
  font-size: 1rem;
  width: 100%;
  color: ${({ theme }) => theme.colors.textBlack};
  flex-direction: column;
  font-family: Geometria, sans-serif;

  @media screen and (max-width: 800px) {
    box-shadow: none;
    padding: 0;
    margin-top: 40px;
  }
`

// export const Container = styled(Box)`
//   width: 100%;
//   max-width: 1280px;
//   padding: 30px;
//   border-radius: 4px;
//   box-shadow: 0 0 24px 0 rgba(0, 0, 0, 0.12);
//   background-color: ${({ theme }) => theme.colors.white};
// `

export const Divider = styled(Box)`
  width: 100%;
  height: 1px;
  background-color: ${({ theme }) => theme.colors.grey};
`

const activeStepNumberStyles = css`
  color: ${({ theme }) => theme.colors.black};
  font-weight: 500;
`

export const StepNumber = styled(Box)`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 28px;
  height: 28px;
  margin-right: 10px;
  border-radius: 50%;
  font-size: 14px;
  color: ${({ theme }) => theme.colors.greyDark};
  background-color: ${({ theme }) => theme.colors.grey};

  ${({ active }) => active && activeStepNumberStyles}
`

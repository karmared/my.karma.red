import React from "react"

import {
  Box, Flex, Link, Text, Forms, Translate,
} from "src/components"

import {
  Divider, Container, StepNumber, IconContainer,
} from "./styles"

/* eslint-disable max-len */
const ArrowIcon = () => (
  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 14 10">
    <path
      fill="#5a616e"
      d="M13 4H3.4l2.31-2.31A.948.948 0 0 0 6 1a1.012 1.012 0 0 0-1-1 .943.943 0 0 0-.69.29L.331 4.269A.959.959 0 0 0 0 5a.907.907 0 0 0 .323.723L4.31 9.71A.943.943 0 0 0 5 10a1.011 1.011 0 0 0 1-1 .948.948 0 0 0-.29-.69L3.4 6H13a1 1 0 0 0 0-2z"
      data-name="Group 1"
    />
  </svg>
)
/* eslint-enable max-len */

export const Header = () => (
  <Flex mb="25px">
    <Link to="/accounts">
      <IconContainer>
        <ArrowIcon />
      </IconContainer>
    </Link>
    <Box>
      <Text fontSize="32px" lineHeight="1.5">
        Аккредитация в качестве инвестора ИП
      </Text>
      <Box mt="5px">
        <Text fontSize="14px" lineHeight="1.5" color="greyDark">
          <Translate i18n="components:accreditation.individual.subheader" />
        </Text>
      </Box>
    </Box>
  </Flex>
)

export const Instructions = () => (
  <Flex mt="30px">
    <Flex mr="40px" alignItems="center">
      <StepNumber active>1</StepNumber>
      <Text
        fontSize="14px"
        lineHeight="1.5"
        fontWeight="500"
        textTransform="uppercase"
      >
        <Translate i18n="components:accreditation.individual.contact_data" />
      </Text>
    </Flex>
    <Flex alignItems="center">
      <StepNumber>2</StepNumber>
      <Text
        fontSize="14px"
        lineHeight="1.5"
        color="greyDark"
        textTransform="uppercase"
      >
        <Translate i18n="components:accreditation.individual.additional_info" />
      </Text>
    </Flex>
  </Flex>
)

export const InitialProfile = ({ viewer }) => (
  <Container>
    <Header />
    <Divider />
    <Instructions />
    <Forms.individualProfile.create.investor viewer={viewer} />
  </Container>
)

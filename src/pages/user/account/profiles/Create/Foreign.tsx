import React from "react"
import { Redirect } from "react-router"

import { Forms } from "src/components"
import { ProfileType } from "src/constants"
import { isInvestor, isRussianUser } from "src/utils"

import { Header } from "./elements"
import { Divider, Container, Wrapper } from "./styles"

const render = ({ viewer }) => {
  if (isRussianUser(viewer)) {
    return <Redirect to={"/accounts"} />
  }

  const investorProfile = viewer?.foreignProfiles?.find(isInvestor)

  if (investorProfile) {
    return <Redirect to={`/accounts/${investorProfile.id}`} />
  }

  return (
    <Wrapper>
      <Container>
        <Header />
        <Divider />
        <Forms.individualProfile.accreditation.investor
          type={ProfileType.foreignIndividual}
        />
      </Container>
    </Wrapper>
  )
}

export default render

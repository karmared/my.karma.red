import React from "react"
import { Redirect } from "react-router"

import { Forms } from "src/components"
import { ProfileType } from "src/constants"
import { isRussianEntrepreneur, isRussianUser } from "src/utils"

import { Divider, Container, Wrapper } from "./styles"
import { Header, InitialProfile } from "./elements"

const render = ({ viewer }) => {
  const hasEntrepreneurProfile = viewer.profiles.some(isRussianEntrepreneur)

  if (!isRussianUser(viewer) || hasEntrepreneurProfile) {
    return <Redirect to={"/accounts"} />
  }

  if (!viewer.profiles.length) {
    return <InitialProfile viewer={viewer} />
  }

  return (
    <Wrapper>
      <Container>
        <Header />
        <Divider />
        <Forms.individualProfile.accreditation.investor
          type={ProfileType.entrepreneur}
        />
      </Container>
    </Wrapper>
  )
}

export default render

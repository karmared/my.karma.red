/* eslint-disable import/prefer-default-export */
import styled from "styled-components"

import { Box } from "src/components"

export const TemplateContainer = styled(Box)`
  > div {
    width: 430px;
    height: 550px;
    margin-right: 20px;
    padding: 40px 30px;
    box-shadow: 0 0 24px 0 rgba(217, 222, 226, 0.76);

    &:hover {
      background-color: ${({ theme }) => theme.colors.blues[1]};
    }
  }
`

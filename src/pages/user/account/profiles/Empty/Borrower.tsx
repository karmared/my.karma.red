import React from "react"

import { Route } from "react-router"

import {
  Button, Template, Translate,
} from "src/components"

import { TemplateContainer } from "./styles"

const handleRoute = (history) => () => {
  history.push("profiles/create/juristic")
}

const Action = () => (
  <Route>
    {({ history }) => (
      <Button variant="blueWide" onClick={handleRoute(history)}>
        <Translate i18n="components:order_list.loans.empty.buttons.action" />
      </Button>
    )}
  </Route>
)

export default () => (
  <TemplateContainer>
    <Template
      layout="empty"
      header="components:order_list.loans.empty.header"
      image="/images/orderLists/empty/loans.svg"
      description="components:order_list.loans.empty.description"
      appendix="components:order_list.loans.empty.appendix"
      action={<Action />}
    />
  </TemplateContainer>
)

import React from "react"

import { Flex } from "src/components"
import { ViewerDataConsumer } from "src/context"
import { getProperty, isRussianUser } from "src/utils"

import EmptyInvestor from "./Investor"
import EmptyBorrower from "./Borrower"

const render = ({ viewer }) => {
  if (getProperty(viewer, "profiles.length")) return null

  return (
    <Flex>
      {isRussianUser(viewer) && <EmptyBorrower />}
      <EmptyInvestor />
    </Flex>
  )
}

export default () => (
  <ViewerDataConsumer>{(viewer) => render({ viewer })}</ViewerDataConsumer>
)

import React from "react"
import { Redirect } from "react-router"

function Render(props) {
  const { profile } = props

  if (profile && profile.id) {
    return <Redirect to={`/accounts/${profile.id}`} />
  }

  return <Redirect to="market" />
}

export default Render

import React from "react"
import { Route } from "react-router"

import Profiles from "./profiles"

function Account(props) {
  return (
    <Route
      path="/accounts"
      render={(routerProps) => <Profiles {...routerProps} {...props} />}
    />
  )
}

export default Account

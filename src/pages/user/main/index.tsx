import React from "react"
import { Redirect } from "react-router-dom"

const render = () => (
  <React.Fragment>
    <Redirect to={"/market"} />
  </React.Fragment>
)

export default render

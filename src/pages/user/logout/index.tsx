import React from "react"

import { removeAuthToken } from "src/utils"

import { RelayEnvironmentConsumer } from "src/context"

const render = (props) => {
  const { history, refreshEnvironment } = props

  removeAuthToken()
  refreshEnvironment()
  history.push("/login")

  return null
}

export default (props) => (
  <RelayEnvironmentConsumer>
    {({ refreshEnvironment }) => render({ ...props, refreshEnvironment })}
  </RelayEnvironmentConsumer>
)

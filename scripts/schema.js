require("../config/env")
require("isomorphic-fetch")

const fs = require("fs")
const {
  printSchema,
  buildClientSchema,
  introspectionQuery,
} = require("graphql")

const paths = require("../config/paths")

fetch(process.env.GRAPHQL_ENDPOINT, {
  method: "POST",
  headers: {
    Accept: "application/json",
    "Content-Type": "application/json",
  },
  body: JSON.stringify({ query: introspectionQuery }),
})
  .then((response) => response.json())
  .then((json) => {
    fs.writeFileSync(paths.schema, printSchema(buildClientSchema(json.data)))
  })

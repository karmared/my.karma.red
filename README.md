# my.karma.red frontend

### Environment

```
GRAPHQL_ENDPOINT=https://devnet-my.karma.red/graphql
GUEST_REDIRECT_PAGE=https://wow.karma.red/main
```

Consumed from `.env` file or|and environment variables. Environment variables take precedence.

#### Dev environment

```
PORT=3000
```

### Install

```sh
npm run install
```

#### Start

```sh
npm run prebuild
npm run dev
```

or

```sh
npm run build
```

and just use `public` folder as static site root

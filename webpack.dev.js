const path = require("path")
const webpack = require("webpack")
const HtmlWebpackPlugin = require("html-webpack-plugin")

const cacheDir = path.resolve(__dirname, "node_modules", ".cache")

function getThreadLoader(name) {
  return {
    loader: "thread-loader",
    options: {
      workerParallelJobs: 50,
      poolRespawn: false,
      name,
    },
  }
}

const common = require("./webpack.common")

module.exports = {
  ...common.config,
  mode: "development",
  output: {
    ...common.config.output,
    filename: "[name].js",
  },
  optimization: {
    ...common.config.optimization,
    runtimeChunk: "single",
  },
  plugins: [
    ...common.config.plugins,
    new webpack.HotModuleReplacementPlugin(),
    new HtmlWebpackPlugin({
      ...common.HtmlWebpackPluginConfig,
      filename: path.resolve(__dirname, "public", "index.html"),
      minify: false,
    }),
  ],
  module: {
    ...common.config.module,
    rules: [
      ...common.config.module.rules,
      {
        test: /\.(js|jsx|ts|tsx)$/,
        include: path.resolve(__dirname, "src"),
        exclude: /(node_modules|bower_components)/,
        use: [
          {
            loader: "cache-loader",
            options: {
              cacheDirectory: path.resolve(cacheDir, "js"),
            },
          },
          getThreadLoader("js"),
          {
            loader: "babel-loader",
            options: {
              cacheDirectory: path.resolve(cacheDir, "babel"),
            },
          },
        ],
      },
      {
        test: /\.css$/,
        use: [
          {
            loader: "cache-loader",
            options: {
              cacheDirectory: path.resolve(cacheDir, "css"),
            },
          },
          getThreadLoader("css"),
          "style-loader",
          {
            loader: "css-loader",
            options: {
              sourceMap: true,
            },
          },
        ],
      },
    ],
  },
  devServer: {
    open: true,
    port: process.env.PORT || 3000,
    historyApiFallback: true,
    hot: true,
    disableHostCheck: true,
    host: "0.0.0.0",
    contentBase: [
      path.resolve(__dirname, "public"),
      path.resolve(__dirname, "static"),
    ],
    publicPath: "/",
    compress: true,
    before: (app) => {
      app.get("/fonts/*", (req, res, next) => {
        res.setHeader("Cache-Control", "public, max-age=300, immutable")
        next()
      })
    },
  },
}
